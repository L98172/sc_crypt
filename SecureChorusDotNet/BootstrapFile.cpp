#include "stdafx.h"
#include "BootstrapFile.h"

namespace SecureChorus {

	using namespace System;
	using namespace System::IO;
	using namespace System::Collections::Generic;

BootstrapFile::BootstrapFile(FileStream^ booststrapFileStream)
{

	parse_file(booststrapFileStream);

}

void BootstrapFile::parse_file(FileStream^ ist)
{

	//  Get hexadecimal key string from stream, verify stream
	//  state and string length.

	StreamReader^ reader = gcnew StreamReader(ist);

	try {

		String^ key = reader->ReadLine();

		unsigned int tk_length;
	
		if( key->Length == 2 * tk_length_128)
		{
			tk_length = tk_length_128;
		}
		else if( key->Length == 2 * tk_length_256)
		{
			tk_length = tk_length_256;
		}
		else
		{
			throw gcnew Exception(String::Format("Unexpected key size '{0}'", key->Length));
		}

		// Convert key to binary, verifying all characters are
		// hexadecimal. Allow either case of A-F or a-f.

		this->BootstrapKey = gcnew List<Byte>(tk_length);

		String^ hexDigits = "0123456789ABCDEFabcdef";

		for (int i = 0; i != tk_length; ++i)
		{
		
			int d1 = hexDigits->IndexOf(key[2 * i]);
			int d2 = hexDigits->IndexOf(key[2 * i + 1]);

			if (d1 < 0 || d2 < 0)
			{
				throw gcnew Exception("Invalid hex digit");
			}

			this->BootstrapKey->Insert(i, 16 * (d1 >= 16 ? d1 - 6 : d1)
								+ (d2 >= 16 ? d2 - 6 : d2));

		}

		//  Get key ID from stream, verify not empty.
		this->KeyId = reader->ReadLine();
	
		if (String::IsNullOrEmpty(this->KeyId)) {

			throw gcnew Exception("Could not read Key ID");

		}

	} finally {
		reader->Close();
	}

}

};
