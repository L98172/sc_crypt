#pragma once

#include "Errno.h"

#include <msclr\marshal_cppstd.h>

namespace SecureChorus {

	using namespace System;
	using namespace System::Collections::Generic;
	using namespace System::Runtime::InteropServices;

	public ref class KMS
	{
	public:

		static ScErrorCode CreateKmsSecurityContext([Out] unsigned long% secCtx, System::String^ KMSUri, System::String^ TransportKeyId, array<Byte>^ transportKey);

		static ScErrorCode RequestKmsInitXML(unsigned long secCtx, System::String^ KMSUrl, System::String^ UserUri, [Out] array<Byte>^% xmlData);

		static ScErrorCode RequestKmsKeyProvXML(unsigned long secCtx, System::String^ KMSUrl, System::String^ UserUri, [Out] array<Byte>^% xmlData);

		static ScErrorCode RequestKmsCertCacheXML(unsigned long secCtx, System::String^ KMSUrl, System::String^ UserUri, [Out] array<Byte>^% xmlData);

		static ScErrorCode ProcessKmsRespXML(unsigned long secCtx, array<Byte>^ KmsResponse, System::String^ schemaDirectory);

	};

};

