#pragma once

#include <msclr\marshal_cppstd.h>

#include "sc_types.h"

namespace SecureChorus {

	using namespace System;
	using namespace System::Runtime::InteropServices;

	ref class InteropUtils
	{
	public:

		static void CopyVectorToByteArray(std::vector<unsigned char>& sourceVector, [Out] array<Byte>^% byteArray);
	
		static void CopyByteArrayToVector(array<Byte>^ byteArray, std::vector<unsigned char>& destVector);

		static void CopyNativeToManagedByteArray(const unsigned char* nativeArray, unsigned int length, [Out] array<System::Byte>^% byteArray);

		static void StringListToManagedArray(StringList& stringList, [Out] array<System::String^>^% managedStringArray);

	};

};

