#include "stdafx.h"
#include "KMS.h"

#include <msclr\marshal.h>
#include <msclr\marshal_cppstd.h>

// Include headers from the Secure Chorus include directory
#include "sc_libmskms.h"
#include "sc_context.h"
#include "sc_types.h"
#include "sc_errno.h"

#include "Errno.h"
#include "InteropUtils.h"

namespace SecureChorus {

	using namespace System;
	using namespace System::Collections::Generic;
	using namespace msclr::interop;
	using namespace System::Runtime::InteropServices;

ScErrorCode KMS::RequestKmsInitXML(unsigned long secCtx, System::String^ KMSUrl, System::String^ UserUri, [Out] array<Byte>^% xmlData)
{

	std::vector<unsigned char> xmlVector;

	ScErrno errorNum = ScLibMsKMS::RequestKmsInitXML(static_cast<SecCtx>(secCtx),
		marshal_as<std::string>(KMSUrl),
		marshal_as<std::string>(UserUri),
		xmlVector
		);

	if (! xmlVector.empty()) {
		InteropUtils::CopyVectorToByteArray(xmlVector, xmlData);
	}

	return static_cast<ScErrorCode>(errorNum);

}

ScErrorCode KMS::CreateKmsSecurityContext([Out] unsigned long% secCtx, System::String^ KMSUri, System::String^ TransportKeyId, array<Byte>^ transportKey)
{

	// Create a vector and copy the byte array into it
	std::vector<unsigned char> transportKeyVector;

	InteropUtils::CopyByteArrayToVector(transportKey, transportKeyVector);

	SecCtx securityContext;

	ScErrno errorNum = ScLibMsKMS::CreateKmsSecurityContext(securityContext,
		marshal_as<std::string>(KMSUri),
		marshal_as<std::string>(TransportKeyId),
		transportKeyVector
		);

	// Copy secCtx back into the managed variable
	secCtx = static_cast<unsigned long>(securityContext);
	
	return static_cast<ScErrorCode>(errorNum);

}

ScErrorCode KMS::RequestKmsKeyProvXML(unsigned long secCtx, System::String^ KMSUrl, System::String^ UserUri, [Out] array<Byte>^% xmlData)
{
	ScErrno errorNum;
	std::vector<unsigned char> xmlVector;
	
	errorNum = ScLibMsKMS::RequestKmsKeyProvXML(static_cast<SecCtx>(secCtx), 
		marshal_as<std::string>(KMSUrl), 
		marshal_as<std::string>(UserUri), 
		xmlVector
		);

	if (! xmlVector.empty()) {
		InteropUtils::CopyVectorToByteArray(xmlVector, xmlData);
	}

	return static_cast<ScErrorCode>(errorNum);
}

ScErrorCode KMS::RequestKmsCertCacheXML(unsigned long secCtx, System::String^ KMSUrl, System::String^ UserUri, [Out] array<Byte>^% xmlData)
{
	ScErrno errorNum;
	std::vector<unsigned char> xmlVector;

	errorNum = ScLibMsKMS::RequestKmsCertCacheXML(static_cast<SecCtx>(secCtx), 
		marshal_as<std::string>(KMSUrl), 
		marshal_as<std::string>(UserUri), 
		xmlVector
		);

	if (! xmlVector.empty()) {
		InteropUtils::CopyVectorToByteArray(xmlVector, xmlData);
	}

	return static_cast<ScErrorCode>(errorNum);
}

ScErrorCode KMS::ProcessKmsRespXML(unsigned long secCtx, array<Byte>^ KmsResponse, System::String^ schemaDirectory)
{
	ScErrno errorNum;

	// Create a vector and copy the byte array into it
	std::vector<unsigned char> responseXmlVector;

	InteropUtils::CopyByteArrayToVector(KmsResponse, responseXmlVector);

	errorNum = ScLibMsKMS::ProcessKmsRespXML(static_cast<SecCtx>(secCtx),
		responseXmlVector,
		marshal_as<std::string>(schemaDirectory)
		);
	
	return static_cast<ScErrorCode>(errorNum);
}

};
