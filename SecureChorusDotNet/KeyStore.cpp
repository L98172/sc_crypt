#include "stdafx.h"
#include "KeyStore.h"

#include <msclr\marshal.h>
#include <msclr\marshal_cppstd.h>

// Include headers from the Secure Chorus include directory
#include "sc_errno.h"
#include "sc_keystore.h"

#include "Errno.h"
#include "InteropUtils.h"

namespace SecureChorus {

	using namespace System;
	using namespace System::Collections::Generic;
	using namespace msclr::interop;
	using namespace System::Runtime::InteropServices;

ScErrorCode KeyStore::SetStoreFilePath(System::String^ newFilePath)
{

	// Free the previous copy of the path
	if (mFilePath != 0)
	{
		delete mFilePath;
	}

	// Copy the string so the underlying char array won't go out of scope and will remaing available to Secure Chorus
	mFilePath = new std::string(marshal_as<std::string>(newFilePath));

	// Assign Secure Chorus a pointer to the underlying char array of the string
	ScErrno errorNum = SetFilePath(mFilePath->c_str());

	return static_cast<ScErrorCode>(errorNum);

}

};
