#include "stdafx.h"
#include "MikeySakke.h"

#include <msclr\marshal.h>
#include <msclr\marshal_cppstd.h>

// Include headers from the Secure Chorus include directory
#include "sc_libms.h"
#include "sc_context.h"
#include "sc_types.h"
#include "sc_errno.h"

#include "Errno.h"
#include "InteropUtils.h"

namespace SecureChorus {

	using namespace System;
	using namespace System::Collections::Generic;
	using namespace msclr::interop;
	using namespace System::Runtime::InteropServices;

	ScErrorCode	MikeySakke::CreateMikeySakkeIMessage([Out] array<Byte>^% iMessage,
			[Out] array<Byte>^% ssv,
			unsigned int secCtx,
			System::String^ senderUri,
			System::String^ recipientUri,
			bool v
			)
	{

		ScErrno errorNum;
		std::vector<uint8_t> messageDataVec;
		std::vector<uint8_t> ssvVec;

		errorNum = ScLibMs::CreateMikeySakkeIMessage(messageDataVec,
			ssvVec,
			static_cast<SecCtx>(secCtx),
			marshal_as<std::string>(senderUri),
			marshal_as<std::string>(recipientUri),
			v
			);


		if (! messageDataVec.empty()) {
			InteropUtils::CopyVectorToByteArray(messageDataVec, iMessage);
		}

		if (! ssvVec.empty()) {
			InteropUtils::CopyVectorToByteArray(ssvVec, ssv);
		}

		return static_cast<ScErrorCode>(errorNum);

	}

	ScErrorCode MikeySakke::CreateMikeySakkeIMessageWithSsv([Out] array<Byte>^% iMessage,
			unsigned int secCtx,
			System::String^ senderUri,
			System::String^ recipientUri,
			array<Byte>^ ssv,
			bool v
			)
	{
		ScErrno errorNum;
		std::vector<uint8_t> messageDataVec;
		std::vector<uint8_t> ssvVec;

		InteropUtils::CopyByteArrayToVector(ssv, ssvVec);

		errorNum = ScLibMs::CreateMikeySakkeIMessageWithSsv(messageDataVec,
			static_cast<SecCtx>(secCtx),
			marshal_as<std::string>(senderUri),
			marshal_as<std::string>(recipientUri),
			ssvVec,
			v
			);


		if (! messageDataVec.empty()) {
			InteropUtils::CopyVectorToByteArray(messageDataVec, iMessage);
		}

		return static_cast<ScErrorCode>(errorNum);
	}

	ScErrorCode MikeySakke::ProcessMikeySakkeIMessage(System::String^ recipientUri,
			System::String^ senderUri,
			array<Byte>^ ssv,
			bool v,
			unsigned int secCtx,
			array<Byte>^ message,
			bool checkSignature
			)
	{
		ScErrno errorNum;
		std::vector<uint8_t> ssvVec;
		std::vector<uint8_t> messageDataVec;

		InteropUtils::CopyByteArrayToVector(message, messageDataVec);

		errorNum = ScLibMs::ProcessMikeySakkeIMessage(marshal_as<std::string>(recipientUri),
			marshal_as<std::string>(senderUri),
			ssvVec, 
			v,
			static_cast<SecCtx>(secCtx),
			messageDataVec,
			checkSignature
			);

		if (! ssvVec.empty()) {
			InteropUtils::CopyVectorToByteArray(ssvVec, ssv);
		}
		
		return static_cast<ScErrorCode>(errorNum);
	}

	ScErrorCode MikeySakke::CreateMikeyEccsiSig([Out] array<Byte>^% mikeyEccsi,
		unsigned int secCtx,
		System::String^ signingUri,
		System::String^ msgTimestamp,
		array<Byte>^ data
		)
	{
		ScErrno errorNum;
		std::vector<uint8_t> mikeyEccsiVec;
		std::vector<uint8_t> dataVec;

		InteropUtils::CopyByteArrayToVector(data, dataVec);

		errorNum = ScLibMs::CreateMikeyEccsiSig(mikeyEccsiVec,
			static_cast<SecCtx>(secCtx),
			marshal_as<std::string>(signingUri),
			marshal_as<std::string>(msgTimestamp),
			dataVec
			);

		if (! mikeyEccsiVec.empty()) {
			InteropUtils::CopyVectorToByteArray(mikeyEccsiVec, mikeyEccsi);
		}

		return static_cast<ScErrorCode>(errorNum);
	}

	ScErrorCode MikeySakke::VerifyMikeyEccsiSig(
		array<Byte>^ mikeyEccsi,
		unsigned int secCtx,
		System::String^ signingUri,
		System::String^ msgTimestamp,
		array<Byte>^ data
		)
	{
		ScErrno errorNum;
		std::vector<uint8_t> mikeyEccsiVec;
		std::vector<uint8_t> dataVec;

		InteropUtils::CopyByteArrayToVector(mikeyEccsi, mikeyEccsiVec);
		InteropUtils::CopyByteArrayToVector(data, dataVec);

		errorNum = ScLibMs::VerifyMikeyEccsiSig(
			mikeyEccsiVec,
			static_cast<SecCtx>(secCtx),
			marshal_as<std::string>(signingUri),
			marshal_as<std::string>(msgTimestamp),
			dataVec
			);

		return static_cast<ScErrorCode>(errorNum);
	}

};
