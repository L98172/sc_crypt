#pragma once

#include "Errno.h"

namespace SecureChorus {

	using namespace System;
	using namespace System::Collections::Generic;
	using namespace System::Runtime::InteropServices;

	public ref class MikeySakke
	{

	public:
		
		static ScErrorCode CreateMikeySakkeIMessage([Out] array<Byte>^% iMessage,
			[Out] array<Byte>^% ssv,
			unsigned int secCtx,
			System::String^ senderUri,
			System::String^ recipientUri,
			bool v
			);

		static ScErrorCode CreateMikeySakkeIMessageWithSsv([Out] array<Byte>^% iMessage,
			unsigned int secCtx,
			System::String^ senderUri,
			System::String^ recipientUri,
			array<Byte>^ ssv,
			bool v
			);

		static ScErrorCode ProcessMikeySakkeIMessage(System::String^ recipientUri,
			System::String^ senderUri,
			array<Byte>^ ssv,
			bool v,
			unsigned int secCtx,
			array<Byte>^ message,
			bool checkSignature
			);

		static ScErrorCode CreateMikeyEccsiSig([Out] array<Byte>^% mikeyEccsi,
			unsigned int secCtx,
			System::String^ signingUri,
			System::String^ msgTimestamp,
			array<Byte>^ data
			);

		static ScErrorCode VerifyMikeyEccsiSig(
			array<Byte>^ mikeyEccsi,			
			unsigned int secCtx,
			System::String^ signingUri,
			System::String^ msgTimestamp,
			array<Byte>^ data
			);

	};

};
