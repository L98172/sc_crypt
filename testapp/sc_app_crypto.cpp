#include <cstddef>
#include <vector>

#include <openssl/aes.h>
#include <openssl/evp.h>
#include <openssl/rand.h>

#include "sc_app_crypto.h"


//  AES strength in bits. Must match used cipher.
const size_t AES_BITS = 128;


//  Authenticated encrypt data using AES-128 in GCM mode.
//  Args: key        - Key. Must have length 16 bytes.
//        plaintext  - Plaintext to be encrypted.
//        ciphertext - Ciphertext after encryption.
//  Rets: True if encryption succeeds, false otherwise.
bool encrypt(const std::vector<unsigned char> &key,
	         const std::vector<unsigned char> &plaintext,
			 std::vector<unsigned char> &ciphertext)
{
  //  Verify key size.
  if (8 * key.size() != AES_BITS)
  {
	return false;
  }

  //  Plaintext (input) length.
  const std::size_t in_length = plaintext.size();

  if (in_length == 0 || in_length > INT_MAX)
  {
	return false;
  }

  //  Initialise cipher.
  EVP_CIPHER_CTX ctx;
  EVP_CIPHER_CTX_init(&ctx);

  //  Ciphertext set to at least maximum size. This is in_length
  //  rounded up to nearest multiple of AES_BLOCK_SIZE (for
  //  simplicity simply add AES_BLOCK_SIZE) plus AES_BLOCK_SIZE
  //  at start for initialisation vectorx and AES_BLOCK_SIZE
  //  at end for tag (also allows this much extra working space).
  ciphertext.resize(in_length + 3 * AES_BLOCK_SIZE);

  //  Create random IV, put at start of ciphertext. Use length
  //  to record growing length of ciphertext.
  if (!RAND_bytes(&ciphertext[0], AES_BLOCK_SIZE))
  {
	return false;
  }

  size_t length = AES_BLOCK_SIZE;

  //  Initialise encryption, including setting key and IV.
  if (!EVP_EncryptInit_ex(&ctx, EVP_aes_128_gcm(), 0, 0, 0)
      || !EVP_CIPHER_CTX_ctrl(&ctx, EVP_CTRL_GCM_SET_IVLEN, AES_BLOCK_SIZE, 0)
      || !EVP_EncryptInit_ex(&ctx, 0, 0, &key[0], &ciphertext[0]))
  {
	ciphertext.clear();
    EVP_CIPHER_CTX_cleanup(&ctx);
	return false;
  }

  //  Main encryption, putting in ciphertext after IV. out_length1 is
  //  actual length of this part of ciphertext.
  int out_length1;

  if (!EVP_CipherUpdate(&ctx, &ciphertext[length], &out_length1,
	                    &plaintext[0], (int)in_length))
  {
	ciphertext.clear();
    EVP_CIPHER_CTX_cleanup(&ctx);
	return false;
  }

  length += out_length1;

  //  Finish encryption, out_length2 is actual length of this part of
  //  ciphertext.
  int out_length2;

  if (!EVP_CipherFinal_ex(&ctx, &ciphertext[length], &out_length2))
  {
	ciphertext.clear();
    EVP_CIPHER_CTX_cleanup(&ctx);
	return false;
  }

  length += out_length2;

  //  Create tag, add top end of ciphertext.
  if (!EVP_CIPHER_CTX_ctrl(&ctx, EVP_CTRL_GCM_GET_TAG, AES_BLOCK_SIZE,
	                       &ciphertext[length]))
  {
	ciphertext.clear();
    EVP_CIPHER_CTX_cleanup(&ctx);
	return false;
  }

  length += AES_BLOCK_SIZE;

  //  Resize ciphertext to correct size, including IV, cleanup
  //  and exit.
  ciphertext.resize(length);
  EVP_CIPHER_CTX_cleanup(&ctx);
  return true;
}


//  Authenticated decrypt data using AES-128 in GCM mode.
//  Args: key        - Key. Must have length 16 bytes.
//        ciphertext - Ciphertext to be decrypted.
//        ciphertext - Plaintext after decryption.
//  Rets: True if decryption succeeds, false otherwise.
bool decrypt(const std::vector<unsigned char> &key,
	         const std::vector<unsigned char> &ciphertext,
			 std::vector<unsigned char> &plaintext)
{
  //  Verify key size.
  if (8 * key.size() != AES_BITS)
  {
	return false;
  }

  //  Ciphertext (input) length.
  std::size_t in_length = ciphertext.size();

  if (in_length <= 2 * AES_BLOCK_SIZE || in_length > INT_MAX)
  {
	return false;
  }

  in_length -= 2 * AES_BLOCK_SIZE;

  EVP_CIPHER_CTX ctx;
  EVP_CIPHER_CTX_init(&ctx);

  //  Initialise decryption, including setting key and IV from ciphertext.
  if (!EVP_DecryptInit_ex(&ctx, EVP_aes_128_gcm(), 0, 0, 0)
      || !EVP_CIPHER_CTX_ctrl(&ctx, EVP_CTRL_GCM_SET_IVLEN, AES_BLOCK_SIZE, 0)
      || !EVP_DecryptInit_ex(&ctx, 0, 0, &key[0], &ciphertext[0]))
  {
    EVP_CIPHER_CTX_cleanup(&ctx);
	return false;
  }

  //  Ciphertext set to at least size required by following functions.
  plaintext.resize(in_length + AES_BLOCK_SIZE);

  //  Main decryption, using ciphertext after IV. out_length1 is
  //  actual length of this part of plaintext.
  int out_length1;

  if (!EVP_DecryptUpdate(&ctx, &plaintext[0], &out_length1,
	                     &ciphertext[AES_BLOCK_SIZE], (int)in_length))
  {
	plaintext.clear();
    EVP_CIPHER_CTX_cleanup(&ctx);
	return false;
  }

  //  Verify tag (at end of ciphertext) and finish decryption.
  //  out_length2 is actual length of this part of plaintext.
  int out_length2;

  if (!EVP_CIPHER_CTX_ctrl(&ctx, EVP_CTRL_GCM_SET_TAG, AES_BLOCK_SIZE,
	                       (void *)&ciphertext[AES_BLOCK_SIZE + in_length])
      || !EVP_DecryptFinal_ex(&ctx, &plaintext[out_length1], &out_length2))
  {
	plaintext.clear();
    EVP_CIPHER_CTX_cleanup(&ctx);
	return false;
  }

  //  Resize plaintext to correct size, cleanup and exit.
  plaintext.resize(out_length1 + out_length2);
  EVP_CIPHER_CTX_cleanup(&ctx);
  return true;
}
