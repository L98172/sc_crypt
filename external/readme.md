## Third Party Dependencies

This file provides instructions on how to obtain, configure and build the third party dependencies required by the supplied Visual Studio solution.

### Step 1: Obtain the third party dependencies.

A prebuilt version of OpenSSL has been used, but Xerces, Sntiario and Curl required building from source.

#### OpenSSL

https://slproweb.com/products/Win32OpenSSL.html
https://slproweb.com/download/Win32OpenSSL-1_0_1L.exe

#### Apache Xerces

http://xerces.apache.org
http://mirror.ox.ac.uk/sites/rsync.apache.org//xerces/c/3/sources/xerces-c-3.1.1.zip

#### Apache Santuario

http://santuario.apache.org
http://www.apache.org/dyn/closer.cgi?path=/santuario/c-library/xml-security-c-1.7.2.tar.gz

#### Curl

http://curl.haxx.se
http://curl.haxx.se/download/curl-7.40.0.zip

#### Log4cpp (for group-mgr and group-bridge)

http://sourceforge.net/projects/log4cpp/files/log4cpp-1.1.x%20%28new%29/log4cpp-1.1/log4cpp-1.1.2rc1.tar.gz/download


### Step 2: Extract / install the archives.

Extract or install the dependencies into the “external” directory of the supplied Visual Studio solution.The external directory should then have the following top level directories.

* curl-7.40 
* OpenSSL-Win32 
* xerces-c-3.1.1 
* xml-security-c-1.7.2
* log4cpp

### Step 3: Rename directories for Curl and Xerces.

The supplied Visual Studio solution has been configured with paths to curl and OpenSSL which do not include the version string (note this is not the case for xml-security-c-1.7.2). Rename the top-level directories in external to the following.
* curl 
* OpenSSL 
* xerces 
* xml-security-c-1.7.2
* log4cpp

### Step 4: Build Apache Xerces

Navigate to "external\xerces\projects\Win32\VC10\xerces-all"
 Open the "xerces-all.sln" solution, accepting prompts to update to VS2012.
 Change build to "Release" and build solution.

### Step 5: Build Apache Santuario

1. Navigate to "external\xml-security-c-1.7.2\Projects\VC10.0\xsec"
1. Open the "xsec.sln" solution, accepting prompts to update to VS2012.
1. Change build to "Release No Xalan"
1. In the “xsec_lib” project, edit the file “version.rc” and replace the "afxres.h" with “windows.h".
1. In the project properties for each project set C++ "Additional Include Directories" and the Linker “Additional Library Directories” to the include and library paths of OpenSSL and Xerces.
1. Build the solution.

### Step 6: Build Curl

1. Open a command prompt set with the Visual Studio environment (see “vcvarsall.bat” in the visual studio install).
1. On the command prompt, navigate to "external\curl\winbuild"
1. Enter the command "nmake /f Makefile.vc mode=dll" to build.

### Step 7: Build log4cpp (required only for group projects)

1. Navigate to "external\log4cpp\msvc10"
1. Open the "msvc10.sln" solution, accepting prompts to update to latest Visual Studio.
1. Change build to "Release"
1. Build "log4cpp" and "log4cppLIB" projects