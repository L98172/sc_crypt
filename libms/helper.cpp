#include <vector>
#include "helper.h"
#include "sc_types.h"

using namespace std;

// Configures a String descriptor to use data from a string
void ScLibMs::SetStringDescriptor(String &descriptor, const string &s)
{
	descriptor.length = s.size();
	descriptor.pointer = s.data();
}

// Configures a Buffer descriptor to use data from a char vector
void ScLibMs::SetBufferDescriptor(Buffer &descriptor, const vector<uint8_t> &v)
{
	descriptor.length = v.size();
	descriptor.pointer = v.data();
}

// Initialises a vector with the content of a Buffer
void ScLibMs::VectorFromBuffer(vector<uint8_t> &v, const Buffer &b)
{
	v.clear();
	v.insert(v.begin(), b.pointer, b.pointer + b.length);
}

// Converts a 16 character hex string into a 64bit unsigned value.
uint64_t ScLibMs::HexStringToU64(const String &string)
{
	uint64_t value = 0;
	if( string.length != 16)
	{
		// Unexpected string length
		return 0;
	}
	for( uint32_t i = 0; i < 16; ++i)
	{
		uint8_t nibble = string.pointer[i];
		if( nibble >= 'a')
		{
			nibble -= 'a'-10;
		}
		else if( nibble >= 'A')
		{
			nibble -= 'A'-10;
		}
		else if( nibble >= '0')
		{
			nibble -= '0';
		}

		if( nibble > 15)
		{
			// Illegal character in data
			return 0;
		}
		value = (value << 4) + nibble;
	}

	return value;
}

// Converts an unsigned 64bit value into a 16 character hex string.
// Warning: The hex string is returned from local static
// storage. This will be overwritten by subsequent calls.
void ScLibMs::U64ToHexString( String &string, uint64_t value)
{
	// A local static store to return the string value.
	static uint8_t tsarray[16] =
	  {'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0'};

	// Char array of hex string representation of a nibble
	static uint8_t hexchar[16] =
	  {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};

	// Iterate from least significant to most significant nibble
	for( int i = 15; i >= 0; --i)
	{
		// Update string character with ASCII value of nibble
		tsarray[i] = hexchar[ value & 0xF];
		value >>= 4;
	}

	// Update timestamp string to return the value
	string.pointer = reinterpret_cast<const char*>(tsarray);
	string.length = 16;
}

// Appends Buffer contents to an unsigned char vector
void ScLibMs::VectorAppendBuffer(vector<uint8_t> &v, const Buffer &b)
{
	v.insert(v.end(), b.pointer, b.pointer + b.length);
};

// Appends the content of a string to an unsigned char vector
void ScLibMs::VectorAppendString(vector<uint8_t> &v, const string &s)
{
	v.insert(v.end(), s.begin(), s.end());
};

// Appends the content of an unsigned char vector to an unsigned char vector
void ScLibMs::VectorAppendVector(vector<uint8_t> &v1, const vector<uint8_t> &v2)
{
	v1.insert(v1.end(), v2.begin(), v2.end());
};

// Reads len bytes from an unsigned char vector into a string
bool ScLibMs::VectorReadString(const vector<uint8_t> &v,
							   const vector<uint8_t>::const_iterator &index,
							   uint32_t len,
							   string &s)
{
	// Check there is enough data in the vector
	if((index + len) > v.end())
	{
		return false;
	}

	// Copy the content into the string
	s = std::string(index, index + len);
	
	return true;
}

// Reads len bytes from an unsigned char vector into an unsigned char vector
bool ScLibMs::VectorReadVector(const vector<uint8_t> &v,
							   const vector<uint8_t>::const_iterator &index,
							   uint32_t len,
							   vector<uint8_t> &d)
{
	// Check there is enough data in the vector
	if((index + len) > v.end())
	{
		return false;
	}

	// Copy the content into the vector
	d.assign( index, index+len);

	return true;
}

// Checks that the unsigned char vector contains at least n entries from
// the index to the end.
bool ScLibMs::IsDataAvailable(const vector<uint8_t> &data,
							  vector<uint8_t>::const_iterator &index,
							  int32_t n)
{
	return (data.end() - index) >= n;
}
