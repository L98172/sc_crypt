#include <stdint.h>
#include <vector>
#include "sc_types.h"
#include "sc_misc.h"
#include "payload.h"
#include "helper.h"

using namespace std;

/**
* Creates a SAKKE I_MESSAGE from supplied data. The message is not signed.
* V controls if the V bit is set in the HDR payload
* timestamp contains a byte string representation of a UTC-NTP timestamp.
* randlen specifies the length of the random number in bytes. Should be 16 or greater.
* senderUri contains the identity of the sender as a URI.
* recipientUri contains the identity of the receiver as a URI.
* sakkeData contains the SAKKE encrypted data.
* An IDRi payload is only present in the I_MESSAGE if senderURI has non-zero size.
* iMessage will contain the created I_MESSAGE on exit.
* nextPayloadTypeIndex will contain the index for the next payload type upon exit.
*/
void ScLibMs::CreateIMessage(vector<uint8_t> &iMessage,
							 size_t &nextPayloadTypeIndex,
							 bool v,
							 String &timestamp,
							 uint8_t randlen,
							 const string &senderUri,
							 const string &recipientUri,
							 const Buffer &sakkeData)
{
	// create and assemble the I_MESSAGE from various MIKEY payloads
	CreateIMessageWithHdrPayload( iMessage, nextPayloadTypeIndex, v);
	AppendTPayloadToIMessage( iMessage, nextPayloadTypeIndex, timestamp);
	AppendRANDPayloadToIMessage( iMessage, nextPayloadTypeIndex, 16);

	// Append an IDR payload for the sender (initiator) if a sender is specified
	if( senderUri.size() > 0)
	{
		AppendIDRPayloadToIMessage(
			iMessage, nextPayloadTypeIndex,
			MIKEY_PAYLOAD_IDR_TYPE_URI,
			MIKEY_PAYLOAD_IDR_ROLE_IDRI,
			senderUri);
	}

	// Append an IDR payload for the recipient
	AppendIDRPayloadToIMessage(
		iMessage, nextPayloadTypeIndex,
		MIKEY_PAYLOAD_IDR_TYPE_URI,
		MIKEY_PAYLOAD_IDR_ROLE_IDRR,
		recipientUri);

	// Append a SAKKE payload
	AppendSAKKEPayloadToIMessage(
		iMessage, nextPayloadTypeIndex,
		sakkeData);
}

/**
* Creates a GROUP GMK SAKKE I_MESSAGE from supplied data. The message is not
* signed.
*
* Args:
*       iMessage             Pointer to the constructed message
*       nextPayloadTypeIndex Will contain the index for the next payload type
*                            upon exit.
*       V                    Controls if the V bit is set in the HDR payload
*       timestamp            Contains a byte string representation of a UTC-NTP
*                            timestamp.
*       randlen              Specifies the length of the random number in
*                            bytes. Should be 16 or greater.
*       senderUri            Contains the identity of the sender as a URI.
*       recipientUri         Contains the identity of the receiver as a URI.
*       sakkeData            Contains the SAKKE encrypted data.
*
* iMessage will contain the created I_MESSAGE on exit.
*
* Note! Following the lead of CreateIMessage there is no error/ failure return.
*/
void ScLibMs::CreateGmkIMessage(
	vector<uint8_t> &iMessage,
	size_t        &nextPayloadTypeIndex,
	bool             v,
	String          &timestamp,
	uint8_t          randlen,
	const string    &senderUri,
	const string    &recipientUri,
	const Buffer    &sakkeData)
{
	/* create and assemble the I_MESSAGE from various MIKEY payloads */
	CreateIMessageWithHdrPayload(iMessage,
		nextPayloadTypeIndex,
		v,
		ScLibMs::GMKMsgType);
	AppendTPayloadToIMessage(iMessage,
		nextPayloadTypeIndex,
		timestamp);
	AppendRANDPayloadToIMessage(iMessage,
		nextPayloadTypeIndex,
		16);

	/* Refer to Appendix D.2 of 3GPP TS 33.303
	* Version 12.20.0 Release 12 para 5. The
	* IDR_ROLE should be value 3. ID_TYPE
	* field to URI (1).
	*/
	/* Sets role to KMS MIKEY_PAYLOAD_IDR_ROLE_IDRKMS (3). */
	AppendIDRPayloadToIMessage(
		iMessage, nextPayloadTypeIndex,
		MIKEY_PAYLOAD_IDR_TYPE_URI,
		MIKEY_PAYLOAD_IDR_ROLE_IDRKMS,
		senderUri);

	/* Append an IDR payload for the recipient */
	AppendIDRPayloadToIMessage(
		iMessage, nextPayloadTypeIndex,
		MIKEY_PAYLOAD_IDR_TYPE_URI,
		MIKEY_PAYLOAD_IDR_ROLE_IDRI, /* See TS 33.303 v12..0 Rel 12 Section D.2
									 * para 5
									 */
									 recipientUri);

	/* Append a SAKKE payload */
	AppendSAKKEPayloadToIMessage(
		iMessage, nextPayloadTypeIndex,
		sakkeData);
} /* CreateGmkIMessage */

/**
* Creates a GROUP GSK SAKKE I_MESSAGE from supplied data. The message is not signed.
*
* Args:
*       iMessage             Pointer to the constructed message
*       nextPayloadTypeIndex Will contain the index for the next payload type
*                            upon exit.
*       v                    Controls if the v bit is set in the HDR payload
*       timestamp            Contains a byte string representation of a UTC-NTP
*                            timestamp.
*       randlen              Specifies the length of the random number in
*                            bytes. Should be 16 or greater.
*       senderUri            Contains the identity of the sender as a URI.
*       recipientUri         Contains the identity of the receiver as a URI.
*
* KEMAC is not added at this stage the caller will add it.
* iMessage will contain the created I_MESSAGE on exit.
* Note! Following the lead of CreateIMessage there is no error/ failure return.
*/
void ScLibMs::CreateGskIMessage(
	vector<uint8_t> &iMessage,
	size_t        &nextPayloadTypeIndex,
	bool             v,
	String          &timestamp,
	uint8_t          randlen,
	const string    &senderUri,
	const string    &recipientUri)
{
	/* Add header */
	CreateIMessageWithHdrPayload(iMessage,
		nextPayloadTypeIndex,
		v,
		ScLibMs::GSKMsgType);

	/* Time must ABSOLUTELY NOT be removed - required for GSK AES
	* encryption and ending a loop checking partially built iMessage
	* see CreateMikeySakkeGSKIMessage.
	*/
	AppendTPayloadToIMessage(iMessage, nextPayloadTypeIndex, timestamp);

	AppendRANDPayloadToIMessage(iMessage, nextPayloadTypeIndex, 16);

	/* Refer to Appendix D.2 of 3GPP TS 33.303
	* Version 12.20.0 Release 12 para 5. The
	* IDR_ROLE should be value 3. ID_TYPE
	* field to URI (1).
	*/
	AppendIDRPayloadToIMessage(
		iMessage, nextPayloadTypeIndex,
		MIKEY_PAYLOAD_IDR_TYPE_URI,
		MIKEY_PAYLOAD_IDR_ROLE_IDRI,
		senderUri);

	/* Append an IDR payload for the recipient. */
	AppendIDRPayloadToIMessage(
		iMessage, nextPayloadTypeIndex,
		MIKEY_PAYLOAD_IDR_TYPE_URI,
		MIKEY_PAYLOAD_IDR_ROLE_IDRR,
		recipientUri);

	/* KEMAC added by caller after this first part of construction done.
	* This is because the AES encryption needs the CSB-ID and timestamp
	* from this part of the message construction.
	*/

} /* CreateGskIMessage */

/**
* Creates a GROUP MC PTT SAKKE I_MESSAGE from supplied data. The message
* is not signed.
*
* Args:
*       iMessage             Pointer to the constructed message
*       nextPayloadTypeIndex Will contain the index for the next payload
*                            type upon exit.
*       v                    Controls if the v bit is set in the HDR payload
*       timestamp            Contains a byte string representation of a
*                            UTC-NTP timestamp.
*       randlen              Specifies the length of the random number in
*                            bytes. Should be 16 or greater.
*       senderUri            Contains the identity of the sender as a URI.
*       recipientUri         Contains the identity of the receiver as a URI.
*       sakkeData            Contains the SAKKE encrypted data.
*
* iMessage will contain the created I_MESSAGE on exit.
* Note! Following the lead of CreateIMessage there is no error/ failure
* return.
*/
void ScLibMs::CreateMcPttIMessage(
	vector<uint8_t> &iMessage,
	size_t        &nextPayloadTypeIndex,
	bool             v,
	String          &timestamp,
	uint8_t          randlen,
	const string    &senderUri,
	const string    &recipientUri,
	const Buffer    &sakkeData)
{
	/* Add Header */
	CreateIMessageWithHdrPayload(iMessage,
		nextPayloadTypeIndex,
		v,
		ScLibMs::MCPTTMsgType);

	AppendTPayloadToIMessage(iMessage, nextPayloadTypeIndex, timestamp);

	AppendRANDPayloadToIMessage(iMessage, nextPayloadTypeIndex, 16);

	/* Refer to Appendix D.2 of 3GPP TS 33.303
	* Version 12.20.0 Release 12 para 5. The
	* IDR_ROLE should be value 3. ID_TYPE
	* field to URI (1).
	*/

	/* Sets role to KMS MIKEY_PAYLOAD_IDR_ROLE_IDRKMS (3). */
	AppendIDRPayloadToIMessage(
		iMessage,
		nextPayloadTypeIndex,
		MIKEY_PAYLOAD_IDR_TYPE_URI,
		MIKEY_PAYLOAD_IDR_ROLE_IDRKMS,
		senderUri);

	/* Append an IDR payload for the recipient */
	AppendIDRPayloadToIMessage(
		iMessage, nextPayloadTypeIndex,
		MIKEY_PAYLOAD_IDR_TYPE_URI,
		MIKEY_PAYLOAD_IDR_ROLE_IDRR,
		recipientUri);

	/* Append a SAKKE payload */
	AppendSAKKEPayloadToIMessage(
		iMessage, nextPayloadTypeIndex,
		sakkeData);
} /* CreateMcPttIMessage */

/**
* Creates an I_MESSAGE with an HDR payload
* V controls if the V bit is set in the HDR payload
* iMessage will contain the HDR payload upon exit
* nextPayloadTypeIndex will contain the index for the next payload type upon exit
*/
void ScLibMs::CreateIMessageWithHdrPayload(vector<uint8_t> &iMessage,
										   size_t &nextPayloadTypeIndex,
										   bool v,
										   const uint8_t &hdrMsgType)
{
	Buffer bufferDescriptor;
	uint8_t version = 1;
	uint8_t type = 26;
	uint8_t nextPayload = MIKEY_PAYLOAD_NEXT_PAYLOAD_LAST;
	uint8_t PRFfunc;
	uint32_t CSBID = 0;
	uint8_t NCS = 0;
	uint8_t CSIDMapType = 0;
	uint16_t CSIDMapInfo = 0;

	// Create PRF function byte
	if( v)
	{
		PRFfunc = 0x80; // Secure Chorus changes RFC6509 V bit to be optionally set.
	}
	else
	{
		PRFfunc = 0x0;
	}

	// Create a random CSBID id required.
	switch (hdrMsgType) {
	case (ScLibMs::GMKMsgType) :     /* Leave empty */
		break;
	case (ScLibMs::defaultMsgType) : /* Standard - existing p2p */
	case (ScLibMs::GSKMsgType) :     /* GSK (Group Session Key) */
	case (ScLibMs::MCPTTMsgType) :   /* PTT (proposal)          */
	default:
		bufferDescriptor.length = sizeof(CSBID);
		bufferDescriptor.pointer = reinterpret_cast<const uint8_t *>(&CSBID);
		GetSecureRandom(bufferDescriptor);
		break;
	}

	// Initialise I Message
	iMessage.clear();

	// Put HDR values into iMessage
	VectorAppend( iMessage, version);
	VectorAppend( iMessage, type);
	VectorAppend( iMessage, nextPayload);
	VectorAppend( iMessage, PRFfunc);
	VectorAppend( iMessage, htonl(CSBID));
	VectorAppend( iMessage, NCS);
	VectorAppend( iMessage, CSIDMapType);
	VectorAppend( iMessage, htons(CSIDMapInfo));

	// Set next payload index
	nextPayloadTypeIndex = 2;
}

/**
* Creates and appends a MIKEY T payload to an I_MESSAGE
* timestampString contains a hex string representation of a UTC-NTP timestamp
* iMessage will be updated by appending a T payload
* nextPayloadTypeIndex will contain the index for the next payload type upon exit
*/
void ScLibMs::AppendTPayloadToIMessage(vector<uint8_t> &iMessage,
									   size_t &nextPayloadTypeIndex,
									   String &timestampString)
{
	uint8_t nextPayload = MIKEY_PAYLOAD_NEXT_PAYLOAD_LAST;
	uint8_t TStype;
	uint64_t TSvalue;

	// Update next payload
	iMessage[nextPayloadTypeIndex] = MIKEY_PAYLOAD_NEXT_PAYLOAD_T;
	nextPayloadTypeIndex = iMessage.size();

	// Create NTP-UTP timestamp
	TStype = MIKEY_PAYLOAD_T_TYPE_NTP_UTC;
	TSvalue = HexStringToU64( timestampString);

	// Append the payload to the message
	VectorAppend( iMessage, nextPayload);
	VectorAppend( iMessage, TStype);
	VectorAppend( iMessage, htonll(TSvalue));
}

/**
* Creates and appends a MIKEY RAND payload to an I_MESSAGE
*	randlen specifies the length of the random number in bytes. Should be 16 or greater.
*	iMessage will be updated by appending a RAND payload
*	nextPayloadTypeIndex will contain the index for the next payload type upon exit
*/
void ScLibMs::AppendRANDPayloadToIMessage(vector<uint8_t> &iMessage,
										  size_t &nextPayloadTypeIndex,
										  uint8_t randlen)
{
	vector<uint8_t> rand(randlen);
	uint8_t nextPayload = MIKEY_PAYLOAD_NEXT_PAYLOAD_LAST;
	Buffer bufferDescriptor;

	// Update next payload
	iMessage[nextPayloadTypeIndex] = MIKEY_PAYLOAD_NEXT_PAYLOAD_RAND;
	nextPayloadTypeIndex = iMessage.size();

	// Create RAND data
	bufferDescriptor.length = rand.size();
	bufferDescriptor.pointer = rand.data();
	GetSecureRandom( bufferDescriptor);

	// Append the payload to the message
	VectorAppend( iMessage, nextPayload);
	VectorAppend( iMessage, randlen);
	VectorAppendVector( iMessage, rand);
}

/**
* Creates and appends a MIKEY RAND payload to an I_MESSAGE
* type specifies the ID type
* role specifies the ID role
* identity contains the identity
* iMessage will be updated by appending a IDR payload
* nextPayloadTypeIndex will contain the index for the next payload type upon exit
*/
void ScLibMs::AppendIDRPayloadToIMessage(vector<uint8_t> &iMessage,
										 size_t &nextPayloadTypeIndex,
										 const uint8_t type,
										 const uint8_t role,
										 const string &identity)
{
	uint8_t nextPayload = MIKEY_PAYLOAD_NEXT_PAYLOAD_LAST;
	
	if (identity.size() > UINT16_MAX)
	{
		return;
	}
	uint16_t length = (uint16_t)identity.size();

	// Update next payload
	iMessage[nextPayloadTypeIndex] = MIKEY_PAYLOAD_NEXT_PAYLOAD_IDR;
	nextPayloadTypeIndex = iMessage.size();

	// Append the payload to the message
	VectorAppend( iMessage, nextPayload);
	VectorAppend( iMessage, role);
	VectorAppend( iMessage, type);
	VectorAppend( iMessage, htons(length));
	VectorAppendString( iMessage, identity);
}

/**
 * Creates and appends a KEMAC payload to an I_MESSAGE.
 *	encryptionAlgorithm specifies the algrithmn used to encrypt the GSK
 *	encryptedData The encrypted GSK
 *	encryptedData The AES salt
 *	macAlgorithm specifies the MAC algorithm
 *	macData The MAC itself
 *	iMessage will be updated by appending a KEMAC payload
 *	nextPayloadTypeIndex will contain the index for the next payload type upon exit
 */

void ScLibMs::AppendKEMACPayloadToIMessage(
	std::vector<uint8_t> &iMessage,
	size_t             &nextPayloadTypeIndex,
	uint8_t               encryptionAlgorithm,
	const Buffer         &encryptedData,
	const Buffer          salt)
{
	uint8_t nextPayload = MIKEY_PAYLOAD_NEXT_PAYLOAD_LAST;
	uint8_t typeAndKV = 0;

	// Update next payload
	iMessage[nextPayloadTypeIndex] = MIKEY_PAYLOAD_NEXT_PAYLOAD_KEMAC;
	nextPayloadTypeIndex = iMessage.size();

	// Append the payload to the message
	VectorAppend(iMessage, nextPayload);

	int c = 0;
	VectorAppend(iMessage, (uint8_t)encryptionAlgorithm);

	VectorAppend(iMessage, htons(6 // nextPayload, typeAndKV and two lengths */
		+ (unsigned short)salt.length + (unsigned short)encryptedData.length));

	/**********************************************************************/
	/* Add in Encrypted GSK data sub payload                              */
	/**********************************************************************/
	/* Type options are:
	*     0 TGK
	*     1 TGK+SALT
	*     2 TEK
	*     3 TEK+SALT
	* I would use TGK+SALT, however TS 33.303 Appendix D paragraph 8
	* states - "the Type subfield shall be set to TGK (value 0)."
	*
	* KV options are:
	*     0 No specific usage
	*     1 SPI
	*     2 Interval
	* We will use 0.
	*/
	VectorAppend(iMessage, nextPayload); /* LAST */

	/* See RFC 3830 Section 6.13 (Key Data sub payload) */
	typeAndKV = 0x00;
	VectorAppend(iMessage, typeAndKV);
	VectorAppend(iMessage, htons((unsigned short)encryptedData.length));
	VectorAppendBuffer(iMessage, encryptedData);

	VectorAppend(iMessage, htons((unsigned short)salt.length));
	VectorAppendBuffer(iMessage, salt);

	/* No KV (optional) */
}

void ScLibMs::AppendMACPayloadToIMessage(
	std::vector<uint8_t> &iMessage,
	size_t             &nextPayloadTypeIndex,
	uint8_t               macAlgorithm,
	const Buffer         &mac)
{
	VectorAppend(iMessage, macAlgorithm);
	VectorAppendBuffer(iMessage, mac);
}


/**
* Creates and appends a MIKEY SAKKE payload to an I_MESSAGE
* sakke contains the SAKKE data
* iMessage will be updated by appending a SAKKE payload
* nextPayloadTypeIndex will contain the index for the next payload type upon exit
*/
void ScLibMs::AppendSAKKEPayloadToIMessage(vector<uint8_t> &iMessage,
										   size_t &nextPayloadTypeIndex,
										   const Buffer &sakke)
{
	uint8_t nextPayload = MIKEY_PAYLOAD_NEXT_PAYLOAD_LAST;
	uint8_t params = 1;     // Specify using SAKKE parameter set 1. rfc6509.
	uint8_t idScheme = 1;	// Using tel URI with monthly keys scheme

	// Update next payload
	iMessage[nextPayloadTypeIndex] = MIKEY_PAYLOAD_NEXT_PAYLOAD_SAKKE;
	nextPayloadTypeIndex = iMessage.size();

	// Append the payload to the message
	VectorAppend( iMessage, nextPayload);
	VectorAppend( iMessage, params);
	VectorAppend( iMessage, idScheme);
	VectorAppend( iMessage, htons((unsigned short)(sakke.length)));
	VectorAppendBuffer( iMessage, sakke);
}

/**
* Creates and appends a MIKEY SAKKE payload to an I_MESSAGE.
* This payload must be the last in an I_MESSAGE.
* type indicates the signature type. Only lower 4 bits are used as S type.
* signature contains the I_MESSAGE signature
* iMessage will be updated by appending a SIGN payload
*/
void ScLibMs::AppendSIGNPayloadToIMessage(vector<uint8_t> &iMessage,
										  size_t nextPayloadTypeIndex,
										  uint8_t type,
										  const Buffer &signature)
{
	uint8_t nextPayload = MIKEY_PAYLOAD_NEXT_PAYLOAD_LAST;
	uint16_t typeAndLength = 0; // 4 bits type and 12 bits length

	// Note: Next payload type set before signing.

	typeAndLength = (type & 0x0F);
	typeAndLength = (typeAndLength << 12) | (signature.length & 0x0FFF);

	// Append the payload to the message
	VectorAppend( iMessage, htons(typeAndLength));
	VectorAppendBuffer( iMessage, signature);
}

/**
* Parses an I_MESSAGE HDR payload.
* iMessage is the vector containing the payload to parse.
* index is the index of the start of the payload to parse within the iMessage.
* Returns true if parsing is successful, otherwise false.
* Upon successful exit:
*   index will be updated to point to the start of the following payload.
*   nextPayloadTypeIndex will be updated to point to the next payload type.
*   csbid will contain the CSBID extracted from the payload.
*   v will be set to the state of the HDR payload V bit.
*/
bool ScLibMs::ProcessHDRPayload(const vector<uint8_t> &iMessage,
								vector<uint8_t>::const_iterator &index,
								vector<uint8_t>::const_iterator &nextPayloadTypeIndex,
								uint32_t &csbid,
								bool &v,
								uint8_t &csMapType)
{
	uint8_t version;
	uint8_t type;
	uint8_t PRFfunc;

	// Check the message has enough content
	if( !IsDataAvailable( iMessage, index, 12))
	{
		return false;
	}

	// Read values
	VectorRead( iMessage, index + 0, version);
	VectorRead( iMessage, index + 1, type);

	// Check values
	if( version != 0x1 || type != MIKEY_PAYLOAD_HDR_TYPE_SAKKE)
	{
		return false;
	}

	// Read values
	VectorRead( iMessage, index + 3, PRFfunc);
	if( PRFfunc & 0x80)
	{
		v = true;
	}
	else
	{
		v = false;
	}
	VectorRead( iMessage, index + 4, csbid);
	csbid = ntohl( csbid);

	VectorRead(iMessage, index + 9, csMapType);

	// Update iterators
	nextPayloadTypeIndex = index + 2;
	index += 12;

	return true;
};

/**
* Parses an I_MESSAGE T payload.
* iMessage is the vector containing the payload to parse.
* index is the index of the start of the payload to parse within the iMessage.
* Returns true if parsing is successful, otherwise false.
* Upon successful exit:
*   index will be updated to point to the start of the following payload.
*   nextPayloadTypeIndex will be updated to point to the next payload type.
*   t will contain the NTP UTC timestamp extracted from the payload.
*/
bool ScLibMs::ProcessTPayload(const vector<uint8_t> &iMessage,
							  vector<uint8_t>::const_iterator &index,
							  vector<uint8_t>::const_iterator &nextPayloadTypeIndex,
							  uint64_t &t)
{
	uint8_t type;

	// Check the message has enough content
	if( !IsDataAvailable( iMessage, index, 10))
	{
		return false;
	}

	// Read values
	VectorRead( iMessage, index + 1, type);

	// Check values. Only support NTP_UTC time.
	if( type != MIKEY_PAYLOAD_T_TYPE_NTP_UTC)
	{
		return false;
	}

	// Read values
	VectorRead( iMessage, index + 2, t);
	t = ntohll( t);

	// Update iterators
	nextPayloadTypeIndex = index;
	index += 10;

	return true;
}

/**
* Parses an I_MESSAGE RAND payload.
* iMessage is the vector containing the payload to parse.
* index is the index of the start of the payload to parse within the iMessage.
* Returns true if parsing is successful, otherwise false.
* Upon successful exit:
*   index will be updated to point to the start of the following payload.
*   nextPayloadTypeIndex will be updated to point to the next payload type.
*/
bool ScLibMs::ProcessRANDPayload(const vector<uint8_t> &iMessage,
	vector<uint8_t>::const_iterator &index,
	vector<uint8_t>::const_iterator &nextPayloadTypeIndex)
{
	vector<uint8_t> rndNum; /* dummy variable */
	return ProcessRANDPayload(iMessage, index, nextPayloadTypeIndex, rndNum);
}

bool ScLibMs::ProcessRANDPayload(const vector<uint8_t> &iMessage,
								 vector<uint8_t>::const_iterator &index,
								 vector<uint8_t>::const_iterator &nextPayloadTypeIndex,
								 vector<uint8_t>                 &rndNum)
{
	uint8_t length;

	// Check the message has enough content to start
	if( !IsDataAvailable( iMessage, index, 2))
	{
		return false;
	}

	// Read values
	VectorRead( iMessage, index + 1, length);

	// Is there enough room for the random array
	if( !IsDataAvailable( iMessage, index, 2 + length))
	{
		return false;
	}

	if (length < 16) /* Check identified RFC 3830 section 6.11 */
	{
		return false;
	}

	/* Read values */
	VectorReadVector(iMessage, index + 4, length, rndNum);

	// Update iterators
	nextPayloadTypeIndex = index;
	index += 2 + length;

	return true;
}

/**
* Parses an I_MESSAGE IDR payload.
* iMessage is the vector containing the payload to parse.
* index is the index of the start of the payload to parse within the iMessage.
* Returns true if parsing is successful, otherwise false.
* Upon successful exit:
*   index will be updated to point to the start of the following payload.
*   nextPayloadTypeIndex will be updated to point to the next payload type.
*   role will contain the IDR role extracted from the payload.
*   identity will contain the identity string extracted from the payload.
*/
bool ScLibMs::ProcessIDRPayload(const vector<uint8_t> &iMessage,
								vector<uint8_t>::const_iterator &index,
								vector<uint8_t>::const_iterator &nextPayloadTypeIndex,
								uint8_t &role, string &identity)
{
	uint8_t type;
	uint16_t length;

	// Check the message has enough content to start
	if( !IsDataAvailable( iMessage, index, 5))
	{
		return false;
	}

	// Read values
	VectorRead( iMessage, index + 1, role);
	VectorRead( iMessage, index + 2, type);
	VectorRead( iMessage, index + 3, length);
	length = ntohs( length);

	// Check values
	if( type != MIKEY_PAYLOAD_IDR_TYPE_URI)
	{
		return false;
	}

	// Is there enough room for the identity string
	if( !IsDataAvailable( iMessage, index, length + 5))
	{
		return false;
	}

	// Read values
	VectorReadString( iMessage, index + 5, length, identity);

	// Update iterators
	nextPayloadTypeIndex = index;
	index += 5 + length;

	return true;
}

/**
* Parses an I_MESSAGE KEMAC payload.
* iMessage is the vector containing the payload to parse.
* index is the index of the start of the payload to parse within the iMessage.
*
* Args:
*       iMessage             The received message.
*       index                Will be updated to point to the start of the
*                            following payload.
*       nextPayloadTypeIndex Will be updated to point to the next payload type.
*       kemacEncType         KEMAC Encryption Type. Only one should be defined
*                            according to 3GPP TS 33.303 AES-CM-128
*       kemacEncData         KEMAC Encryption data (the encrypted GSK).
*       kemacEncType         KEMAC MAC Type. Only one should be defined
*                            according to 3GPP TS 33.303 HASH-sHA-256-256
*       kemacEncType         KEMAC MAC.
* Returns:
*       bool                 true/ false for success/ failurei of parse.
*/
bool ScLibMs::ProcessKEMACPayload(
	const vector<uint8_t>           &iMessage,
	vector<uint8_t>::const_iterator &index,
	vector<uint8_t>::const_iterator &nextPayloadTypeIndex,
	uint8_t                         &kemacEncType,
	vector<uint8_t>                 &kemacEnc,
	vector<uint8_t>                 &kemacSalt,
	uint8_t                         &kemacMacType,
	vector<uint8_t>                 &kemacMac)
{
	uint16_t encLength;
	uint8_t  nextPayload; /* should be last */
	uint8_t  typeAndKv;
	uint16_t dataLen;
	uint16_t kemacSaltLen;

	// Check the message has enough content to start
	if (!IsDataAvailable(iMessage, index, 8))
	{
		return false;
	}

	/* Read values */
	nextPayloadTypeIndex = index;

	index += 1;
	VectorRead(iMessage, index, kemacEncType);

	index += 1;
	VectorRead(iMessage, index, encLength);
	encLength = ntohs(encLength);

	/* Read sub payload - see RFC 3830 Section 6.13 */
	/* Encrypted GSK */
	index += 2;
	VectorRead(iMessage, index, nextPayload);

	index += 1;
	VectorRead(iMessage, index, typeAndKv);

	/* Encrypted Key */
	index += 1;
	VectorRead(iMessage, index, dataLen);
	dataLen = ntohs(dataLen);
	index += 2;
	VectorReadVector(iMessage, index, dataLen, kemacEnc);

	/* Salt */
	index += dataLen;
	VectorRead(iMessage, index, kemacSaltLen);
	kemacSaltLen = ntohs(kemacSaltLen);

	index += 2;
	VectorReadVector(iMessage, index, kemacSaltLen, kemacSalt);

	index += kemacSaltLen;
	VectorRead(iMessage, index, kemacMacType);

	/* Only one MAC type defined in 3GPP TS 33.303 and that's
	* HMAC SHA-256-256. So, length is 32 bytes.
	*/
	index += 1;
	VectorReadVector(iMessage, index, 32, kemacMac);

	index += 32;

	return true;
} /* ProcessKEMACPayload */

/**
* Parses an I_MESSAGE SAKKE payload.
* iMessage is the vector containing the payload to parse.
* index is the index of the start of the payload to parse within the iMessage.
* Returns true if parsing is successful, otherwise false.
* Upon successful exit:
*   index will be updated to point to the start of the following payload.
*   nextPayloadTypeIndex will be updated to point to the next payload type.
*   sakke will contain the SAKKE encrypted data extracted from the payload.
*/
bool ScLibMs::ProcessSAKKEPayload(const vector<uint8_t> &iMessage,
								  vector<uint8_t>::const_iterator &index,
								  vector<uint8_t>::const_iterator &nextPayloadTypeIndex,
								  vector<uint8_t> &sakke)
{
	uint8_t params;
	uint8_t scheme;
	uint16_t length;

	// Check the message has enough content to start
	if( !IsDataAvailable( iMessage, index, 5))
	{
		return false;
	}

	// Read values
	VectorRead( iMessage, index + 1, params);
	VectorRead( iMessage, index + 2, scheme);
	VectorRead( iMessage, index + 3, length);
	length = ntohs( length);

	// Check values
	if( params != 1 || scheme != 1)
	{
		return false;
	}

	// Is there enough room for the identity string
	if( !IsDataAvailable( iMessage, index, length + 5))
	{
		return false;
	}

	// Read values
	VectorReadVector( iMessage, index + 5, length, sakke);

	// Update iterators
	nextPayloadTypeIndex = index;
	index += 5 + length;

	return true;
}

/**
* Parses an I_MESSAGE SIGN payload.
* iMessage is the vector containing the payload to parse.
* index is the index of the start of the payload to parse within the iMessage.
* Returns true if parsing is successful, otherwise false.
* Upon successful exit:
*   index will be updated to point to the start of the following payload, which
*   should normally be the end of the iMessage.
*   signature will contain the SAKKE encrypted data extracted from the payload.
*/
bool ScLibMs::ProcessSIGNPayload(const vector<uint8_t> &iMessage,
								 vector<uint8_t>::const_iterator &index,
								 vector<uint8_t>::const_iterator &nextPayloadTypeIndex,
								 vector<uint8_t> &signature)
{
	uint8_t type;
	uint16_t length;

	// Check the message has enough content to start
	if( !IsDataAvailable( iMessage, index, 2))
	{
		return false;
	}

	// Read values
	VectorRead( iMessage, index, length);
	length = ntohs( length);
	type = (length >> 12) & 0x000F;
	length &= 0x0FFF;

	// Check values
	if( type != MIKEY_PAYLOAD_SIGN_TYPE_ECCSI)
	{
		return false;
	}

	// Is there enough room for the signature data
	if( !IsDataAvailable( iMessage, index, length + 2))
	{
		return false;
	}

	// Read values
	VectorReadVector( iMessage, index + 2, length, signature);

	// Update iterators
	index += 2 + length;

	return true;
}


