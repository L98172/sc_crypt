#ifndef SC_LIBMS_HELPER_H
#define SC_LIBMS_HELPER_H

// Includes
#include <string>
#include <vector>
#include <stdint.h>
#include "sc_errno.h"

// Forward declarations
struct Buffer;
struct String;

// The secure Chorus namespace
namespace ScLibMs
{
	// Appends the value to an unsigned char vector
	template<typename T>
	  void VectorAppend(std::vector<uint8_t> &v, const T &value)
	{
		v.insert(v.end(), reinterpret_cast<const uint8_t *>(&value),
			     reinterpret_cast<const uint8_t *> (&value + 1));
	};

	// Configures a String descriptor to use data from a string
	void SetStringDescriptor(String &descriptor, const std::string &s);

	// Configures a Buffer descriptor to use data from a char vector
	void SetBufferDescriptor(Buffer &descriptor, const std::vector<uint8_t> &v);

	// Initialises a vector with the content of a Buffer
	void VectorFromBuffer(std::vector<uint8_t> &v, const Buffer &b);

	// Appends Buffer contents to an unsigned char vector
	void VectorAppendBuffer(std::vector<uint8_t> &v, const Buffer &b);

	// Appends the content of a string to an unsigned char vector
	void VectorAppendString(std::vector<uint8_t> &v, const std::string &s);

	// Appends the content of an unsigned char vector to an unsigned char vector
	void VectorAppendVector(std::vector<uint8_t> &v1,
		                    const std::vector<uint8_t> &v2);

	// Converts a 16 character hex string into a 64 bit unsigned value.
	uint64_t HexStringToU64(const String &string);

	// Converts an unsigned 64bit value into a 16 character hex string.
	void U64ToHexString(String &string, uint64_t value);

	// Reads a value from an unsigned char vector
	template<typename T>
 	  bool VectorRead(const std::vector<uint8_t> &v,
		              const std::vector<uint8_t>::const_iterator &index, T &value)
	{
		const uint8_t* p = (const uint8_t*)&(*index);

		// Check there is enough data in the vector
		if (index + sizeof(T) > v.end())
		{
			return false;
		}

		for (int i = 0; i < sizeof(T); ++i) {
			memcpy((uint8_t *)&value + i, p + i, 1);
		}

		return true;
	};

	// Reads len bytes from an unsigned char vector into a string
	bool VectorReadString(const std::vector<uint8_t> &v,
		                  const std::vector<uint8_t>::const_iterator &index,
		                  uint32_t len, std::string &s);

	// Reads len bytes from an unsigned char vector into an unsigned char vector
	bool VectorReadVector(const std::vector<uint8_t> &v,
		                  const std::vector<uint8_t>::const_iterator &index,
		                  uint32_t len, std::vector<uint8_t> &d);

	// Checks that the unsigned char vector contains at least n entries from
	// the index to the end.
	bool IsDataAvailable(const std::vector<uint8_t> &data,
		                 std::vector<uint8_t>::const_iterator &index,
		                 int32_t n);
}

#endif SC_LIBMS_HELPER_H