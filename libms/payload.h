#ifndef SC_LIBMS_PAYLOAD_H
#define SC_LIBMS_PAYLOAD_H

// Includes
#include <string>
#include <vector>
#include <stdint.h>
#include "sc_errno.h"
#include "sc_libms.h"

// Define the network to/from host functions ourself to avoid importing Winsock

#define SWAP_LONGLONG(x) \
	((((x) >> 56) & 0x00000000000000FFLL) ^\
     (((x) >> 40) & 0x000000000000FF00LL) ^\
     (((x) >> 24) & 0x0000000000FF0000LL) ^\
     (((x) >> 8)  & 0x00000000FF000000LL) ^\
     (((x) << 8)  & 0x000000FF00000000LL) ^\
     (((x) << 24) & 0x0000FF0000000000LL) ^\
     (((x) << 40) & 0x00FF000000000000LL) ^\
     (((x) << 56) & 0xFF00000000000000LL))

#define SWAP_LONG(x) \
   ((((x) >> 24) & 0x000000FFL) ^\
	(((x) >> 8)  & 0x0000FF00L) ^\
	(((x) << 8)  & 0x00FF0000L) ^\
	(((x) << 24) & 0xFF000000L))

#define SWAP_SHORT(x) \
	((((x) >> 8) & (short)0x00FF) ^ (((x) << 8) & (short)0xFF00))

inline unsigned long long ntohll(unsigned long long x)
{
	const unsigned long long ret = SWAP_LONGLONG(x);
	return ret;
}
inline unsigned long long htonll(unsigned long long x)
{
	const unsigned long long ret = SWAP_LONGLONG(x);
	return ret;
}
inline unsigned long ntohl(unsigned long x)
{
	const unsigned long ret = SWAP_LONG(x);
	return ret;
}
inline unsigned long htonl(unsigned long x)
{
	const unsigned long ret = SWAP_LONG(x);
	return ret;
}
inline unsigned short ntohs(unsigned short x)
{
	const unsigned short ret = SWAP_SHORT(x);
	return ret;
}
inline unsigned short htons(unsigned short x)
{
	const unsigned short ret = SWAP_SHORT(x);
	return ret;
}

// Forward declarations
struct Buffer;
struct String;

// Bitfield for checking which payloads are included in an I_MESSAGE
#define PAYLOAD_HAS_RAND	(uint8_t) 1
#define PAYLOAD_HAS_T		(uint8_t) 2
#define PAYLOAD_HAS_IDRI	(uint8_t) 4
#define PAYLOAD_HAS_IDRR	(uint8_t) 8
#define PAYLOAD_HAS_SAKKE	(uint8_t) 16
#define PAYLOAD_HAS_SIGN	(uint8_t) 32

// The secure Chorus namespace
namespace ScLibMs
{
	// Helper functions that manipulate MIKEY payloads and I_MESSAGES
	// and related definitions

	// Define useful constants
	static const uint8_t MIKEY_PAYLOAD_NEXT_PAYLOAD_LAST = 0;
	static const uint8_t MIKEY_PAYLOAD_NEXT_PAYLOAD_KEMAC = 1;
	static const uint8_t MIKEY_PAYLOAD_NEXT_PAYLOAD_SIGN = 4;
	static const uint8_t MIKEY_PAYLOAD_NEXT_PAYLOAD_T = 5;
	static const uint8_t MIKEY_PAYLOAD_NEXT_PAYLOAD_RAND = 11;
	static const uint8_t MIKEY_PAYLOAD_NEXT_PAYLOAD_IDR = 14;
	static const uint8_t MIKEY_PAYLOAD_NEXT_PAYLOAD_SAKKE = 26;

	static const uint8_t MIKEY_PAYLOAD_HDR_TYPE_SAKKE = 26;

	static const uint8_t MIKEY_PAYLOAD_T_TYPE_NTP_UTC = 0;
	static const uint8_t MIKEY_PAYLOAD_T_TYPE_NTP = 1;
	static const uint8_t MIKEY_PAYLOAD_T_TYPE_COUNTER = 2;

	static const uint8_t MIKEY_PAYLOAD_IDR_TYPE_NAI = 0;
	static const uint8_t MIKEY_PAYLOAD_IDR_TYPE_URI = 1;

	static const uint8_t MIKEY_PAYLOAD_IDR_ROLE_IDRI = 1;    /* RFC 6043 Section ID Role Table */
	static const uint8_t MIKEY_PAYLOAD_IDR_ROLE_IDRR = 2;    /* RFC 6043 Section ID Role Table */

	static const uint8_t MIKEY_PAYLOAD_IDR_ROLE_IDRKMS = 3;  /* RFC 6043 Section ID Role Table */
	static const uint8_t MIKEY_PAYLOAD_IDR_ROLE_IDRPSK = 4;  /* RFC 6043 Section ID Role Table */
	static const uint8_t MIKEY_PAYLOAD_IDR_ROLE_IDRAPP = 5;  /* RFC 6043 Section ID Role Table */

	static const uint8_t MIKEY_PAYLOAD_IDR_ROLE_IDRKMSI = 6; /* RFC 6509 Section 4.4 */
	static const uint8_t MIKEY_PAYLOAD_IDR_ROLE_IDRKMSR = 7; /* RFC 6509 Section 4.4 */

	static const uint8_t MIKEY_PAYLOAD_SIGN_TYPE_RSA_PKCS = 0;
	static const uint8_t MIKEY_PAYLOAD_SIGN_TYPE_RSASSA_PSS = 1;
	static const uint8_t MIKEY_PAYLOAD_SIGN_TYPE_ECCSI = 2;
	static const uint64_t MIKEY_MESSAGE_MAX_TIME_ERROR = (5 * 60ll) << 32; // 5 minutes

	/**
	* Creates a SAKKE I_MESSAGE from supplied data. The message is not signed.
	* V controls if the V bit is set in the HDR payload
	* timestamp contains a byte string representation of a UTC-NTP timestamp.
	* randlen specifies the length of the random number in bytes. Should be 16 or greater.
	* senderUri contains the identity of the sender as a URI.
	* recipientUri contains the identity of the receiver as a URI.
	* sakkeData contains the SAKKE encrypted data.
	* An IDRi payload is only present in the I_MESSAGE if senderURI has non-zero size.
	* iMessage will contain the created I_MESSAGE on exit.
	* nextPayloadTypeIndex will contain the index for the next payload type upon exit.
	*/
	void CreateIMessage(std::vector<uint8_t> &iMessage, size_t &nextPayloadTypeIndex,
		bool v, String &timestamp, uint8_t randlen, const std::string &senderUri,
		const std::string &recipientUri, const Buffer &sakkeData);

	/**
	* Creates a GROUP GMK SAKKE I_MESSAGE from supplied data. The message is not
	* signed.
	*
	* Args:
	*       iMessage             Pointer to the constructed message
	*       nextPayloadTypeIndex Will contain the index for the next payload type
	*                            upon exit.
	*       V                    Controls if the V bit is set in the HDR payload
	*       timestamp            Contains a byte string representation of a UTC-NTP
	*                            timestamp.
	*       randlen              Specifies the length of the random number in
	*                            bytes. Should be 16 or greater.
	*       senderUri            Contains the identity of the sender as a URI.
	*       recipientUri         Contains the identity of the receiver as a URI.
	*       sakkeData            Contains the SAKKE encrypted data.
	*
	* iMessage will contain the created I_MESSAGE on exit.
	*
	* Note! Following the lead of CreateIMessage there is no error/ failure return.
	*/
	void CreateGmkIMessage(std::vector<uint8_t> &iMessage,
		size_t             &nextPayloadTypeIndex,
		bool                  v,
		String               &timestamp,
		uint8_t               randlen,
		const std::string    &senderUri,
		const std::string    &recipientUri,
		const Buffer         &sakkeData);
	/**
	* Creates a GROUP GSK SAKKE I_MESSAGE from supplied data. The message is not signed.
	*
	* Args:
	*       iMessage             Pointer to the constructed message
	*       nextPayloadTypeIndex Will contain the index for the next payload type
	*                            upon exit.
	*       v                    Controls if the v bit is set in the HDR payload
	*       timestamp            Contains a byte string representation of a UTC-NTP
	*                            timestamp.
	*       randlen              Specifies the length of the random number in
	*                            bytes. Should be 16 or greater.
	*       senderUri            Contains the identity of the sender as a URI.
	*       recipientUri         Contains the identity of the receiver as a URI.
	*
	* KEMAC is not added at this stage the caller will add it.
	* iMessage will contain the created I_MESSAGE on exit.
	* Note! Following the lead of CreateIMessage there is no error/ failure return.
	*/
	void CreateGskIMessage(std::vector<uint8_t> &iMessage,
		size_t             &nextPayloadTypeIndex,
		bool                  v,
		String               &timestamp,
		uint8_t               randlen,
		const std::string    &senderUri,
		const std::string    &recipientUri);
	/**
	* Creates a GROUP MC PTT SAKKE I_MESSAGE from supplied data. The message
	* is not signed.
	*
	* Args:
	*       iMessage             Pointer to the constructed message
	*       nextPayloadTypeIndex Will contain the index for the next payload
	*                            type upon exit.
	*       v                    Controls if the v bit is set in the HDR payload
	*       timestamp            Contains a byte string representation of a
	*                            UTC-NTP timestamp.
	*       randlen              Specifies the length of the random number in
	*                            bytes. Should be 16 or greater.
	*       senderUri            Contains the identity of the sender as a URI.
	*       recipientUri         Contains the identity of the receiver as a URI.
	*       sakkeData            Contains the SAKKE encrypted data.
	*
	* iMessage will contain the created I_MESSAGE on exit.
	* Note! Following the lead of CreateIMessage there is no error/ failure
	* return.
	*/
	void CreateMcPttIMessage(std::vector<uint8_t> &iMessage,
		size_t             &nextPayloadTypeIndex,
		bool                  v,
		String               &timestamp,
		uint8_t               randlen,
		const std::string    &senderUri,
		const std::string    &recipientUri,
		const Buffer         &sakkeData);


	/**
	* Creates an I_MESSAGE with an HDR payload
	* V controls if the V bit is set in the HDR payload
	* iMessage will contain the HDR payload upon exit
	* nextPayloadTypeIndex will contain the index for the next payload type upon exit
	*/
	void CreateIMessageWithHdrPayload(std::vector<uint8_t> &iMessage,
		size_t &nextPayloadTypeIndex, bool V,
		const uint8_t &hdrMsgType = ScLibMs::defaultMsgType);

	/**
	* Creates and appends a MIKEY T payload to an I_MESSAGE
	* timestampString contains a hex string representation of a UTC-NTP timestamp
	* iMessage will be updated by appending a T payload
	* nextPayloadTypeIndex will contain the index for the next payload type upon exit
	*/
	void AppendTPayloadToIMessage(std::vector<uint8_t> &iMessage,
		size_t &nextPayloadTypeIndex, String &timestampString);

	/**
	* Creates and appends a MIKEY RAND payload to an I_MESSAGE
	* randlen specifies the length of the random number in bytes. Should be 16 or greater.
	* iMessage will be updated by appending a RAND payload
	* nextPayloadTypeIndex will contain the index for the next payload type upon exit
	*/
	void AppendRANDPayloadToIMessage(std::vector<uint8_t> &iMessage,
		size_t &nextPayloadTypeIndex, uint8_t randlen);

	/**
	* Creates and appends a MIKEY RAND payload to an I_MESSAGE
	* type specifies the ID type
	* role specifies the ID role
	* identity contains the identity
	* iMessage will be updated by appending a IDR payload
	* nextPayloadTypeIndex will contain the index for the next payload type upon exit
	*/
	void AppendIDRPayloadToIMessage(std::vector<uint8_t> &iMessage,
		size_t &nextPayloadTypeIndex, const uint8_t type,
		const uint8_t role, const std::string &identity);

	/**
	* Creates and appends a MIKEY SAKKE payload to an I_MESSAGE
	* sakke contains the SAKKE data
	* iMessage will be updated by appending a SAKKE payload
	* nextPayloadTypeIndex will contain the index for the next payload type upon exit
	*/
	void AppendSAKKEPayloadToIMessage(std::vector<uint8_t> &iMessage,
		size_t &nextPayloadTypeIndex, const Buffer &sakke);

	/**
	* Creates and appends a MIKEY SAKKE payload to an I_MESSAGE.
	* This payload must be the last in an I_MESSAGE.
	* type indicates the signature type. Only lower 4 bits are used as S type.
	* signature contains the I_MESSAGE signature
	* iMessage will be updated by appending a SIGN payload
	*/
	void AppendSIGNPayloadToIMessage(std::vector<uint8_t> &iMessage,
		size_t nextPayloadTypeIndex, uint8_t type, const Buffer &signature);

	/**
	* Creates and appends a KEMAC payload to an I_MESSAGE.
	* encryptionAlgorithm specifies the algrithmn used to encrypt the GSK
	* encryptedData The encrypted GSK
	* macAlgorithm specifies the MAC algorithm
	* macData The MAC itself
	* iMessage will be updated by appending a KEMAC payload
	* nextPayloadTypeIndex will contain the index for the next payload type upon exit
	*/
	void AppendKEMACPayloadToIMessage(std::vector<uint8_t> &iMessage,
		size_t &nextPayloadTypeIndex,
		uint8_t encryptionAlgorithm, const Buffer &encryptedData,
		const Buffer salt);

	void AppendMACPayloadToIMessage(
		std::vector<uint8_t> &iMessage,
		size_t             &nextPayloadTypeIndex,
		uint8_t               macAlgorithm,
		const Buffer         &mac);


	/**
	* Parses an I_MESSAGE HDR payload.
	* iMessage is the vector containing the payload to parse.
	* index is the index of the start of the payload to parse within the iMessage.
	* Returns true if parsing is successful, otherwise false.
	* Upon successful exit:
	*   index will be updated to point to the start of the following payload.
	*   nextPayloadTypeIndex will be updated to point to the next payload type.
	*   csbid will contain the CSBID extracted from the payload.
	*   v will be set to the state of the HDR payload V bit.
	*/
	bool ProcessHDRPayload(const std::vector<uint8_t> &iMessage,
						   std::vector<uint8_t>::const_iterator &index,
						   std::vector<uint8_t>::const_iterator &nextPayloadTypeIndex,
						   uint32_t &csbid,
						   bool &v,
						   uint8_t &csMapType = csMsgTypeDefault);

	/**
	* Parses an I_MESSAGE T payload.
	* iMessage is the vector containing the payload to parse.
	* index is the index of the start of the payload to parse within the iMessage.
	* Returns true if parsing is successful, otherwise false.
	* Upon successful exit:
	*   index will be updated to point to the start of the following payload.
	*   nextPayloadTypeIndex will be updated to point to the next payload type.
	*   t will contain the NTP UTC timestamp extracted from the payload.
	*/
	bool ProcessTPayload(const std::vector<uint8_t> &iMessage,
						 std::vector<uint8_t>::const_iterator &index,
						 std::vector<uint8_t>::const_iterator &nextPayloadTypeIndex,
						 uint64_t &t);

	/**
	* Parses an I_MESSAGE RAND payload.
	* iMessage is the vector containing the payload to parse.
	* index is the index of the start of the payload to parse within the iMessage.
	* Returns true if parsing is successful, otherwise false.
	* Upon successful exit:
	*   index will be updated to point to the start of the following payload.
	*   nextPayloadTypeIndex will be updated to point to the next payload type.
	*/
	bool ProcessRANDPayload(const std::vector<uint8_t> &iMessage,
							std::vector<uint8_t>::const_iterator &index,
							std::vector<uint8_t>::const_iterator &nextPayloadTypeIndex);

	bool ProcessRANDPayload(const std::vector<uint8_t> &iMessage,
		std::vector<uint8_t>::const_iterator &index,
		std::vector<uint8_t>::const_iterator &nextPayloadTypeIndex,
		std::vector<uint8_t> &rndNum);

	/**
	* Parses an I_MESSAGE IDR payload.
	* iMessage is the vector containing the payload to parse.
	* index is the index of the start of the payload to parse within the iMessage.
	* Returns true if parsing is successful, otherwise false.
	* Upon successful exit:
	*   index will be updated to point to the start of the following payload.
	*   nextPayloadTypeIndex will be updated to point to the next payload type.
	*   role will contain the IDR role extracted from the payload.
	*   identity will contain the identity string extracted from the payload.
	*/
	bool ProcessIDRPayload(const std::vector<uint8_t> &iMessage,
						   std::vector<uint8_t>::const_iterator &index,
						   std::vector<uint8_t>::const_iterator &nextPayloadTypeIndex,
						   uint8_t &role, std::string &identity);

	/**
	* Parses an I_MESSAGE KEMAC payload.
	* iMessage is the vector containing the payload to parse.
	* index is the index of the start of the payload to parse within the iMessage.
	*
	* Args:
	*       iMessage             The received message.
	*       index                Will be updated to point to the start of the
	*                            following payload.
	*       nextPayloadTypeIndex Will be updated to point to the next payload type.
	*       kemacEncType         KEMAC Encryption Type. Only one should be defined
	*                            according to 3GPP TS 33.303 AES-CM-128
	*       kemacEncData         KEMAC Encryption data (the encrypted GSK).
	*       kemacEncType         KEMAC MAC Type. Only one should be defined
	*                            according to 3GPP TS 33.303 HASH-sHA-256-256
	*       kemacEncType         KEMAC MAC.
	* Returns:
	*       bool                 true/ false for success/ failurei of parse.
	*/
	bool ProcessKEMACPayload(const std::vector<uint8_t> &iMessage,
		std::vector<uint8_t>::const_iterator &index,
		std::vector<uint8_t>::const_iterator &nextPayloadTypeIndex,
		uint8_t &kemacEncType,
		std::vector<uint8_t> &kemacEnc,
		std::vector<uint8_t> &kemacSalt,
		uint8_t &kemacMacType,
		std::vector<uint8_t> &kemacMac);

	/**
	* Parses an I_MESSAGE SAKKE payload.
	* iMessage is the vector containing the payload to parse.
	* index is the index of the start of the payload to parse within the iMessage.
	* Returns true if parsing is successful, otherwise false.
	* Upon successful exit:
	*   index will be updated to point to the start of the following payload.
	*   nextPayloadTypeIndex will be updated to point to the next payload type.
	*   sakke will contain the SAKKE encrypted data extracted from the payload.
	*/
	bool ProcessSAKKEPayload(const std::vector<uint8_t> &iMessage,
							 std::vector<uint8_t>::const_iterator &index,
							 std::vector<uint8_t>::const_iterator &nextPayloadTypeIndex,
							 std::vector<uint8_t> &sakke);

	/**
	* Parses an I_MESSAGE SIGN payload.
	* iMessage is the vector containing the payload to parse.
	* index is the index of the start of the payload to parse within the iMessage.
	* Returns true if parsing is successful, otherwise false.
	* Upon successful exit:
	*   index will be updated to point to the start of the following payload, which
	*   should normally be the end of the iMessage.
	*   signature will contain the SAKKE encrypted data extracted from the payload.
	*/
	bool ProcessSIGNPayload(const std::vector<uint8_t> &iMessage,
							std::vector<uint8_t>::const_iterator &index,
							std::vector<uint8_t>::const_iterator &nextPayloadTypeIndex,
							std::vector<uint8_t> &signature);
};

#endif SC_LIBMS_PAYLOAD_H
