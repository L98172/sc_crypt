/*
 *  Definition of Secure Chorus data store reference type.
 */

#ifndef SC_DATAREF_H
#define SC_DATAREF_H

#include <stddef.h>

#include "sc_types.h"


/*
 *  This type provides a "handle" to data stored in the data store.
 *  This should be treated as a copyable opaque type by code using
 *  the data store. Each implementation of the data store may
 *  redefine this type appropriately.
 */
typedef size_t DataRef;


#endif
