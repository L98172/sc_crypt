#ifndef SC_LIBMS_H
#define SC_LIBMS_H

// Includes
#include <string>
#include <vector>
#include <stdint.h>
#include "sc_errno.h"
#include "sc_context.h"

#define IMESSAGE_RAND_BYTES 16

// Forward declarations
struct Buffer;

/**
 * The Secure Chorus Lib MS namespace.
 */
namespace ScLibMs
{

	enum IMsgType { defaultMsgType = 0, GMKMsgType, GSKMsgType, MCPTTMsgType };
	static uint8_t csMsgTypeDefault = defaultMsgType;

	/**
	*  Creates a MIKEY SAKKE I_MESSAGE and a shared secret value.
	*  The I_MESSAGE will be signed using ECCSI if a non empty
	*  sender URI is provided.
	*  Args: iMessage       - vector that will contain the
	*                       - I_MESSAGE (output).
	*        ssv            - vector that will contain the shared
	*                         secret value (output).
	*        secCtx         - The secure context (input).
	*        senderUri      - The URI of the sender (input).
	*        recipientUi    - The URI of the recipient (input).
	*        v              - The required state of the HDR payload
	*                       - v bit (input).
	*  Rets: Standard error code.
	*/
	ScErrno CreateMikeySakkeIMessage(
		std::vector<uint8_t> &iMessage,
		std::vector<uint8_t> &ssv,
		SecCtx const &secCtx,
		std::string const &senderUri,
		std::string const &recipientUri,
	    bool const &v = false
		);

	/**
	*  Creates a MIKEY SAKKE I_MESSAGE.
	*  The I_MESSAGE will be signed using ECCSI if a non empty
	*  sender URI is provided.
	*  Args: iMessage       - vector that will contain the
	*                       - I_MESSAGE (output).
	*        secCtx         - The secure context (input).
	*        senderUri      - The URI of the sender (input).
	*        recipientUi    - The URI of the recipient (input).
	*        ssv            - vector containing the shared
	*                         secret value (input).
	*        v              - The required state of the HDR payload
	*                       - v bit (input).
	*  Rets: Standard error code.
	*/
	ScErrno CreateMikeySakkeIMessageWithSsv(
		std::vector<uint8_t> &iMessage,
		SecCtx const &secCtx,
		std::string const &senderUri,
		std::string const &recipientUri,
		std::vector<uint8_t> const &ssv,
		bool const &v = false
		);

	/**
	*  Processes a MIKEY SAKKE I_MESSAGE with optional signature verification
	*  and returns the extracted shared secret value.
	*  Args: recipientUi    - The URI of the recipient (output).
	*        senderUri      - The URI of the sender (output) , if present.
	*        ssv            - vector that will contain the extracted shared
	*                         secret value (output).
	*        v              - The state of the HDR payload
	*                       - v bit (output).
	*        secCtx         - The secure context (input).
	*        iMessage       - vector containing the I_MESSAGE (input).
	*        checkSignature - Controls if the signature is verified (input).
	*                         True to enable verisification.
	*  Rets: Standard error code.
	*/
	ScErrno ProcessMikeySakkeIMessage(
		std::string &recipientUri,
		std::string &senderUri,
		std::vector<uint8_t> &ssv,
		bool &v,
		SecCtx const &secCtx,
		std::vector<uint8_t> const &iMessage,
		bool checkSignature=true
		);


	/**
	*  Processes a MIKEY SAKKE I_MESSAGE with optional signature verification
	*  as if the validation was happening at some other time given as a
	*  parameter. Returns the extracted shared secret value.
	*  Args: recipientUi    - The URI of the recipient (output).
	*        senderUri      - The URI of the sender (output) , if present.
	*        ssv            - vector that will contain the extracted shared
	*                         secret value (output).
	*        v              - The state of the HDR payload
	*                       - v bit (output).
	*        secCtx         - The secure context (input).
	*        iMessage       - vector containing the I_MESSAGE (input).
	*        timestamp      - uint64_t timestamp of verification time
	*        checkSignature - Controls if the signature is verified (input).
	*                         True to enable verisification.
	*  Rets: Standard error code.
	*/
	ScErrno ProcessMikeySakkeIMessageForTime(
		std::string &recipientUri,
		std::string &senderUri,
		std::vector<uint8_t> &ssv,
		bool &v,
		SecCtx const &secCtx,
		std::vector<uint8_t> const &iMessage,
		uint64_t timestamp,
		bool checkSignature=true);

	/**
	*  Processes a MIKEY SAKKE I_MESSAGE with optional signature verification
	*  and returns the extracted shared secret or GMK or GSK value.
	*
	*  This implementation of the ProcessMikeySakkeIMessage method handles
	*  messages that would also be handled by returns the implementation
	*  above. However, this implementation also handles Group Messages and,
	*  as such, also returns an IDRiRole indication for the caller to know
	*  whether the IMessage was group related.
	*
	*  Of course, unlike when creating a GMK or GSK I-Message and the intent
	*  known, it is not possible to know 'a priori' whether a received
	*  I-Message is an original P2P or group (GMK/GSK) I-Message.
	*
	*  Args: recipientUi    - The URI of the recipient (output).
	*        senderUri      - The URI of the sender (output) , if present.
	*        ssv            - vector that will contain the extracted shared
	*                         secret value (output).
	*        v              - The state of the HDR payload
	*                       - v bit (output).
	*        secCtx         - The secure context (input).
	*        iMessage       - vector containing the I_MESSAGE (input).
	*        checkSignature - Controls if the signature is verified (input).
	*                         True to enable verisification.
	*  Rets: Standard error code.
	*/
	ScErrno ProcessMikeySakkeIMessage(
		std::string &recipientUri,
		std::string &senderUri,
		std::vector<uint8_t> gmk,
		std::vector<uint8_t> &ssv,
		bool &v,
		IMsgType &msgType,
		SecCtx const &secCtx,
		std::vector<uint8_t> const &iMessage,
		bool checkSignature = true
		);

	/**
	*  Parse a MIKEY SAKKE I_MESSAGE and extract the key fields
	*  No verification or decryption of the fields is performed.
	*  Args: iMessage       - The I_MESSAGE contents to be parsed
	*        v              - The state of the HDR payload
	*                         v bit (output).
	*        senderUri      - String that will contain the extracted
	*                         sender URI (output).
	*        recipientUri   - String that will contain the extracted
	*                         recipent URI (output).
	*        csbid          - Cryptosession Bundle ID (output).
	*        messageTime    - Timestamp of the message.
	*        messageLength  - Length of the message up to the
	*                         signature part (output).
	*        sakke          - vector that will contain the extracted
	*                         sakke encapsulated data (output).
	*        signature      - vector that will contain the extracted
	*                         signature data (output).
	*        payloadFlags   - bitfield of header fields present in
	*                         the message (output).
	*  Rets: Standard error code.
	*/
	ScErrno ParseMikeySakkeIMessage(std::vector<uint8_t> const &iMessage,
		bool &v,
		std::string &senderUri,
		std::string &recipientUri,
		uint32_t &csbid,
		uint64_t &messageTime,
		size_t &messageLength,
		std::vector<uint8_t> &sakke,
		std::vector<uint8_t> &signature,
		uint8_t &payload);
	
	
	/**
	*  Parse a MIKEY SAKKE I_MESSAGE and extract a subset of
	*  the key fields.
	*  No verification or decryption of the fields is performed.
	*  Args: iMessage       - The I_MESSAGE contents to be parsed
	*        senderUri      - String that will contain the extracted
	*                         sender URI (output).
	*        recipientUri   - String that will contain the extracted
	*                         recipent URI (output).
	*        messageTime    - Timestamp of the message.
	*  Rets: Standard error code.
	*/
	ScErrno ParseMikeySakkeIMessage(std::vector<uint8_t> const &iMessage,
		std::string &senderUri,
		std::string &recipientUri,
		uint64_t &messageTime);

	/**
	* Creates a MIKEY ECCSI signature for the supplied data
	* Args: mikeyEccsi	 - Following successful operation this vector
	*                      will contain the created MIKEY-SAKKE ECCSI
	*	                   signature, otherwise it will be empty (output).
    *       secCtx	     - The security context to use for cryptographic
	*                      operations (input).
    *       signingUri	 - A string containing the URI to use when
	*                      signing the message (input).
    *       msgTimestamp - A string containing the message timestamp to
	*                      use when signing the message (input).
    *       data         - The data to be signed (input).
    * Rets:	Standard error code.
	*/
	ScErrno CreateMikeyEccsiSig(
		std::vector<uint8_t> &mikeyEccsi,
		SecCtx const &secCtx,
		std::string const &signingUri,
		std::string const &msgTimestamp,
		std::vector<uint8_t> const &data
		);

	/**
	* Verifies a MIKEY ECCSI signature against supplied data
	* Args: mikeyEccsi   - the ECCSI signature (input)
	*		secCtx	     - The security context to use for cryptographic
	*                      operations (input).
    *       signingUri	 - A string containing the URI to use when
	*                      signing the message (input).
    *       msgTimestamp - A string containing the message timestamp to
	*                      use when signing the message (input).
    *       data         - The data that is signed (input).
    *       signature	 - the ECCSI signature (input).
	* Rets:	Standard error code. SUCCESS indicates successful verification.
	*/
	ScErrno VerifyMikeyEccsiSig(
		std::vector<uint8_t> &mikeyEccsi,
		SecCtx const &secCtx,
		std::string const &signingUri,
		std::string const &msgTimestamp,
		std::vector<uint8_t> const &data
		);
	
	/*
	 *  Creates a Group(GMK) MIKEY SAKKE I_MESSAGE and a GMK value.
	 *  The I_MESSAGE will be signed using ECCSI with sender URI.
	 *  Args:	iMessage	- vector that will contain the
	 *						- I_MESSAGE(output).
	 *			gmk			- vector that will contain the Group
	 *						- Master Key(output).
	 *			secCtx		- The secure context(input).
	 *			senderUri	- The URI of the sender(input).
	 *			recipientUi - The URI of the recipient(input).
	 *			v			- The required state of the HDR payload
	 *						- v bit(input).
	 *	Rets: Standard error code.
	 */
	ScErrno CreateMikeySakkeGMKIMessage(
		std::vector<uint8_t> &iMessage,
		std::vector<uint8_t> &gmk,
		SecCtx const &secCtx,
		std::string const &senderUri,
		std::string const &recipientUri,
		bool const &v = false
		);
	
	/**
		*  Creates a Group (GMK) MIKEY SAKKE I_MESSAGE with specified GMK.
		*  The I_MESSAGE will be signed using ECCSI with sender URI.
		*  Args: iMessage       - vector that will contain the
		*                       - I_MESSAGE (output).
		*        secCtx         - The secure context (input).
		*        senderUri      - The URI of the sender (input).
		*        recipientUi    - The URI of the recipient (input).
		*        gmk            - vector containing the Group
		*                         Master Key (input).
		*        v              - The required state of the HDR payload
		*                       - v bit (input).
		*  Rets: Standard error code.
		*/
	ScErrno CreateMikeySakkeGMKIMessageWithGmk(
		std::vector<uint8_t> &iMessage,
		SecCtx const &secCtx,
		std::string const &senderUri,
		std::string const &recipientUri,
		std::vector<uint8_t> const &gmk,
		bool const &v = false
		);

	/**
		*  Creates a Group (GSK) MIKEY SAKKE I_MESSAGE and a GSK value.
		*  The I_MESSAGE will be signed using ECCSI if a non empty
		*  sender URI is provided.
		*  Args: iMessage       - vector that will contain the
		*                       - I_MESSAGE (output).
		*        gmk            - vector that will contain the Group
		*                       - Master Key.
		*        gsk            - vector that will contain the Group
		*                         Session Key (output).
		*        secCtx         - The secure context (input).
		*        senderUri      - The URI of the sender (input).
		*        recipientUi    - The URI of the recipient (input).
		*        v              - The required state of the HDR payload
		*                       - v bit (input).
		*  Rets: Standard error code.
		*/
	ScErrno CreateMikeySakkeGSKIMessage(
		std::vector<uint8_t>       &iMessage,
		std::vector<uint8_t> const &gmk,
		std::vector<uint8_t> const &gsk,
		SecCtx const               &secCtx,
		std::string const       &senderUri,
		std::string const       &recipientUri,
		bool const              &v = false
	);
	
	/**
		*  Creates a MIKEY SAKKE MC-PTT I_MESSAGE and a gmk value.
		*  The I_MESSAGE will be signed using ECCSI if a non empty
		*  sender URI is provided.
		*  Args: iMessage       - vector that will contain the
		*                       - I_MESSAGE (output).
		*        gmk            - vector that will contain the GMK.
		*        secCtx         - The secure context (input).
		*        senderUri      - The URI of the sender (input).
		*        recipientUi    - The URI of the recipient (input).
		*        v              - The required state of the HDR payload
		*                       - v bit (input).
		*  Rets: Standard error code.
		*/
	ScErrno CreateMikeySakkeMCPTTIMessage(
		std::vector<uint8_t> &iMessage,
		std::vector<uint8_t> &gmk,
		SecCtx const &secCtx,
		std::string const &senderUri,
		std::string const &recipientUri,
		bool const &v = false
		);
	
	/**
		*  Creates a MIKEY SAKKE MC-PTT I_MESSAGE.
		*  The I_MESSAGE will be signed using ECCSI if a non empty
		*  sender URI is provided.
		*  Args: iMessage       - vector that will contain the
		*                       - I_MESSAGE (output).
		*        secCtx         - The secure context (input).
		*        senderUri      - The URI of the sender (input).
		*        recipientUi    - The URI of the recipient (input).
		*        gmk            - vector containing the GMK.
		*        v              - The required state of the HDR payload
		*                       - v bit (input).
		*  Rets: Standard error code.
		*/
	ScErrno CreateMikeySakkeMCPTTIMessageWithGmk(
		std::vector<uint8_t> &iMessage,
		SecCtx const &secCtx,
		std::string const &senderUri,
		std::string const &recipientUri,
		std::vector<uint8_t> const &gmk,
		bool const &v = false
		);

}

#endif // SC_LIBMS_H

