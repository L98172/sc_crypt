/*
 *  Header file for Secure Chorus miscellaneous functions.
 */

#ifndef SC_MISC_H
#define SC_MISC_H

#include "sc_errno.h"
#include "sc_types.h"


/*
 *  To allow use by both C and C++ code (start).
 */

#ifdef __cplusplus
extern "C"
{
#endif


/*
 *  Returns string random bytes.
 *  Args: rand - Used to indicate the random bytes.
 *               On entry must have rand.length set to
 *               indicate required number of bytes, and
 *               rand.pointer set to be able to store
 *               random bytes.
 *  Rets: Standard error code.
 */
ScErrno GetSecureRandom(Buffer rand);


/*
 *  Returns current time UTC timestamp, using numbering as defined by
 *  RFC 3339.
 *  Args: utcTimestamp_p - *utcTimestamp_p is set to UTC timestamp.
 *                         Ignored if null.
 *        year_p         - *year_p is set to year. Ignored if null.
 *        month_p        - *month_p is set to month. Ignored if null.
 *        dayYear_p      - *dayYear_p is set to day of year.
 *                         Ignored if null.
 *  Rets: Standard error code.
 */
ScErrno GetDateTime(String *utcTimestamp_p, int *year_p, int *month_p,
                    int *dayYear_p);


/*
 *  Converts UTC timestamp to date, using numbering as defined by RFC 3339.
 *  Args: utcTimestamp - UTC timestamp (input).
 *        year_p       - *year_p is set to year. Ignored if null.
 *        month_p      - *month_p is set to month. Ignored if null.
 *        dayYear_p    - *dayYear_p is set to day of year.
 *                       Ignored if null.
 *  Rets: Standard error code.
 */
ScErrno GetDateFromTimestamp(String utcTimestamp, int *year_p, int *month_p,
	                         int *dayYear_p);


/*
 *  To allow use by both C and C++ code (finish).
 */

#ifdef __cplusplus
}
#endif

#endif
