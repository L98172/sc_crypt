#include <cctype>
#include <string.h>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>

#include <xercesc/dom/DOM.hpp> 

#include <xercesc/framework/XMLValidator.hpp>
#include <xercesc/framework/MemoryManager.hpp>
#include <xercesc/framework/XMLGrammarPool.hpp>
#include <xercesc/framework/MemBufInputSource.hpp>
#include <xercesc/framework/MemBufFormatTarget.hpp>

#include <xercesc/parsers/XercesDOMParser.hpp>

#include <xercesc/sax/HandlerBase.hpp>

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/TransService.hpp>
#include <xercesc/util/XercesDefs.hpp>
#include <xercesc/util/XMLUni.hpp> 

#include <xsec/canon/XSECC14n20010315.hpp>
#include <xsec/utils/XSECNameSpaceExpander.hpp>
#include <xsec/utils/XSECPlatformUtils.hpp>

#include "sc_errno.h"
#include "sc_libmskms.h"
#include "sc_libmscrypto.h"
#include "sc_misc.h"
#include "sc_types.h"

namespace
{
	/* ---- XML Namespace Prefixes --------------------------------------------- */

	const std::string DIGITAL_SIG_PREFIX     = "DS";
	const std::string SCHEMA_INSTANCE_PREFIX = "XSI";
	const std::string SE_PREFIX              = "SE";

	/* ---- XML Namespace Enum ------------------------------------------------- */

	enum XMLNAMESPACE
	{
		MAIN = 0,
		DS = 1,
		XSI = 2,
		SE = 3
	};

	/* ---- XML Standard URIs DO NOT MODIFY ------------------------------------ */

	const std::string SCHEMA_INSTANCE = 
	  "http://www.w3.org/2001/XMLSchema-instance";
	const std::string NAMESPACE_URI = "http://www.w3.org/2000/xmlns/";

	/* ---- XML Namespace Declarations ----------------------------------------- */

	const std::string MAIN_NAMESPACE = "http://www.cesg.gov.uk/SecureChorus";

	const std::string XSI_NAMESPACE  = 
	  "http://www.w3.org/2001/XMLSchema-instance";
	const std::string DS_NAMESPACE	 = "http://www.w3.org/2000/09/xmldsig#";
	const std::string SE_NAMESPACE	 = "http://www.cesg.gov.uk/SecureChorus";
	const std::string XSI_LOCATION	 = 
	  "http://www.cesg.gov.uk/SecureChorus SE_KmsInterface_XMLSchema.xsd";

	/* ---- XML Algorithm Specifications --------------------------------------- */

	const std::string HMAC_ALGORITHM   = 
	  "http://www.w3.org/2001/04/xmldsig-core#hmac-sha256";
	const std::string C14N_ALGORITHM   = 
	  "http://www.w3.org/TR/2001/REC-xml-c14n-20010315";
	const std::string DIGEST_ALGORITHM = 
	  "http://www.w3.org/2001/04/xmlenc#sha256"; 

	/* ---- Assorted Data ------------------------------------------------------ */

	// Maximum Length of HMAC.
	const size_t HMAC_OUT_LEN = 128;

	// Reference tag for XML signatures.
	const std::string REFERENCE_URI = "#xmldoc";

	// KMS Response XML Schema name for xerces grammar parsing.
	const std::string KMS_RESPONSE_SCHEMA = "SE_KmsInterface_XMLSchema.xsd";

	// Version number for formulating an XML Request .
	const std::string VERSION_VALUE = "1.0.0";

	// Hash truncation length.
	const int TRUNC_LEN = 0;

	/* ---- Temporary Declaration ---------------------------------------------- */

	//  Base-64 decoding.
	//  Args: input  - Data to be base-64 decoded.
	//        output - Set to result of base-64 decoding.
	//  Rets: True if successful, false if not.
	bool base64Decode(const std::vector<unsigned char> & input,
		              std::vector<unsigned char> & output)
	{
		//  Base64 encoded characters.
		const char * const encoded
			= "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

		//  Verify input is multiple of four characters.
		const size_t length = input.size();

		if (length % 4 != 0)
		{
			return false;
		}

		//  Loop through input four characters at a time (incremented in loop).
		for (size_t i = 0; i != length; )
		{
			const char * const p0 = std::strchr(encoded, input[i++]);
			const char * const p1 = std::strchr(encoded, input[i++]);
			const char * const p2 = std::strchr(encoded, input[i++]);
			const char * const p3 = std::strchr(encoded, input[i++]);

			if (p0 == 0 || p1 == 0)
			{
				return false;
			}

			const unsigned char bits0 = (const unsigned char)(p0 - encoded);
			const unsigned char bits1 = (const unsigned char)(p1 - encoded);

			if (p2 == 0)
			{
				if (i != length || input[i - 2] != '=' || input[i - 1] != '=')
				{
					return false;
				}
				else
				{
					output.push_back((bits0 << 2) | (bits1 >> 4));
				}
			}
			else if (p3 == 0)
			{
				if (i != length || input[i - 1] != '=')
				{
					return false;
				}
				else
				{
					const unsigned char bits2 = (const unsigned char)(p2 - encoded);
					output.push_back((bits0 << 2) | (bits1 >> 4));
					output.push_back((bits1 << 4) | (bits2 >> 2));
				}
			}
			else
			{
				const unsigned char bits2 = (const unsigned char)(p2 - encoded);
				const unsigned char bits3 = (const unsigned char)(p3 - encoded);
				output.push_back((bits0 << 2) | (bits1 >> 4));
				output.push_back((bits1 << 4) | (bits2 >> 2));
				output.push_back((bits2 << 6) | bits3);
			}
		}

		return true;
	}

	/* ---- Timestamp management functions ------------------------------------- */	

	/*
	*  Convert timestamp from certificate format to NTP format.
	*  Args: timestamp - Timestamp as string, replaced by this function.
	*  Rets: True if timestamp is valid, false otherwise.
	*/  
	bool convert_timestamp(std::string & timestamp)
	{
		//  Verify format yyyy-mm-ddThh:mm:ssZ.
		if (timestamp.size() != 20 || timestamp[4] != '-'
			|| timestamp [7] != '-' || timestamp[10] != 'T'
			|| timestamp[13] != ':' || timestamp[16] != ':'
			|| timestamp[19] != 'Z')
		{
			return false;
		}

		//  Verify year.
		if (!std::isdigit(timestamp[0]) || !std::isdigit(timestamp[1])
			|| !std::isdigit(timestamp[2]) || !std::isdigit(timestamp[3]))
		{
			return false;
		}

		const unsigned int year = 1000 * (timestamp[0] - '0')
			                      + 100 * (timestamp[1] - '0')
			                      + 10 * (timestamp[2] - '0')
			                      + (timestamp[3] - '0');

		//  Convert year to POSIX, failing if before epoch.
		if (year < 1970)
		{
			return false;
		}

		const unsigned posix_year = year - 1970;

		//  Verify month. Offset to range 0 to 11 if used.
		if (!std::isdigit(timestamp[5]) || !std::isdigit(timestamp[6]))
		{
			return false;
		}

		unsigned int month = 10 * (timestamp[5] - '0')  + (timestamp[6] - '0');

		if (month > 12 || month-- == 0)
		{
			return false;
		}

		//  Number of days in each months, including leap year calculation.
		const bool leap_year
		  = year % 4 == 0 && (year % 100 != 0 || year % 400 == 0);
		const unsigned int month_days[]
		  = {31, (leap_year ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

		//  Verify day of month, offset range from 0 if used.
		unsigned int days = 10 * (timestamp[8] - '0') + (timestamp[9] - '0');

		if (days > month_days[month] || days-- == 0)
		{
			return false;
		}

		//  Count number of leap days since epoch, excluding this year.
		//  First assume multiple of four rule, then do correction for
		//  future cases. Note exclude current year in all cases, and
		//  thus first year with leap day is Posix year 3 (1973) and
		//  first year with century correction is 2101.
		unsigned int leap_days = (posix_year + 1) / 4;

		if (year > 2100)
		{
			const unsigned int cyear = year - 2001;
			leap_days -= cyear / 100 - cyear / 400;
		}

		//  Convert to day of year. Includes leap year offset for this year.
		for (unsigned int i = 0; i != month; ++i)
		{
			days += month_days[i];
		}

		//  Total days since epoch is made up from three components as shown.
		//  Note first day of epoch is day zero.
		const unsigned long total_days = 365 * posix_year + leap_days + days;

		//  Verify housr minutes and seconds (does not include leap seconds).
		const unsigned int hours   = 10 * (timestamp[11] - '0') + (timestamp[12] - '0');
		const unsigned int minutes = 10 * (timestamp[14] - '0') + (timestamp[15] - '0');
		const unsigned int seconds = 10 * (timestamp[17] - '0') + (timestamp[18] - '0');

		if (hours >= 24 || minutes >= 60 || seconds >= 60)
		{
			return false;
		}

		//  Compute total seconds since start of epoch.
		const unsigned long total_seconds = 86400 * total_days + 3600 * hours
			                                + 60 * minutes + seconds;

		//  NTP offset known for start of POSIX epoch.
		unsigned long ntp = 2208988800ul + total_seconds;

		//  Convert NTP to store as return string (destroys value of NTP).
		//  Return success.
		timestamp.erase();
		timestamp.resize(16, '0');

		for (int i = 7; i >= 0; --i)
		{
			timestamp[i] = "0123456789ABCDEF"[ntp & 0x0F];
			ntp >>= 4;
		}

		return true;
	}

	/*
	*  Converts a hexadecimal String into an unsigned long value, for use in 
	*  timstamp manipulation.  
	*  Args: hexString - The hexadecimal timestamp as a String.
	*  Rets: Unsigned long variant of timestamp.
	*/	
	unsigned long hexToUL(String * hexString) 
	{
		unsigned long value = 0ul;	
		std::string
		  trimmedString(const_cast<const char *>(hexString->pointer),
			            hexString->length);
		trimmedString = trimmedString.substr(0, 8);
		std::istringstream iss(trimmedString);
		iss >> std::hex >> value;
		value = value - 2208988800ul;
		return value;
	}

	/*
	*  Calculates day of the month from the data provided by the ScCrypto 
	*  GetDateTime function.
	*  Args: dayYear - Value representing the day of the year (input).
	*        month   - Value representing month (input).
	*        year    - Value representing year (input).
	*        day     - Value representing day in specific month (input).
	*/
	void getDateFromDayOfYear(const int * const dayYear, const int * const month, 
		                      const int * const year, int * day)
	{
		int monthsArray[13] = {0,31,28,31,30,31,30,31,31,30,31,30,31};

		if((*year % 4 == 0 && *year % 100 != 0) || *year % 400 == 0) // Is a leap year
		{
			monthsArray[2] = 29;

			if(*dayYear > 366)
			{
				*day = 0;
				return;
			}
		}
		else if(*dayYear > 365)
		{
			*day = 0;
			return;
		}

		*day = *dayYear;

		for(int i = 1; i < *month; ++i)
		{
			*day -= monthsArray[i];
		}
	}

	/*
	*  Function to obtain timestamp, from ScCrypto, and convert it to 
	*  a timestamp conforming to ISO 8601 standard.
	*  Args: toBeReturned - std::string holding the ISO 8601 conforment
	* 						timestamp (output).
	*  Rets: Standard error code return. 
	*/
	ScErrno getTimeStamp(std::string * toBeReturned)
	{
		String _timeStamp;
		int _year = 0;
		int _month = 0;
		int _dayYear = 0;
		int _day = 0;
		int _hour = 0;
		int _second = 0;
		int _minute = 0;
		unsigned long _value;
		std::string monthStr, dayStr,hourStr,minuteStr, secStr;
		ScErrno ret_val = ScErrno::SUCCESS;

		// Call to ScCrypto function
		ret_val = GetDateTime(&_timeStamp, &_year, &_month, &_dayYear);

		if(ret_val != ScErrno::SUCCESS)
		{
			*toBeReturned = "";
			return ret_val;
		}

		// Call to convert hexadecimal timestamp to integer (unsigned long)
		_value = hexToUL(&_timeStamp);

		if(_value == 0)
		{
			return ScErrno::GENERAL_FAILURE;
		}

		// Gets day of the month from timestamp data.
		getDateFromDayOfYear(&_dayYear, &_month, &_year, &_day);
		if(_day <= 0)
		{
			return ScErrno::GENERAL_FAILURE;
		}

		/*
		* Calculates the hour from the timestamp by dividing by number of 
		* seconds in a day and then applying a modulo of the number of 
		* hours in a day.
		*/
		_hour   = (_value / 3600) % 24;
		_minute = (_value / 60) % 60;
		_second =  _value % 60;

		if(_month < 10)
		{
			monthStr = "0" + std::to_string(_month);
		}
		else
		{
			monthStr = std::to_string(_month);
		}

		if(_day < 10)
		{
			dayStr = "0" + std::to_string(_day);
		}
		else
		{
			dayStr = std::to_string(_day);
		}

		if(_hour < 10)
		{
			hourStr = "0" + std::to_string(_hour);
		}
		else
		{
			hourStr = std::to_string(_hour);
		}

		if(_minute < 10)
		{
			minuteStr = "0" + std::to_string(_minute);
		}
		else
		{
			minuteStr = std::to_string(_minute);
		}

		if(_second < 10)
		{
			secStr = "0" + std::to_string(_second);
		}
		else
		{
			secStr = std::to_string(_second);
		}

		/*
		*  Populates a string with the ISO 8601 compliant timestamp.
		*/
		*toBeReturned = std::to_string(_year) + "-" + monthStr + "-"
			            + dayStr + "T" + hourStr + ":" + minuteStr + ":"
						+ secStr + "Z" ;
		return ScErrno::SUCCESS;
	}

	/* ---- XML Tag Finding Functions ------------------------------------------ */

	/*
	*  Converts an inputted std::strinh into an std::vector.
	*  Args: vec - The reference to the vector which is to be populated (output).
	*        str - The reference to the string which is to populate the 
	*              vector (input).
	*  Template used to allow function to be used for unsigned and signed
	*  chars.
	*/
	template<class T>	
	  void stringToVec(std::vector<T> & vec, const std::string & str)
	{
		/*
		*  Assigns the values held in Str to vec.
		*/
		vec.assign(str.begin(), str.end());
	}

	/*
	*  Takes a text node and pulls out the text. 
	*  This is then extracted and packaged into a std::vector of unsigned char. 
	*  Args: textNode        - The node containg the Base64 endoed data (input).
	*        extractedVector - Vector containg the extracted values
	*                          (input/output).
	*/
	ScErrno textToVector(const xercesc::DOMNode * const textNode,
		std::vector<unsigned char> & extractedVector)
	{
		if(textNode->getNodeType() != xercesc::DOMNode::NodeType::TEXT_NODE)
		{
			return ScErrno::RESOURCE_NOT_ALLOCATED;
		}
		std::string dataFromChild;

		/*
		*  Pull out text from child text node.
		*/
		dataFromChild = xercesc::XMLString::transcode(textNode->getNodeValue());

		if(dataFromChild == "")
		{
			return ScErrno::RESOURCE_NOT_EXIST;
		}

		/* 
		*  Package data into a vector to be passed to decode.
		*/
		stringToVec(extractedVector, dataFromChild);

		if(extractedVector.size() != dataFromChild.length())
		{
			return ScErrno::GENERAL_FAILURE;
		}

		return ScErrno::SUCCESS;
	}

	/*
	*  Takes an XML node, and pulls out the Base64 encoded data from it. 
	*  This is then decoded and packaged into a std::vector of unsigned char. 
	*  Args: base64Node    - The node containg the Base64 endoed data (input).
	*        decodedVector - Vector containg the decoded values (input/output).
	*/
	ScErrno base64XMLToVector(const xercesc::DOMNode * const base64Node,
		std::vector<unsigned char> & decodedVector)
	{
		if(base64Node->getNodeType() != xercesc::DOMNode::NodeType::TEXT_NODE)
		{
			return ScErrno::RESOURCE_NOT_ALLOCATED;
		}
		std::string dataFromChild;
		std::vector<unsigned char> encodedVector;

		/*
		*  Pull out text from child text node.
		*/
		dataFromChild = xercesc::XMLString::transcode(base64Node->getNodeValue());

		if(dataFromChild == "")
		{
			return ScErrno::RESOURCE_NOT_EXIST;
		}

		/* 
		*  Package data into a vector to be passed to decode.
		*/
		stringToVec(encodedVector,dataFromChild);

		if(encodedVector.size() != dataFromChild.length())
		{
			return ScErrno::GENERAL_FAILURE;
		}

		/*
		*  Decode data and package into vector.
		*/
		base64Decode(encodedVector,decodedVector);
		return ScErrno::SUCCESS;
	}

	/*
	*  Trims a URI based string to only contain the data within the fragment,
	*  data after the hash (#) symbol.
	*  Args: URI - The URI which is to be trimmed and returned (input/output).
	*  Rets: Standard error code.
	*/
	ScErrno getFragment(std::string * URI)
	{
		for(size_t i3 = 0; i3 < (*URI).length(); ++i3)
		{
			if((*URI)[i3] == '#')
			{
				(*URI).erase(0, i3 + 1); //+1 because it needs to erase the # as well
				return ScErrno::SUCCESS;
			}
		}
		return ScErrno::RESOURCE_NOT_EXIST;
	}

	/*
	*  Adds an attribute with a given name and value to the DOMElement passed in. 
	*  Must be a DOMElement, as only they have a setAttribute function, and as 
	*  they are a subset of DOMNode they can be easily cast into DOMElements.
	*  Args: element     - DOMElement to which the attribute is to be added,
	*                      passed as a pointer to allow it to be returned
	*                      (input/output).
	*        nameOfAttr  - UTF-16 representation of the name of the attribute
	*                      to be added to the DOMElement (input).
	*        valueOfAttr -	UTF-16 representation of the value of the attribute
	*                      to be added to the DOMElement (input).
	*  Rets: Standard error code.
	*/ 
	ScErrno addAttribute(xercesc::DOMElement * const element, 
		                 const XMLCh * const nameOfAttr,
		                 const XMLCh * const valueOfAttr)
	{
		/*
		*  Ensures all pointers passed have valid memory adresses.
		*/
		if(element == NULL || nameOfAttr == NULL || valueOfAttr == NULL)
		{
			return ScErrno::RESOURCE_NOT_EXIST;
		}

		/*
		*  Attempts to add the attribute to the DOMElement.
		*/
		try
		{
			element->setAttribute(nameOfAttr,valueOfAttr);
		}
		catch(...)
		{
			return ScErrno::GENERAL_FAILURE;
		}
		return ScErrno::SUCCESS;
	}

	/*
	*  Adds a namespace 'attribute' to the specific DOMElement.
	*  Args: toBeAddedTo  - The DOMElement to which the namespace will be
	*                       added to, usually root (input).
	*        qualName     - The fully qualified name of the namespace.
	*                       Usually along the lines of "xmlns:xyz" (input).
	*        value        - The URI wherein the namespace being created 
	*                       is stored (input).
	*        namespaceUri - URI representing the standard used to create this
	*                       variant of namespace. Ignored if empty (input).
	*  Rets: Standard error code.
	*/  
	ScErrno addNamespaceToElement(xercesc::DOMElement * toBeAddedTo,
		                          const std::string & qualName,
		                          const std::string & value,
		                          const std::string & namespaceUri)
	{
		ScErrno retVal = ScErrno::SUCCESS;
		XMLCh * nsUri = NULL;

		if(qualName == "" || value == "" || toBeAddedTo == NULL)
		{
			retVal = ScErrno::RESOURCE_NOT_ALLOCATED;
		}
		else
		{
			XMLCh * namespaceLocation = xercesc::XMLString::transcode(value.c_str());
			XMLCh * qualifiedName = xercesc::XMLString::transcode(qualName.c_str());

			nsUri = xercesc::XMLString::transcode(namespaceUri.c_str());

			try
			{
				toBeAddedTo->setAttributeNS(nsUri, qualifiedName,
					                        namespaceLocation);
			}
			catch(xercesc::DOMException e)
			{
				retVal = ScErrno::GENERAL_FAILURE;
			}

			xercesc::XMLString::release(&namespaceLocation);
			xercesc::XMLString::release(&qualifiedName);
			xercesc::XMLString::release(&nsUri);
		}
		return retVal;
	}
	/*
	*  Converts a xerces node into a Buffer type, by serializing the data and
	*  then packaging that data into a vector which is then allocated to a 
	*  Buffer. 
	*  Args: node - The node to be packaged into the Buffer (input).
	*        buff - The Buffer that is to hold the pointer to the data (output).
	*        vec  - The vector that is used to store the serialized node,
	*               passed as a reference to prevent data loss from going 
	*               out of scope (output).
	*  Rets: Standard error code
	*/	
	ScErrno nodeToBuffer(xercesc::DOMNode * node, Buffer * buff,
		                 std::vector<unsigned char> & vec) 
	{
		xercesc::DOMDocument * tempDoc = NULL;		
		ScErrno ret_val = ScErrno::SUCCESS;
		XMLCh * LSTag = NULL;
		xercesc::DOMImplementation * impl = NULL;
		xercesc::DOMLSSerializer * writer = NULL;
		xercesc::DOMLSOutput * out = NULL;
		xercesc::MemBufFormatTarget * target = NULL;
		xercesc::DOMConfiguration * domConfig = NULL;

		/*
		*  Specify a DOMLSSerializer.
		*/
		LSTag = xercesc::XMLString::transcode("LS");
		impl = xercesc::DOMImplementationRegistry::getDOMImplementation(LSTag);
		xercesc::XMLString::release(&LSTag);
		writer = ((xercesc::DOMImplementationLS*)impl)->createLSSerializer();

		/* 
		*  Stops the serializer from adding <xml> tags. 
		*  Unnecessary here as not full XML document.
		*/
		domConfig = writer->getDomConfig(); 
		if(domConfig->canSetParameter(xercesc::XMLUni::fgDOMXMLDeclaration,false)) 
		{
			domConfig->setParameter(xercesc::XMLUni::fgDOMXMLDeclaration,false);
		}

		try
		{	
			/*
			*  Serializes the document to the specified output (and its target).
			*/
			XMLCh * w = writer->writeToString(node);

			/*
			*  Converts the outputted data to an array of XMLBytes for traversal. 
			*/
			std::string a = xercesc::XMLString::transcode(w);
			vec.assign(a.begin(), a.end());
			writer->release();
		}
		catch(...)
		{
			/*
			*  Releases the serializer memory and returns GENERAL_FAILURE if any 
			*  exceptions occur.
			*/
			writer->release();
			return ScErrno::GENERAL_FAILURE;
		}

		/*
		*  Assigns the necessary data to Buffer members and then returns SUCCESS.
		*/ 
		buff->length = vec.size();
		buff->pointer = &vec[0];
		return ScErrno::SUCCESS; 
	}

	/*
	*  Iterates over the data in a Buffer type and populates a std::string
	*  with the data held.
	*  Args: inBuff  - Buffer containing data (input).
	*	      outStr - String to put Buffer data into (input).
	*  Rets: Standard error code.
	*/
	ScErrno bufferToString(Buffer * inBuff, std::string * outStr)
	{
		for(size_t i = 0; i < inBuff->length; ++i)
		{
			(*outStr) += inBuff->pointer[i];
		}
		return ScErrno::SUCCESS;
	}

	/*
	*  Populates a Buffer type using a Vector input.
	*  Args: toBuff - Buffer type to add data to (output).
	*        vec    - Vector containing data to be added to String type (input).
	*  Rets: Standard error code.
	*/
	ScErrno vecToBuff(Buffer * toBuff, const std::vector<unsigned char> & vec)
	{
		toBuff->length = vec.size();
		toBuff->pointer = &vec[0];
		return ScErrno::SUCCESS;
	} 

	/*
	*  Function to canonicalize and package a node into a Buffer type. However,
	*  the ability to canonicalize the node is not currently present as it was
	*  not available with the xerces package.
	*  Args: nodeForCanon - Node to be canonicalized and packaged (input).
	*        outBuff      - Buffer to hold the formatted data (input/output).
	*        vec          - Vector to hold the formatted data (input/output).
	*  Rets: Standard error code.
	*/
	ScErrno canonicalizeData(xercesc::DOMDocument * doc, xercesc::DOMNode * nodeForCanon,
		Buffer * outBuff, std::vector<unsigned char> & vec)
	{
		ScErrno ret_val = ScErrno::SUCCESS;
		XSECC14n20010315 canon(doc, nodeForCanon);
		canon.setCommentsProcessing(false);
		canon.setUseNamespaceStack(true);
		canon.setInclusive11();
		unsigned char buffer[128];

		while(xsecsize_t length = canon.outputBuffer(buffer, 128))
		{
			vec.insert(vec.end(), buffer, buffer + length);
		}

		vecToBuff(outBuff, vec);
		return ret_val;
	}

	/*
	*  Populates a String type using a Vector input.
	*  Args: toStr - String type to add data to (output).
	*        vec   - Vector containing data to be added to String type (input).
	*  Rets:	Standard error code.
	*/
	ScErrno vecToString(String * toStr, std::vector<char> & vec)
	{
		toStr->length = vec.size();
		toStr->pointer = &vec[0];
		return ScErrno::SUCCESS;
	} 

	/*
	*  Returns a pointer to an XML node within the parent node, with name
	*  nameOfTag.
	*  Args: parent    - The parent DOMNode of the proposed XML tag (input).
	*        nameOfTag - The name of the tag in question in UTF-16 format (input).
	*  Rets: Pointer to a DOMNode memory address within parent DOMNode,
	*    		or NULL if data is invalid.
	*/
	xercesc::DOMNode * getXMLTag(const xercesc::DOMNode * const parent,
		const XMLCh * const nameOfTag)
	{
		xercesc::DOMNodeList * childNodeList = NULL;
		XMLSize_t count, loop;

		/*
		*  Returns a NULL pointer if the parent node does not have any child 
		*  nodes, or if no name is provided for the tag.
		*/
		if(!parent->hasChildNodes() || nameOfTag == NULL)
		{
			return NULL;
		}

		/*
		*  Sets the children nodes into an array of pointers for iteration.
		*/
		childNodeList = parent->getChildNodes();

		/*
		*  Ensures that the array pointer returned exists.
		*/
		if(childNodeList == NULL)
		{
			return NULL;
		}

		count = childNodeList->getLength();

		/*
		*  Loops through the array of pointers and determines if the name of
		*  the current node is equivalent to nameOfTag. If true, then returns
		*  the node. Elsewise, NULL is returned.
		*  n.b. getLocalName is used in place of getNodeName as the latter
		*  returns the XML fully qualified name which includes namespace
		*  prefixes which could be variable.
		*/
		for(loop = 0; loop < count; ++loop)
		{
			if(xercesc::XMLString::equals(childNodeList->item(loop)->getLocalName(),
				nameOfTag))
			{
				return childNodeList->item(loop);
			}
		}

		return NULL;
	} 

	/*
	*  Obtains a node which has an attribute with name "Id" and a specific
	*  value. This is used for verification of signatures, to find the XML
	*  tag which has been canonicalized and hashed.
	*  Args: parent    - A pointer to the XML tag which contains the XML 
	*                    tag with the "Id" attribute (input).
	*        attrValue - Expected value of the "Id" attribute (input).
	*  Rets: Pointer to the DOMNode within parent which has the "Id" 
	*        attribute required.
	*/
	xercesc::DOMNode *
	  getNodeWithAttributeId(const xercesc::DOMNode * const parent,
		                     const XMLCh * const attrValue)
	{
		xercesc::DOMNodeList * childNodes = NULL;
		xercesc::DOMNode * attribute = NULL;
		xercesc::DOMNamedNodeMap * attributesOfNode = NULL;
		xercesc::DOMNode * currentNode = NULL;
		XMLSize_t count = NULL;
		XMLCh * attributeName = NULL;

		/*
		*  Returns a NULL pointer if the parent node has no child nodes
		*  or no value is passed representing the expected "Id" value. 
		*/
		if(!parent->hasChildNodes() || attrValue == NULL)
		{
			return NULL;
		}

		/*
		*  Obtains the children nodes from the parent XML node.
		*/
		childNodes = parent->getChildNodes();
		count = childNodes->getLength();

		/*
		*  Iterates over the child nodes determining if they have any 
		*  attributes associated with them.
		*/
		for(XMLSize_t i = 0; i < count; ++i)
		{
			if(childNodes->item(i)->hasAttributes())
			{
				currentNode = childNodes->item(i);
				/*
				*  Assigns the attributes of the curent node that is being
				*  iterated over to a DOMNamedNodeMap.
				*/
				attributesOfNode = currentNode->getAttributes();

				/*
				*  The DOMNamedNodeMap is traversed to find any node with the 
				*  attribute with name "Id", which is represented in UTF-16
				*  form by the XMLCh value attributeName.
				*/
				attributeName = xercesc::XMLString::transcode("Id");
				attribute = attributesOfNode->getNamedItem(attributeName);
				xercesc::XMLString::release(&attributeName);

				if(attribute != NULL)
				{
					/*
					*  Checks to see if the value of the attribute is equivalent 
					*	to the value provided by attrValue, and returns the current
					*  node if true.
					*/	
					if(xercesc::XMLString::equals(attribute->getNodeValue(), attrValue))
					{
						attribute = NULL;
						attributesOfNode = NULL;
						childNodes = NULL;
						return currentNode;
					}
				}
			}
		}

		return NULL;
	}

	/*
	*  Returns the text from an XML tag with name nameOfTag
	*  as a DOMNode, as it is held in a TextNode under xerces 
	*  which is a subset of DOMNode.
	*  Args: parent    - Parent XML tag which should contain DOMNode with
	*                    name nameOfTag (input).
	*        nameOfTag - Contains the UTF-16 value of the XML tag whose 
	*                    TextNode is being searched for (input).
	*  Rets: Pointer to a DOMNode containg the TextNode that was searched for.
	*/
	xercesc::DOMNode * getTextTag(const xercesc::DOMNode * const parent,
		                          const XMLCh * const nameOfTag)
	{
		xercesc::DOMNodeList * childrenList = NULL;
		xercesc::DOMNode * found = NULL;
		XMLCh * textNodeName = NULL;

		/*
		*  Returns a NULL pointer if the parent node has no child nodes
		*  or no value is passed representing the name of the tag. 
		*/
		if(parent->hasChildNodes() && nameOfTag != NULL)
		{
			/*
			*  Call to getXMLTag function to find the node with name nameOfTag.
			*/
			found = getXMLTag(parent, nameOfTag);
			/*
			*  Returns NULL pointer if no node with that name is found, 
			*  or if the node found has no child nodes and therefore no
			*  TextNodes.
			*/
			if(found == NULL || !found->hasChildNodes())
			{
				return NULL;
			}

			/*
			*  Assigns the children of the found node to a DOMNodeList and then 
			*  iterates over them searching for a node which has a name "#text"
			*  and is of NodeType TEXT_NODE (ensuring the node in question cotains
			*  text values). Returns the node if true, else if all nodes have 
			*  returned false then a NULL pointer is returned.
			*/ 
			textNodeName = xercesc::XMLString::transcode("#text");
			childrenList = found->getChildNodes();

			for(XMLSize_t i =0; i < childrenList->getLength(); ++i)
			{
				if(xercesc::XMLString::equals(textNodeName, 
					childrenList->item(i)->getNodeName()) &&
					childrenList->item(i)->getNodeType() == 
					xercesc::DOMNode::NodeType::TEXT_NODE)													  
				{
					xercesc::XMLString::release(&textNodeName);	
					return childrenList->item(i);	
				}
			}

			xercesc::XMLString::release(&textNodeName);	
			return NULL;
		}
		else
		{
			return NULL;
		}
	}

	/*
	*  Searches throught the attributes associated with a given node for an
	*  attribute with name nameOfAttr. Returns either the attribute as a 
	*  DOMNode pointer, as a DOMAttr type is an abstraction of DOMNode, or
	*  a NULL pointer if no attribute with that name was found in the DOMNode.
	*  Args: currentNode - The node in which the attribute should be held
	*                      (input).
	*        nameOfAttr  - The name of the attribute that is being 
	*					    searched for (input).
	*  Rets: Pointer to a DOMNode which represents the attribute searched for,
	*        or a NULL pointer.       
	*/
	xercesc::DOMNode * getAttribute(const xercesc::DOMNode * const currentNode,
		                            const XMLCh * const nameOfAttr) 
	{
		/*
		*  Ensures that the node has attributes, and a name has been passed in.
		*/
		if(currentNode->hasAttributes() && nameOfAttr != NULL)
		{
			/*
			*  Assigns the attributes of the node to a DOMNamedNodeMap, which is 
			*  then iterated over. A check is then made as to whether the name of
			*  the attribute is equivalent to nameOfAttr(name of node searche for).
			*/
			xercesc::DOMNamedNodeMap * attributeMap = currentNode->getAttributes();

			for(XMLSize_t i =0; i < attributeMap->getLength(); ++i)
			{
				if(xercesc::XMLString::equals(nameOfAttr, 
					attributeMap->item(i)->getNodeName())) 
				{
					return attributeMap->item(i);
				}
			}

			/*
			*  No attribute with name nameOfATtr was found, therefore return NULL.
			*/
			return NULL; 
		}
		else
		{
			return NULL; 
		}
	}

	/*
	*  Pulls out any relevant information from an XML signature, trascodes it to
	*  UTF-8 and then returns these values as std::string.
	*  Args: sigElement   - The DOMNode which should contain the XML tags 
	*                       associated with XML Signatures (input).
	*        refUri       - The reference URI from the XML Signature, 
	*                       which has been	trimmed to only include 
	*                       the fragment (output).
	*        sigMethodAlg - The algorithm which has been used to sign the 
	*                       document (output).
	*        digestMethod - The algorithm used by the signature to encrypt
	*                       the data from the document (output).
	*        digestVal    - The encrypted data of the XML signature (output).
	*        signatureVal - The hashed data from the document, which is hashed
	*                       using the algorithm specified in sigMethodAlg (output).
	*        trkId        - The transport key identifier used to encrypt the 
	*                       data from the document, held in digestVal (output).
	*        hmacOutLen   - Truncated length of the HMAC string, if HMAC is the
	*                       signature method algorithm (output).
	*  Rets: Standard error code.   
	*/
	ScErrno pullOutSignatureData(xercesc::DOMNode *   sigElement,
		std::string *       refUri, 
		std::string * sigMethodAlg, 
		std::string * digestMethod, 
		std::string *    digestVal, 
		std::vector<unsigned char> & signatureVal, 
		std::string *        trkId,
		size_t *   hmacOutLen)
	{
		XMLCh            * tempChar   = NULL;
		xercesc::DOMNode * signedInfo = NULL;
		xercesc::DOMNode * tempNode   = NULL;
		xercesc::DOMNode * tempNode2  = NULL;
		xercesc::DOMNode * parentNode = NULL;
		xercesc::DOMNodeList * childrenOfSignedInfo = NULL;
		ScErrno returnedErrorCode = ScErrno::SUCCESS;

		if(!sigElement->hasChildNodes())
		{
			return ScErrno::RESOURCE_NOT_EXIST;	
		}

		/*
		*  Obtains the SignedInfo XML tag from the node passed.  
		*/
		tempChar = xercesc::XMLString::transcode("SignedInfo");
		signedInfo = getXMLTag(sigElement, tempChar); 
		xercesc::XMLString::release(&tempChar);

		/* 
		*  Ensures that the SignedInfo tag exists, is not NULL, and has 
		*  child nodes.
		*/
		if(signedInfo == NULL || !signedInfo->hasChildNodes()) 
		{
			return ScErrno::RESOURCE_NOT_EXIST;
		}

		/*
		*  Obtains the SignatureMethod XML tag from the signedInfo node.  
		*/
		tempChar = xercesc::XMLString::transcode("SignatureMethod");
		parentNode = getXMLTag(signedInfo, tempChar);
		xercesc::XMLString::release(&tempChar);

		if(parentNode == NULL)
		{
			return ScErrno::RESOURCE_NOT_EXIST;
		}

		/*
		*  Obtains the algorithm attribute from the SignatureMethod XML tag.
		*/
		tempChar = xercesc::XMLString::transcode("Algorithm");
		tempNode = getAttribute(parentNode, tempChar);
		xercesc::XMLString::release(&tempChar);

		if(tempNode == NULL)
		{
			return ScErrno::RESOURCE_NOT_EXIST;
		}

		/*
		*  Transcodes the attributes value to UTF-8, and assigns it to the 
		*  sigMethodAlg string. 
		*/
		*sigMethodAlg = xercesc::XMLString::transcode(tempNode->getNodeValue()); 
		tempNode = NULL;

		/* 
		*  Trims the sigMathodAlg, so that only the fragment data is held in the 
		*  string.
		*/
		returnedErrorCode = getFragment(sigMethodAlg);

		if(returnedErrorCode != ScErrno::SUCCESS)
		{
			return returnedErrorCode;
		}

		/*
		*  If the signature method algorithm is hmac-sha256, then it is necessary 
		*	to obtain a HMACOutputLength tag from the HMAC node and assign it to 
		*  the variable hmacOutLen. Elsewise, set hmacOutLen to NULL. 
		*/
		if(parentNode->hasChildNodes() && *sigMethodAlg == "hmac-sha256")
		{
			tempChar = xercesc::XMLString::transcode("HMACOutputLength");
			tempNode = getTextTag(parentNode,tempChar);
			if(tempNode == NULL){return ScErrno::RESOURCE_NOT_EXIST;}
			parentNode = NULL;
			xercesc::XMLString::release(&tempChar);

			try
			{
				/*
				*	Transcode a node value to UTF-8 and then attempts to 
				*  convert it to	a Size_t value using atoi.
				*/ 
				*hmacOutLen = atoi(xercesc::XMLString::transcode(
					tempNode->getNodeValue()));
				tempNode = NULL;
			}
			catch(...) 
			{
				return ScErrno::RESOURCE_NOT_EXIST;
			}
		}
		else
		{
			hmacOutLen = NULL; 
		}

		/*
		*  Obtains the reference XML tag.
		*/
		tempChar = xercesc::XMLString::transcode("Reference");
		parentNode = getXMLTag(signedInfo,tempChar);
		xercesc::XMLString::release(&tempChar);

		if(parentNode == NULL || !parentNode->hasChildNodes()
			|| !parentNode->hasAttributes())
		{
			return ScErrno::RESOURCE_NOT_EXIST;
		} 

		/*
		*  Attempts to obtain the URI attribute and if successful, assigns the 
		*  value to refUri string which is then trimmed to the fragment, using
		*  getFragment function.
		*/
		tempChar = xercesc::XMLString::transcode("URI");
		tempNode = getAttribute(parentNode,tempChar);
		xercesc::XMLString::release(&tempChar);

		if(tempNode == NULL)
		{
			return ScErrno::RESOURCE_NOT_EXIST;
		}

		*refUri = xercesc::XMLString::transcode(tempNode->getNodeValue());
		returnedErrorCode = getFragment(refUri);

		if(returnedErrorCode != ScErrno::SUCCESS)
		{
			return returnedErrorCode;
		}

		tempNode = NULL;

		/*
		*  Obtains the DigestMethod XML tag.
		*/
		tempChar = xercesc::XMLString::transcode("DigestMethod");
		tempNode = getXMLTag(parentNode,tempChar);
		xercesc::XMLString::release(&tempChar);

		if(tempNode == NULL)
		{
			return ScErrno::RESOURCE_NOT_EXIST;
		}

		/*
		*  Attempts to obtain the Algorithm attribute and if successful, assigns 
		*  the value to digestMethod string which is then trimmed to the fragment 
		*  using the getFragment function.
		*/
		tempChar = xercesc::XMLString::transcode("Algorithm");
		tempNode2 = getAttribute(tempNode, tempChar);
		xercesc::XMLString::release(&tempChar);

		if(tempNode == NULL)
		{
			return ScErrno::RESOURCE_NOT_EXIST;
		}

		*digestMethod =  xercesc::XMLString::transcode(tempNode2->getNodeValue());
		tempNode = NULL;
		tempNode2 = NULL;
		returnedErrorCode = getFragment(digestMethod);

		if(returnedErrorCode != ScErrno::SUCCESS)
		{
			return returnedErrorCode;
		}

		/*
		*  Obtains the DigestValue XML tag, and assigns its textual value
		*  to digestValue string.
		*/
		tempChar = xercesc::XMLString::transcode("DigestValue");
		tempNode = getTextTag(parentNode,tempChar);
		xercesc::XMLString::release(&tempChar);

		if(tempNode == NULL)
		{
			return ScErrno::RESOURCE_NOT_EXIST;
		}

		*digestVal = xercesc::XMLString::transcode(tempNode->getNodeValue());
		tempNode = NULL;
		parentNode = NULL;
		signedInfo = NULL;
		childrenOfSignedInfo = NULL;

		/*
		*  Obtains the SignatureValue XML tag, and assigns its textual value
		*  to signatureValue string.
		*/
		tempChar = xercesc::XMLString::transcode("SignatureValue");
		tempNode = getTextTag(sigElement,tempChar);
		xercesc::XMLString::release(&tempChar);

		if(tempNode == NULL)
		{
			return ScErrno::RESOURCE_NOT_EXIST;
		}

		returnedErrorCode = textToVector(tempNode,signatureVal);

		if(returnedErrorCode != ScErrno::SUCCESS)
		{
			return returnedErrorCode;
		}

		tempNode = NULL;

		/*
		*  Obtains the KeyInfo XML tag.
		*/
		tempChar = xercesc::XMLString::transcode("KeyInfo");
		parentNode = getXMLTag(sigElement,tempChar);
		xercesc::XMLString::release(&tempChar);

		if(parentNode == NULL)
		{
			return ScErrno::RESOURCE_NOT_EXIST;
		}

		/*
		*  Obtains the KeyName XML tag, and assigns its textual value
		*  to trkId string.
		*/
		tempChar = xercesc::XMLString::transcode("KeyName");
		tempNode = getTextTag(parentNode, tempChar);
		xercesc::XMLString::release(&tempChar);

		if(tempNode == NULL)
		{
			return ScErrno::RESOURCE_NOT_EXIST;
		}

		*trkId = xercesc::XMLString::transcode(tempNode->getNodeValue());
		parentNode = NULL;
		tempNode = NULL;
		return ScErrno::SUCCESS;
	}


	/*
	*  Obtains the reference URI value from a signature, trims it and returns it 
	*  by value.
	*  Args: signature - The parental DOMNode of the Reference tag, which contains
	*                    the signature (input).
	*  Rets: std::string containing the trimmed reference URI. 
	*/
	std::string getReferenceURI(xercesc::DOMNode * signature)
	{
		xercesc::DOMNode * parent = NULL;
		xercesc::DOMNode * child = NULL;
		xercesc::DOMNode * tempNode = NULL;
		XMLCh * nodeName = NULL;

		/*
		*  Obtain SignedInfo XML tag.
		*/
		nodeName = xercesc::XMLString::transcode("SignedInfo");
		parent = getXMLTag(signature,nodeName);
		xercesc::XMLString::release(&nodeName);

		if(parent == NULL)
		{
			return "";
		}

		/*
		*  Obtain Reference XML tag.
		*/
		nodeName = xercesc::XMLString::transcode("Reference");
		child = getXMLTag(parent,nodeName);
		xercesc::XMLString::release(&nodeName);

		if(child == NULL)
		{
			return "";
		}

		/*
		*  Get URI attribute, then transcode trim and return.
		*/
		nodeName = xercesc::XMLString::transcode("URI");
		tempNode = getAttribute(child, nodeName);
		xercesc::XMLString::release(&nodeName);

		if(tempNode == NULL)
		{
			return "";
		}

		std::string tempStr = xercesc::XMLString::transcode(tempNode->getNodeValue());
		getFragment(&tempStr);
		return tempStr;
	}

	/*
	*  Verifies the XML signature passed in using ScCrypto functions.
	*  Args: secCtx        - Security context associated with the sending KMS
	*                        (input).
	*        root          - The root node of the document that the signature
	*                        is related to (input).
	*        signatureNode - The node which contains the signature data (input).
	*  Rets: Standard error code.
	*/
	ScErrno verifySignature(SecCtx secCtx, xercesc::DOMDocument * doc,
		                    xercesc::DOMNode * root, xercesc::DOMNode * signatureNode)
	{
		std::string signatureMethodAlgorithm = "";
		std::string referenceURI   = "";
		std::string digestMethod   = "";
		std::string digestValue    = "";
		std::string transportKeyId = "";
		String trkID;
		std::vector<unsigned char> signatureValueVec,canonicalizedVec;
		std::vector<unsigned char> signedObjectVec,signatureValue;
		size_t HMACLength = 0;
		XMLCh * tempXMLChar = NULL;
		xercesc::DOMNode * referencedNode = NULL;
		ScErrno returnedErrorCode = ScErrno::SUCCESS;
		Buffer hmacBuff, dataBuff, hashBuff, signedObjectBuffer, signatureBuffer;

		/*
		*  Pulls out data from the signature node and assigns to variables.
		*/
		returnedErrorCode
		  = pullOutSignatureData(signatureNode, &referenceURI, 
		                         &signatureMethodAlgorithm, &digestMethod,
			                     &digestValue, signatureValue,
			                     &transportKeyId, &HMACLength);

		if(returnedErrorCode != ScErrno::SUCCESS)
		{
			return returnedErrorCode;
		}

		if(referenceURI != "" && signatureMethodAlgorithm != "" 
			&& digestMethod != "" && digestValue != ""
			&& signatureValue.size() > 0 && transportKeyId != "") 
		{
			/*
			*  Searches the document node for the node "Id" attribute, whose value
			*  is equivalent to the value held in referenceUri.
			*/
			tempXMLChar = xercesc::XMLString::transcode(referenceURI.c_str());
			referencedNode = getNodeWithAttributeId(root, tempXMLChar);
			xercesc::XMLString::release(&tempXMLChar);

			if(referencedNode == NULL)
			{
				return ScErrno::RESOURCE_NOT_EXIST;
			}

			/*
			*  If the signature method requires HMAC, then a comparison of 
			*  signatures is required. Else, if the method is ECDSA then this
			*  requires verification from SCCrypto.
			*/
			if(signatureMethodAlgorithm == "hmac-sha256") 
			{
				/*
				*  Canonicalize the XML tag which has the appropriate "Id" 
				*  attribute, held as referencedNode.
				*/
				returnedErrorCode = canonicalizeData(doc,referencedNode,
					                                 &dataBuff, canonicalizedVec);

				if(returnedErrorCode != ScErrno::SUCCESS)
				{
					return returnedErrorCode;
				}

				/*
				*  Hash the data which has been canonicalized. 
				*/
				returnedErrorCode = HashData(digestMethod.c_str(), dataBuff, 
					                         0, &hashBuff);

				dataBuff.length  = NULL;
				dataBuff.pointer = NULL;

				if(returnedErrorCode != ScErrno::SUCCESS)
				{
					hashBuff.length  = NULL;
					hashBuff.pointer = NULL;
					return returnedErrorCode;
				}

				/*
				* Compare the generated hash with the received digest value.
				*/
				std::string	hashString(reinterpret_cast<const char*>(hashBuff.pointer),
					                   hashBuff.length);
				if(hashString != digestValue)
				{
					// Digest are not equivalent
					return ScErrno::HMAC_NOT_VERIFIED;
				}

				/*
				*  Convert transport key ID into a C style string.
				*/
				trkID.pointer = transportKeyId.c_str();
				trkID.length = transportKeyId.length();

				tempXMLChar = xercesc::XMLString::transcode("SignedInfo");
				xercesc::DOMNode * signedInfo = getXMLTag(signatureNode,tempXMLChar);
				xercesc::XMLString::release(&tempXMLChar);
				if(signedInfo != NULL)
				{
					/*
					*  Data held in vector no longer needed and so can be 
					*  cleared for use below.
					*/
					canonicalizedVec.clear();
					returnedErrorCode = canonicalizeData(doc,signedInfo,
						&dataBuff,canonicalizedVec);

					if(returnedErrorCode != ScErrno::SUCCESS)
					{
						return returnedErrorCode;
					}

					/*
					*  HMAC the data gathered.
					*/
					returnedErrorCode = HmacTk(secCtx, 
						signatureMethodAlgorithm.c_str(), 
						trkID, 
						dataBuff, 
						HMACLength/8,
						&hmacBuff);

					hashBuff.length  = NULL;
					hashBuff.pointer = NULL;
					trkID.pointer    = NULL;
					trkID.length     = NULL;

				}
				else
				{
					return ScErrno::RESOURCE_NOT_EXIST;
				}

				/*
				*  Compare the value from HmacTk to the value held within the 
				*  represented as signatureValue. Return SUCCESS if equal, else 
				*  return HMAC_NOT_VERIFIED.
				*/
				std::vector<unsigned char>
				  hmacVec(&hmacBuff.pointer[0], &hmacBuff.pointer[0] + hmacBuff.length);

				if(signatureValue == hmacVec)
				{
					hmacBuff.length  = NULL;
					hmacBuff.pointer = NULL;
					return ScErrno::SUCCESS;
				}
				else
				{
					hmacBuff.length  = NULL;
					hmacBuff.pointer = NULL;
					return ScErrno::HMAC_NOT_VERIFIED;
				}
			}
			else if(signatureMethodAlgorithm == "ecdsa-sha256")
			{
				/*
				*	Convert the data which has been signed to a Buffer.
				*  Data represented as referencedNode.
				*/
				returnedErrorCode = nodeToBuffer(referencedNode,
					&signedObjectBuffer,
					signedObjectVec);

				if(returnedErrorCode != ScErrno::SUCCESS)
				{
					signedObjectBuffer.length = NULL;
					signedObjectBuffer.pointer = NULL;
					return returnedErrorCode;
				}

				/*
				*	Convert the signature value to a Buffer.
				*/
				returnedErrorCode = vecToBuff(&signatureBuffer,signatureValueVec);

				if(returnedErrorCode != ScErrno::SUCCESS)
				{
					signedObjectBuffer.length  = NULL;
					signedObjectBuffer.pointer = NULL;
					signatureBuffer.length     = NULL;
					signatureBuffer.pointer    = NULL;
					return returnedErrorCode;
				}

				/*
				*	Pass both Buffers of data to ScCrypto for verification.
				*/
				returnedErrorCode = EcdsaVerify(secCtx,signedObjectBuffer,
					                            signatureBuffer);

				signedObjectBuffer.length  = NULL;
				signedObjectBuffer.pointer = NULL;
				signatureBuffer.length     = NULL;
				signatureBuffer.pointer    = NULL;
				if(returnedErrorCode != ScErrno::SUCCESS)
				{
					return returnedErrorCode;
				}
			}
		}
		else
		{
			return ScErrno::RESOURCE_NOT_EXIST;
		}

		return ScErrno::SUCCESS;

	}

	/*
	*  Searches through a signed document for Signature XML tag, and then verifies
	*  said signature.
	*  Args: secCtx - Security context associated with the sending KMS (input).
	*        root   - Top most node of the document in question (input).
	*  Rets: Standard error code.
	*/
	ScErrno verifySigParent(SecCtx secCtx, xercesc::DOMDocument * doc,
		                    xercesc::DOMNode * root)
	{
		XMLCh * nodeName = NULL;
		xercesc::DOMNode * signatureObject = NULL;

		/*
		*  Searches for the XML tag with name "Signature" and assigns it
		*  to a DOMNode pointer if found.
		*/
		nodeName = xercesc::XMLString::transcode("Signature");
		signatureObject = getXMLTag(root, nodeName);
		xercesc::XMLString::release(&nodeName);

		if(signatureObject == NULL)
		{ 
			return ScErrno::RESOURCE_NOT_EXIST;
		}

		/*
		*  Passes the appropriate data to the verifySignature function and
		*  returns the value from that function.
		*/
		return verifySignature(secCtx, doc, root, signatureObject);
	}

	/*
	*  Obtains the transport key ID from a Key Provisioning responses
	*  UserDecryptKey tag and returns it as a std::string.
	*  Args: parent - DOMNode pointer to the EncryptedKey tag, which is a 
	*                 parent to the "ds:KeyInfo" tag (input).
	*  Rets: std::string representing the transport key ID.
	*/
	std::string getDSTransportKey(xercesc::DOMNode * parent)
	{
		XMLCh * nodeName = NULL;
		xercesc::DOMNode * node = NULL;
		xercesc::DOMNode * tempNode = NULL;
		xercesc::DOMNodeList * childen = NULL;

		/*
		*  Gets the KeyInfo tag.
		*/
		nodeName = xercesc::XMLString::transcode("KeyInfo");
		node = getXMLTag(parent, nodeName);
		xercesc::XMLString::release(&nodeName);

		if(node == NULL)
		{
			return "";
		}

		/*
		*  Gets the TextNode which is a child of the KeyName tag.
		*/
		nodeName = xercesc::XMLString::transcode("KeyName");
		tempNode = getTextTag(node, nodeName);
		xercesc::XMLString::release(&nodeName);

		if(tempNode == NULL)
		{
			return "";
		}

		return xercesc::XMLString::transcode(tempNode->getNodeValue());
	}

	/*
	*  Obtains the encryption method algorithm for the Transport Key within a Key 
	*  Provision responses KeyPack.
	*  Args: parent - The parental DOMNode of the EncryptionMethod tag (input).
	*  Rets: std::string representing the encryption method.  
	*/ 
	std::string getEncryptionMethodAlgorithm(xercesc::DOMNode * parent)
	{
		XMLCh * nodeName = NULL;
		xercesc::DOMNode * node     = NULL;
		xercesc::DOMNode * tempNode = NULL;
		std::string encAlgorithm = "";
		ScErrno retCode = ScErrno::SUCCESS;

		/*
		*  Obtain EncryptionMethod tag.
		*/
		nodeName = xercesc::XMLString::transcode("EncryptionMethod");
		node = getXMLTag(parent, nodeName);
		xercesc::XMLString::release(&nodeName);

		if(node == NULL)
		{
			return "";
		}

		/*
		*  Obtains the "Algorithm" attribute of the EncryptionMethod tag
		*  and then transcodes and trims the data before returning it.
		*/
		nodeName = xercesc::XMLString::transcode("Algorithm");
		tempNode = getAttribute(node,nodeName);
		xercesc::XMLString::release(&nodeName);
		node = NULL;

		if(tempNode == NULL)
		{
			return "";
		}

		encAlgorithm = xercesc::XMLString::transcode(tempNode->getNodeValue());
		retCode = getFragment(&encAlgorithm);

		if(retCode != ScErrno::SUCCESS)
		{
			return "";
		}

		tempNode = NULL;
		return encAlgorithm;
	}

	/*
	*  Strips out necessary data from certificate and converts it to key value 
	*  pairs, then packages it into Buffer type. 
	*  Args: node   - DOMNode pointer to KMSCertificate tag (input).
	*        buff   - Buffer in which to store pointer to vector (output).
	*        vec    - Vector to store KVPs (input/output).
	*        isRoot - Boolean to indicate if the certificate in question 
	*                 is a root or an external certificate (output).
	*  Rets: STandard error code.
	*/
	ScErrno certificateToBuffer(xercesc::DOMNode * node, Buffer * buff,
		std::vector<unsigned char> & vec,bool * isRoot)
	{
		XMLCh * nameOfNode;
		xercesc::DOMNode * parent = NULL;
		xercesc::DOMNode * child = NULL;
		xercesc::DOMNode * kmsDomain = NULL;
		xercesc::DOMNodeList * kmsDomainList = NULL;
		size_t lengthOfData = 0;
		int domainCount = 0;
		std::string dataFromNode = "";

		/*
		*  Ensure data passed in is valid.
		*/
		if(node != NULL && node->hasChildNodes() && isRoot != NULL && buff != NULL)
		{			
			/*
			*  Obtain Role and version number attributes from KMSCertificate tag.
			*/
			nameOfNode = xercesc::XMLString::transcode("Version");
			parent = getAttribute(node,nameOfNode);
			xercesc::XMLString::release(&nameOfNode);

			if(parent == NULL)
			{
				return ScErrno::RESOURCE_NOT_EXIST;
			}

			/*
			*  Transcode data to UTF-8 then convert the length to string.
			*  Then concatenate the length and the value onto the vector.
			*/ 
			dataFromNode = xercesc::XMLString::transcode(parent->getNodeValue());
			lengthOfData = dataFromNode.length();

			if(lengthOfData > 65535)
			{
				return ScErrno::GENERAL_FAILURE;
			}

			vec.push_back((unsigned char)(lengthOfData / 256));
			vec.push_back(lengthOfData % 256);
			vec.insert(vec.end(),dataFromNode.begin(),dataFromNode.end());
			dataFromNode = "";
			lengthOfData = 0;
			nameOfNode = xercesc::XMLString::transcode("Role");
			parent = getAttribute(node,nameOfNode);
			xercesc::XMLString::release(&nameOfNode);

			if(parent == NULL)
			{
				return ScErrno::RESOURCE_NOT_EXIST;
			}

			/*
			*  Transcode data to UTF-8 then convert the length to string.
			*  Then concatenate the length and the value onto the vector.
			*/
			dataFromNode = xercesc::XMLString::transcode(parent->getNodeValue());
			lengthOfData = dataFromNode.length();

			if(lengthOfData > 65535)
			{
				return ScErrno::GENERAL_FAILURE;
			}

			vec.push_back((unsigned char)(lengthOfData / 256));
			vec.push_back(lengthOfData % 256);
			vec.insert(vec.end(),dataFromNode.begin(),dataFromNode.end());

			if(dataFromNode == "Root")
			{
				*isRoot = true;
			}
			else
			{
				*isRoot = false;
			}

			dataFromNode = "";
			lengthOfData = 0;

			/*
			* Obtain Certificate URI.
			*/
			nameOfNode = xercesc::XMLString::transcode("CertUri");
			parent = getTextTag(node,nameOfNode);
			xercesc::XMLString::release(&nameOfNode);

			if(parent == NULL)
			{
				return ScErrno::RESOURCE_NOT_EXIST;
			}

			/*
			*  Transcode data to UTF-8 then convert the length to string.
			*  Then concatenate the length and the value onto the vector.
			*/
			dataFromNode = xercesc::XMLString::transcode(parent->getNodeValue());
			lengthOfData = dataFromNode.length();

			if(lengthOfData > 65535)
			{
				return ScErrno::GENERAL_FAILURE;
			}

			vec.push_back((unsigned char)(lengthOfData / 256));
			vec.push_back(lengthOfData % 256);
			vec.insert(vec.end(),dataFromNode.begin(),dataFromNode.end());

			parent = NULL;
			dataFromNode = "";
			lengthOfData = 0;

			/*
			*  Obtain KMS URI.
			*/
			nameOfNode = xercesc::XMLString::transcode("KmsUri");
			parent = getTextTag(node,nameOfNode);
			xercesc::XMLString::release(&nameOfNode);

			if(parent == NULL)
			{
				return ScErrno::RESOURCE_NOT_EXIST;
			}

			/*
			*  Transcode data to UTF-8 then convert the length to string.
			*  Then concatenate the length and the value onto the vector.
			*/
			dataFromNode = xercesc::XMLString::transcode(parent->getNodeValue());
			lengthOfData = dataFromNode.length();

			if(lengthOfData > 65535)
			{
				return ScErrno::GENERAL_FAILURE;
			}

			vec.push_back((unsigned char)(lengthOfData / 256));
			vec.push_back(lengthOfData % 256);
			vec.insert(vec.end(),dataFromNode.begin(),dataFromNode.end());

			parent = NULL;
			dataFromNode = "";
			lengthOfData = 0;

			/*
			*  Obtain Issuer data.
			*/
			nameOfNode = xercesc::XMLString::transcode("Issuer");
			parent = getTextTag(node,nameOfNode);
			xercesc::XMLString::release(&nameOfNode);

			if(parent == NULL)
			{
				return ScErrno::RESOURCE_NOT_EXIST;
			}

			/*
			*  Transcode data to UTF-8 then convert the length to string.
			*  Then concatenate the length and the value onto the vector.
			*/
			dataFromNode = xercesc::XMLString::transcode(parent->getNodeValue());
			lengthOfData = dataFromNode.length();

			if(lengthOfData > 65535)
			{
				return ScErrno::GENERAL_FAILURE;
			}

			vec.push_back((unsigned char)(lengthOfData / 256));
			vec.push_back(lengthOfData % 256);
			vec.insert(vec.end(),dataFromNode.begin(),dataFromNode.end());

			parent = NULL;
			dataFromNode = "";
			lengthOfData = 0;

			/*
			*  Obtain ValidFrom timestamp.
			*/
			nameOfNode = xercesc::XMLString::transcode("ValidFrom");
			parent = getTextTag(node,nameOfNode);
			xercesc::XMLString::release(&nameOfNode);

			if(parent == NULL)
			{
				return ScErrno::RESOURCE_NOT_EXIST;
			}

			/*
			*  Transcode data to UTF-8 then pass to convert_timestamp
			*  to convert from ISO 8601 to NTP style timestamp (RFC 5905)
			*  Then concatenate the length and the value onto the vector.
			*/
			dataFromNode = xercesc::XMLString::transcode(parent->getNodeValue());

			if(convert_timestamp(dataFromNode) == false)
			{
				return ScErrno::GENERAL_FAILURE;
			}

			lengthOfData = dataFromNode.length();

			if(lengthOfData > 65535)
			{
				return ScErrno::GENERAL_FAILURE;
			}

			vec.push_back((unsigned char)(lengthOfData / 256));
			vec.push_back(lengthOfData % 256);
			vec.insert(vec.end(),dataFromNode.begin(),dataFromNode.end());

			parent = NULL;
			dataFromNode = "";
			lengthOfData = 0;

			/*
			*  Obtain ValidTo timestamp.
			*/
			nameOfNode = xercesc::XMLString::transcode("ValidTo");
			parent = getTextTag(node,nameOfNode);
			xercesc::XMLString::release(&nameOfNode);

			if(parent == NULL)
			{
				return ScErrno::RESOURCE_NOT_EXIST;
			}

			/*
			*  Transcode data to UTF-8 then pass to convert_timestamp
			*  to convert from ISO 8601 to NTP style timestamp (RFC 5905)
			*  Then concatenate the length and the value onto the vector.
			*/
			dataFromNode = xercesc::XMLString::transcode(parent->getNodeValue());

			if(convert_timestamp(dataFromNode) == false)
			{
				return ScErrno::GENERAL_FAILURE;
			}

			lengthOfData = dataFromNode.length();

			if(lengthOfData > 65535)
			{
				return ScErrno::GENERAL_FAILURE;
			}

			vec.push_back((unsigned char)(lengthOfData / 256));
			vec.push_back(lengthOfData % 256);
			vec.insert(vec.end(),dataFromNode.begin(),dataFromNode.end());

			parent = NULL;
			dataFromNode = "";
			lengthOfData = 0;

			/*
			*  Obtain Revoked data.
			*/
			nameOfNode = xercesc::XMLString::transcode("Revoked");
			parent = getTextTag(node,nameOfNode);
			xercesc::XMLString::release(&nameOfNode);

			if(parent == NULL)
			{
				return ScErrno::RESOURCE_NOT_EXIST;
			}

			/*
			*  Transcode data to UTF-8 then convert the length to string.
			*  Then concatenate the length and the value onto the vector.
			*/
			dataFromNode = xercesc::XMLString::transcode(parent->getNodeValue());
			lengthOfData = dataFromNode.length();

			if(lengthOfData > 65535)
			{
				return ScErrno::GENERAL_FAILURE;
			}

			vec.push_back((unsigned char)(lengthOfData / 256));
			vec.push_back(lengthOfData % 256);
			vec.insert(vec.end(),dataFromNode.begin(),dataFromNode.end());

			parent = NULL;
			dataFromNode = "";
			lengthOfData = 0;

			/*
			*  Obtain UserIdFormat.
			*/
			nameOfNode = xercesc::XMLString::transcode("UserIdFormat");
			parent = getXMLTag(node,nameOfNode);
			xercesc::XMLString::release(&nameOfNode);

			if(parent == NULL)
			{
				return ScErrno::RESOURCE_NOT_EXIST;
			}

			/*
			*  Check to ensure "Conversion" attribute is set to "Hash"
			*/
			nameOfNode = xercesc::XMLString::transcode("Conversion");
			child = getAttribute(parent, nameOfNode);
			xercesc::XMLString::release(&nameOfNode);

			if(!xercesc::XMLString::equals(child->getNodeValue(), 
				                           xercesc::XMLString::transcode("Hash")))
			{
				return ScErrno::RESOURCE_NOT_EXIST;
			}

			/*
			*  Transcode data to UTF-8 then convert the length to string.
			*  Then concatenate the length and the value onto the vector.
			*/
			child = parent->getFirstChild();
			dataFromNode = xercesc::XMLString::transcode(child->getNodeValue());
			lengthOfData = dataFromNode.length();

			if(lengthOfData > 65535)
			{
				return ScErrno::GENERAL_FAILURE;
			}

			vec.push_back((unsigned char)(lengthOfData / 256));
			vec.push_back(lengthOfData % 256);
			vec.insert(vec.end(),dataFromNode.begin(),dataFromNode.end());

			parent = NULL;
			child = NULL;
			dataFromNode = "";
			lengthOfData = 0;

			/*
			*  Obtain Public Encryption Key Data.
			*/
			nameOfNode = xercesc::XMLString::transcode("PubEncKey");
			parent = getTextTag(node,nameOfNode);
			xercesc::XMLString::release(&nameOfNode);

			if(parent == NULL)
			{
				return ScErrno::RESOURCE_NOT_EXIST;
			}

			/*
			*  Transcode data to UTF-8 then convert the length to string.
			*  Then concatenate the length and the value onto the vector.
			*/
			dataFromNode = xercesc::XMLString::transcode(parent->getNodeValue());
			lengthOfData = dataFromNode.length();

			if(lengthOfData > 65535)
			{
				return ScErrno::GENERAL_FAILURE;
			}
			vec.push_back((unsigned char)(lengthOfData / 256));
			vec.push_back(lengthOfData % 256);
			vec.insert(vec.end(),dataFromNode.begin(),dataFromNode.end());

			parent = NULL;
			dataFromNode = "";
			lengthOfData = 0;

			/*
			*  Obtain PubAuthKey data.
			*/
			nameOfNode = xercesc::XMLString::transcode("PubAuthKey");
			parent = getTextTag(node,nameOfNode);
			xercesc::XMLString::release(&nameOfNode);

			if(parent == NULL)
			{
				return ScErrno::RESOURCE_NOT_EXIST;
			}

			/*
			*  Transcode data to UTF-8 then convert the length to string.
			*  Then concatenate the length and the value onto the vector.
			*/
			dataFromNode = xercesc::XMLString::transcode(parent->getNodeValue());
			lengthOfData = dataFromNode.length();

			if(lengthOfData > 65535)
			{
				return ScErrno::GENERAL_FAILURE;
			}

			vec.push_back((unsigned char)(lengthOfData / 256));
			vec.push_back(lengthOfData % 256);
			vec.insert(vec.end(),dataFromNode.begin(),dataFromNode.end());

			parent = NULL;
			dataFromNode = "";
			lengthOfData = 0;

			/*
			*  Obtain list of domains associated with KMS (if any).
			*/
			nameOfNode = xercesc::XMLString::transcode("KmsDomainList");
			parent = getXMLTag(node,nameOfNode);
			xercesc::XMLString::release(&nameOfNode);

			if(parent == NULL)
			{
				return ScErrno::RESOURCE_NOT_EXIST;
			}

			if(!parent->hasChildNodes()) //No child domains
			{
				lengthOfData = 0;
				vec.push_back((unsigned char)(lengthOfData / 256));
				vec.push_back(lengthOfData % 256);
			}
			else
			{
				kmsDomainList = parent->getChildNodes();
				XMLCh * kmsdom = xercesc::XMLString::transcode("KmsDomain");
				XMLSize_t kmsDomListSize = kmsDomainList->getLength();
				lengthOfData = kmsDomListSize;

				if(lengthOfData > 65535)
				{
					return ScErrno::GENERAL_FAILURE;
				}

				vec.push_back((unsigned char)(lengthOfData / 256));
				vec.push_back(lengthOfData % 256);
				lengthOfData = 0;

				for(XMLSize_t i =0; i < kmsDomListSize; ++i)
				{
					kmsDomain = kmsDomainList->item(i);

					if(xercesc::XMLString::equals(kmsdom,kmsDomain->getNodeName()))
					{
						++domainCount;
						dataFromNode
						  = xercesc::XMLString::transcode(
						      kmsDomain->getFirstChild()->getNodeValue());

						lengthOfData = dataFromNode.length();

						if(lengthOfData > 65535)
						{
							return ScErrno::GENERAL_FAILURE;
						}

						vec.push_back((unsigned char)(lengthOfData / 256));
						vec.push_back(lengthOfData % 256);
						vec.insert(vec.end(),dataFromNode.begin(),dataFromNode.end());
						dataFromNode = "";
						lengthOfData = 0;
					}
				}
				xercesc::XMLString::release(&kmsdom);
			}

			parent = NULL;
			return vecToBuff(buff,vec);
		}
		else
		{
			return ScErrno::RESOURCE_NOT_EXIST;
		}

	}

	//Obtain new transport key from KMSKeyProv and store it in SCCrypto
	/*
	*  Obtains a new transport key from the NewTransportKey tag within a keyprov
	*  request. This is then stored using ScCrypto function StoreTk.
	*  Args: secCtx     - Security Context (input).
	*        newTrKNode - The xerces node corresponding to the NewTransportKey
	*                     XML tag within the XML document (input).
	*        userUri    - Holds the UserUri, for use with StoreTk function
	*                     (input).
	*  Rets: Standard error code. 
	*/
	ScErrno getTransportKey(SecCtx secCtx, xercesc::DOMNode * newTrKNode,
		String userUri)
	{
		xercesc::DOMNode * encryptedKeyNode = NULL;
		xercesc::DOMNode * parentNode = NULL;
		xercesc::DOMNode * childNode = NULL;
		XMLCh * nameOfNode = NULL;

		std::vector<unsigned char> encTransKeyVec;
		std::vector<char> trkIdVec,newTrKIdVec; 
		std::string temp = "";
		std::string transAlg = "";
		std::string encTransKey = "";

		String transKeyId;
		String newTransKeyId;
		Buffer encNewTrK;

		ScErrno returnedValue = ScErrno::SUCCESS;

		/*
		*  Ensure the node being traversed has child nodes.
		*/
		if(!newTrKNode->hasChildNodes())
		{
			return ScErrno::RESOURCE_NOT_EXIST;
		}

		/*
		*  Attempt to obtain Encrypted key tag	which encapsulates all the
		*  encrypted key data.
		*/
		nameOfNode = xercesc::XMLString::transcode("EncryptedKey");
		encryptedKeyNode = getXMLTag(newTrKNode, nameOfNode);
		xercesc::XMLString::release(&nameOfNode);

		if(encryptedKeyNode == NULL || !encryptedKeyNode->hasChildNodes())
		{
			return ScErrno::RESOURCE_NOT_EXIST;
		}

		xercesc::XMLString::release(&nameOfNode);

		/*
		*  Attempts to obtain the algorithm used to encrypt the key data,
		*  from the algorithm attribute of the EncryptionMethod tag.
		*/ 
		nameOfNode = xercesc::XMLString::transcode("EncryptionMethod");	
		parentNode = getXMLTag(encryptedKeyNode,nameOfNode);
		xercesc::XMLString::release(&nameOfNode);

		if(parentNode == NULL)
		{
			return ScErrno::RESOURCE_NOT_EXIST;
		}

		xercesc::XMLString::release(&nameOfNode);
		nameOfNode = xercesc::XMLString::transcode("Algorithm");	
		childNode = getAttribute(parentNode, nameOfNode);
		xercesc::XMLString::release(&nameOfNode);

		if(childNode == NULL)
		{
			return ScErrno::RESOURCE_NOT_EXIST;
		}

		/*
		*  Obtains and trims the algorithm.
		*/
		transAlg = xercesc::XMLString::transcode(childNode->getNodeValue());
		returnedValue = getFragment(&transAlg);
		parentNode = NULL;
		childNode = NULL;
		xercesc::XMLString::release(&nameOfNode);

		/*
		*  Obtains and strips the key name, which represents the Transport Key ID
		*  used to encrypt the data within CipherValue.
		*/
		nameOfNode = xercesc::XMLString::transcode("KeyInfo");	
		parentNode = getXMLTag(encryptedKeyNode,nameOfNode);
		xercesc::XMLString::release(&nameOfNode);

		if(parentNode == NULL)
		{
			return ScErrno::RESOURCE_NOT_EXIST;
		}
		xercesc::XMLString::release(&nameOfNode);
		nameOfNode = xercesc::XMLString::transcode("KeyName");	
		childNode = getTextTag(parentNode, nameOfNode);
		xercesc::XMLString::release(&nameOfNode);

		if(childNode == NULL)
		{
			return ScErrno::RESOURCE_NOT_EXIST;
		}

		/*
		*  Assigns the TrKId to a vector and then to a c-style String.
		*/
		temp = xercesc::XMLString::transcode(childNode->getNodeValue());
		stringToVec(trkIdVec,temp);
		returnedValue = vecToString(&transKeyId,trkIdVec);

		if(returnedValue != ScErrno::SUCCESS)
		{
			return returnedValue;
		}

		temp = "";
		parentNode = NULL;
		childNode = NULL;
		xercesc::XMLString::release(&nameOfNode);

		/*
		*  Obtains and strips the CipherValue, which represents the encrypted 
		*  transport key.
		*/
		nameOfNode = xercesc::XMLString::transcode("CipherData");	
		parentNode = getXMLTag(encryptedKeyNode,nameOfNode);
		xercesc::XMLString::release(&nameOfNode);

		if(parentNode == NULL)
		{
			return ScErrno::RESOURCE_NOT_EXIST;
		}

		xercesc::XMLString::release(&nameOfNode);
		nameOfNode = xercesc::XMLString::transcode("CipherValue");	
		childNode = getTextTag(parentNode, nameOfNode);
		xercesc::XMLString::release(&nameOfNode);

		if(childNode == NULL)
		{
			return ScErrno::RESOURCE_NOT_EXIST;
		}

		/*
		*  Assigns the CipherValue to a vector and then to a Buffer.
		*/
		returnedValue = base64XMLToVector(childNode,encTransKeyVec);

		if(returnedValue != ScErrno::SUCCESS)
		{
			return returnedValue;
		}

		parentNode = NULL;
		childNode = NULL;
		returnedValue = vecToBuff(&encNewTrK,encTransKeyVec);

		if(returnedValue != ScErrno::SUCCESS)
		{
			return returnedValue;
		}

		/*
		*  Obtains and strips the CarriedKeyName, which represents the encrypted 
		*  transport keys ID.
		*/
		nameOfNode = xercesc::XMLString::transcode("CarriedKeyName");	
		parentNode = getTextTag(encryptedKeyNode,nameOfNode);
		xercesc::XMLString::release(&nameOfNode);

		if(parentNode == NULL)
		{
			return ScErrno::RESOURCE_NOT_EXIST;
		}

		/*
		*  Assigns the new TrKId to a vector and then to a c-style String.
		*/
		temp  = xercesc::XMLString::transcode(parentNode->getNodeValue());
		stringToVec(newTrKIdVec,temp);
		returnedValue = vecToString(&newTransKeyId, newTrKIdVec);

		if(returnedValue != ScErrno::SUCCESS)
		{
			return returnedValue;
		}

		temp = "";
		parentNode = NULL;

		/*
		*  Call ScCrypto function to decrypt and store new Transport Key.
		*/
		returnedValue = StoreTk(secCtx,userUri, transAlg.c_str(), transKeyId,
			                    newTransKeyId,encNewTrK);
		encNewTrK.length = NULL;
		encNewTrK.pointer = NULL;

		if(returnedValue != ScErrno::SUCCESS)
		{
			return returnedValue;
		}

		return ScErrno::SUCCESS;
	}

	/*
	*  Pulls out and returns the encrypted value of a given key within a KeyProv
	*	key pack.
	*  Args: parent - The parent node of the CipherData tag, which is the 
	*                 EncryptedKey tag.
	*  Rets: std::string containg the encrypted cipher value. 
	*/
	std::string getCipherValue(xercesc::DOMNode * parent)
	{
		xercesc::DOMNode * tempNode = NULL;
		xercesc::DOMNode * secondaryTempNode = NULL;

		XMLCh * nameOfNode = xercesc::XMLString::transcode("CipherData");
		tempNode = getXMLTag(parent,nameOfNode);
		xercesc::XMLString::release(&nameOfNode);

		if(tempNode == NULL)
		{
			return "";
		}

		nameOfNode = xercesc::XMLString::transcode("CipherValue");
		secondaryTempNode = getTextTag(tempNode,nameOfNode);
		xercesc::XMLString::release(&nameOfNode);

		if(secondaryTempNode == NULL)
		{
			return "";
		}

		return xercesc::XMLString::transcode(secondaryTempNode->getNodeValue());
	}

	/*
	*  Converts a std::string to a C style string.
	*  Args: strin  - The std::string holding the data (input)
	*        strout - The C string to hold the data (output).
	*/
	void stdstringToString(const std::string & strin, String * strout)
	{
		strout->length = strin.length();
		strout->pointer = strin.data();	
	}

	/*
	*  Pulls out relevant data from a keypack and stores it using ScCrypto 
	*  function.
	*  Args: secCtx        - Security Context (input).
	*        KMSKeySetNode - KmsKeySet tag which is parent to key set data
	*                        (input).
	*  Rets: Standard error code.
	*/
	ScErrno getKeyPack(SecCtx secCtx, xercesc::DOMNode * KMSKeySetNode)
	{
		ScErrno retCode = ScErrno::SUCCESS;

		if(!KMSKeySetNode->hasChildNodes())
		{
			retCode = ScErrno::RESOURCE_NOT_EXIST;
		}
		else
		{
			XMLCh * nameOfNode = NULL;
			std::string uIDStr,transAlgStr,trkIdStr,vFromStr,vToStr,uUriStr; 
			std::string _UDKString, _SSKString, _PVTString;
			xercesc::DOMNode * tempNode = NULL;
			xercesc::DOMNode * UDKTag = NULL;
			xercesc::DOMNode * encryptedKeyTag = NULL;
			xercesc::DOMNodeList * childrenOfEncryptedKey = NULL;
			nameOfNode = xercesc::XMLString::transcode("UserUri");
			tempNode = getTextTag(KMSKeySetNode,nameOfNode);
			xercesc::XMLString::release(&nameOfNode);

			if(tempNode == NULL)
			{
				retCode = ScErrno::RESOURCE_NOT_EXIST;
			}
			else
			{
				uUriStr = xercesc::XMLString::transcode(tempNode->getNodeValue());
				if(uUriStr == "")
				{
					retCode = ScErrno::RESOURCE_NOT_EXIST;
				}
			}

			if(ScErrno::SUCCESS == retCode)
			{
				nameOfNode = xercesc::XMLString::transcode("UserID");
				tempNode = getTextTag(KMSKeySetNode,nameOfNode);
				xercesc::XMLString::release(&nameOfNode);

				if(tempNode == NULL)
				{
					retCode = ScErrno::RESOURCE_NOT_EXIST;
				}
				else
				{
					uIDStr = xercesc::XMLString::transcode(tempNode->getNodeValue());
					if(uIDStr == "")
					{
						retCode = ScErrno::RESOURCE_NOT_EXIST;
					}
				}
			}

			if(ScErrno::SUCCESS == retCode)
			{
				nameOfNode = xercesc::XMLString::transcode("ValidFrom");
				tempNode = getTextTag(KMSKeySetNode,nameOfNode);
				xercesc::XMLString::release(&nameOfNode);

				if(tempNode == NULL)
				{
					retCode = ScErrno::RESOURCE_NOT_EXIST;
				}
				else
				{
					vFromStr = xercesc::XMLString::transcode(tempNode->getNodeValue());

					if(vFromStr == "")
					{
						retCode = ScErrno::RESOURCE_NOT_EXIST;
					}
					else
					{
						if(!convert_timestamp(vFromStr))
						{
							retCode = ScErrno::TIMESTAMP_FORMAT_INVALID;
						}
					}
				}
			}

			if(ScErrno::SUCCESS == retCode)
			{
				nameOfNode = xercesc::XMLString::transcode("ValidTo");
				tempNode = getTextTag(KMSKeySetNode,nameOfNode);
				xercesc::XMLString::release(&nameOfNode);

				if(tempNode == NULL)
				{
					retCode = ScErrno::RESOURCE_NOT_EXIST;
				}
				else
				{
					vToStr = xercesc::XMLString::transcode(tempNode->getNodeValue());

					if(vToStr == "")
					{
						retCode = ScErrno::RESOURCE_NOT_EXIST;
					}
					else
					{
						if(!convert_timestamp(vToStr))
						{
							retCode = ScErrno::TIMESTAMP_FORMAT_INVALID;
						}
					}
				}
			}

			if(ScErrno::SUCCESS == retCode)
			{ 
				nameOfNode = xercesc::XMLString::transcode("UserDecryptKey");
				UDKTag = getXMLTag(KMSKeySetNode,nameOfNode);
				xercesc::XMLString::release(&nameOfNode);

				if(UDKTag == NULL)
				{
					retCode = ScErrno::RESOURCE_NOT_EXIST;
				}

				if(ScErrno::SUCCESS == retCode)
				{
					nameOfNode = xercesc::XMLString::transcode("EncryptedKey");
					encryptedKeyTag = getXMLTag(UDKTag,nameOfNode);
					xercesc::XMLString::release(&nameOfNode);
					if(encryptedKeyTag == NULL)
					{
						retCode = ScErrno::RESOURCE_NOT_EXIST;
					}
					if(ScErrno::SUCCESS == retCode)
					{
						if(encryptedKeyTag->hasChildNodes())
						{
							transAlgStr = getEncryptionMethodAlgorithm(encryptedKeyTag);

							if(transAlgStr != "")
							{
								trkIdStr = getDSTransportKey(encryptedKeyTag);

								if(trkIdStr == "")
								{
									retCode = ScErrno::RESOURCE_NOT_EXIST;
								}
								else
								{
									_UDKString = getCipherValue(encryptedKeyTag);

									if(_UDKString == "")
									{
										retCode = ScErrno::RESOURCE_NOT_EXIST;
									}
								}
							}
							else
							{
								retCode = ScErrno::RESOURCE_NOT_EXIST;
							}
						}
						else
						{
							retCode = ScErrno::RESOURCE_NOT_EXIST;
						}
					}
				}
			}

			if(ScErrno::SUCCESS == retCode)
			{
				nameOfNode = xercesc::XMLString::transcode("UserSigningKeySSK");
				UDKTag = getXMLTag(KMSKeySetNode,nameOfNode);
				xercesc::XMLString::release(&nameOfNode);

				if(UDKTag == NULL)
				{
					retCode = ScErrno::RESOURCE_NOT_EXIST;
				}
				else
				{
					nameOfNode = xercesc::XMLString::transcode("EncryptedKey");
					encryptedKeyTag = getXMLTag(UDKTag,nameOfNode);
					xercesc::XMLString::release(&nameOfNode);

					if(encryptedKeyTag == NULL)
					{
						retCode = ScErrno::RESOURCE_NOT_EXIST;
					}
					else
					{
						_SSKString = getCipherValue(encryptedKeyTag);

						if(_SSKString == "")
						{
							retCode = ScErrno::RESOURCE_NOT_EXIST;
						}

					}
				}
			}

			if(ScErrno::SUCCESS == retCode)
			{
				nameOfNode = xercesc::XMLString::transcode("UserPubTokenPVT");
				UDKTag = getXMLTag(KMSKeySetNode,nameOfNode);
				xercesc::XMLString::release(&nameOfNode);

				if(UDKTag == NULL)
				{
					retCode = ScErrno::RESOURCE_NOT_EXIST;
				}
				else
				{
					nameOfNode = xercesc::XMLString::transcode("EncryptedKey");
					encryptedKeyTag = getXMLTag(UDKTag,nameOfNode);
					xercesc::XMLString::release(&nameOfNode);

					if(encryptedKeyTag == NULL)
					{
						retCode = ScErrno::RESOURCE_NOT_EXIST;
					}
					else
					{	
						_PVTString = getCipherValue(encryptedKeyTag);

						if(_PVTString == "")	
						{
							retCode = ScErrno::RESOURCE_NOT_EXIST;
						}
					}
				}
			}		

			// Check all necessary data is populated.
			if(ScErrno::SUCCESS == retCode)
			{
				String userId, transportKeyId,validFrom,validTo,userUri;
				Buffer encSakkeUDK, encEccsiSSK, encEccsiPVT;
				std::vector<unsigned char> UDKEncVec,SSKEncVec,PVTEncVec;
				std::vector<unsigned char> UDKVec,SSKVec,PVTVec;			

				stdstringToString(uUriStr, &userUri);
				stdstringToString(uIDStr, &userId);
				stdstringToString(trkIdStr, &transportKeyId);
				stdstringToString(vFromStr, &validFrom);
				stdstringToString(vToStr, &validTo);

				if(ScErrno::SUCCESS == retCode)
				{
					stringToVec(UDKEncVec,_UDKString);
					if(base64Decode(UDKEncVec,UDKVec))
					{
						retCode = vecToBuff(&encSakkeUDK,UDKVec);
					}
					else
					{
						retCode = ScErrno::RESOURCE_DENIED;
					}
				}

				if(ScErrno::SUCCESS == retCode)
				{
					stringToVec(SSKEncVec,_SSKString);
					if(base64Decode(SSKEncVec,SSKVec))
					{
						retCode = vecToBuff(&encEccsiSSK,SSKVec);
					}
					else
					{
						retCode = ScErrno::RESOURCE_DENIED;
					}
				}

				if(ScErrno::SUCCESS == retCode)
				{
					stringToVec(PVTEncVec,_PVTString);

					if(base64Decode(PVTEncVec,PVTVec))
					{
						retCode = vecToBuff(&encEccsiPVT,PVTVec);
					}
					else
					{
						retCode = ScErrno::RESOURCE_DENIED;
					}
				}

				if(ScErrno::SUCCESS == retCode)
				{
					// Call SCCrypto function.
					retCode = StoreKeyPack(secCtx, userUri, userId,
						                   transAlgStr.c_str(), transportKeyId, 
						                   validFrom, validTo, encSakkeUDK, 
						                   encEccsiSSK, encEccsiPVT);
				}
			}
			else
			{
				retCode = ScErrno::GENERAL_FAILURE;
			}
		}

		return retCode;
	}

	/*
	*  Creates a new DOMNode as a child of toAddTo with a given name, and 
	*  potentially a child node of its own.
	*  Args: temp    - Holds the memory address for the DOMNode which has been
	*                  created (input).
	*        doc     - The DOMDocument that is being modified (input).
	*                  Only DOMDocument has the correct functions to create 
	*                  nodes.
	*        toAddTo -The DOMNOde that is having a child appended to it (input).
	*        name    - The name of the node to be created (input).
	*        value   - The value of the node to be created, ignored if NULL (input).
	*  Rets: Standard error code.
	*/
	ScErrno createNode(xercesc::DOMNode * temp, xercesc::DOMDocument * doc, 
		xercesc::DOMNode * toAddTo, XMLCh * name, XMLCh * value,
		XMLNAMESPACE ns)
	{
		ScErrno retCode = ScErrno::SUCCESS;
		XMLCh * namespaceUri = NULL;
		xercesc::DOMText * text = NULL;

		if(name == NULL || doc == NULL || toAddTo == NULL)
		{
			retCode = ScErrno::RESOURCE_NOT_EXIST;
		}
		else
		{
			switch(ns)
			{
			case 0:
				namespaceUri = xercesc::XMLString::transcode(MAIN_NAMESPACE.c_str());
				break;
			case 1:
				namespaceUri = xercesc::XMLString::transcode(DS_NAMESPACE.c_str());
				break;
			case 2:
				namespaceUri = xercesc::XMLString::transcode(XSI_NAMESPACE.c_str());
				break;
			case 3:
				namespaceUri = xercesc::XMLString::transcode(SE_NAMESPACE.c_str());
				break;
			}

			try
			{
				/*
				*  Creates a new DOMNode and assigns it to temp.
				*/
				temp = doc->createElementNS(namespaceUri, name);

				/*
				* Creates a child text node and gives it data, if a value is passed in.
				*/
				if(value != NULL)
				{
					text = doc->createTextNode(value); 
					temp->appendChild(text); 
				}

				/*
				*  Appends the new DOMNode to the proposed parental node, toAddTo.
				*/
				toAddTo->appendChild(temp); 
			}
			catch(...)
			{
				retCode = ScErrno::GENERAL_FAILURE; 
			}
		}
		return retCode;
	}

	/*
	*  Function to create a Signature XML tag, and populates it with appropriate data,
	*  for an XML document.
	*  Args: parentOfSig - The parent node which will encapsulate the digital 
	*                      signature (input).
	*        doc         - The DOMDocument which will contaion the signature (input).
	*  Rets: Standard error code.
	*/
	ScErrno createSignatureTag(xercesc::DOMElement * parentOfSig, xercesc::DOMDocument * doc)
	{
		xercesc::DOMElement * signature = NULL;
		xercesc::DOMElement * tempElement = NULL;
		xercesc::DOMElement * sigInfo = NULL;
		xercesc::DOMElement * refElement = NULL;
		xercesc::DOMElement * parent = NULL;
		xercesc::DOMElement * child = NULL;
		xercesc::DOMNode * tempNode = NULL;
		XMLCh * nameOfTag = NULL;
		XMLCh * valueOfTag = NULL;
		ScErrno errCode = ScErrno::SUCCESS;

		if(parentOfSig == NULL || doc == NULL)
		{
			return ScErrno::RESOURCE_NOT_EXIST;
		}

		/*
		*  Create a signature tag.
		*/
		nameOfTag = xercesc::XMLString::transcode("Signature");
		valueOfTag = xercesc::XMLString::transcode(DS_NAMESPACE.c_str());

		signature = doc->createElementNS(valueOfTag,nameOfTag);
		parentOfSig->appendChild(signature);

		xercesc::XMLString::release(&valueOfTag);
		xercesc::XMLString::release(&nameOfTag);

		if(errCode != ScErrno::SUCCESS)
		{
			return errCode;
		}

		errCode = addNamespaceToElement(signature, "xmlns",DS_NAMESPACE,NAMESPACE_URI);

		if(errCode != ScErrno::SUCCESS)
		{
			return errCode;
		}

		/*
		*  Create SignedInfo tag.
		*/
		nameOfTag = xercesc::XMLString::transcode("SignedInfo");
		errCode = createNode(tempNode, doc, signature,nameOfTag,NULL,XMLNAMESPACE::DS);
		tempNode = getXMLTag(signature, nameOfTag);
		xercesc::XMLString::release(&nameOfTag);

		if(errCode != ScErrno::SUCCESS)
		{
			return errCode;
		}
		try
		{
			sigInfo = dynamic_cast<xercesc::DOMElement *>(tempNode);
		}
		catch(...)
		{
			return ScErrno::GENERAL_FAILURE;
		}

		tempNode = NULL;

		/*
		*  Create CanonicalizationMethod tag with algorithm attribute.
		*/
		nameOfTag = xercesc::XMLString::transcode("CanonicalizationMethod");
		errCode = createNode(tempNode, doc, sigInfo,nameOfTag,NULL,XMLNAMESPACE::DS);
		tempNode = getXMLTag(sigInfo, nameOfTag);
		xercesc::XMLString::release(&nameOfTag);

		if(errCode != ScErrno::SUCCESS)
		{
			return errCode;
		}

		try
		{
			parent = dynamic_cast<xercesc::DOMElement *>(tempNode);
		}
		catch(...)
		{
			return ScErrno::GENERAL_FAILURE;
		}

		tempNode = NULL;
		nameOfTag = xercesc::XMLString::transcode("Algorithm");
		valueOfTag = xercesc::XMLString::transcode(C14N_ALGORITHM.c_str());
		errCode = addAttribute(parent,nameOfTag,valueOfTag);
		xercesc::XMLString::release(&nameOfTag);
		xercesc::XMLString::release(&valueOfTag);
		parent = NULL;

		/*
		*  Create SignatureMethod tag and algorithm attribute.
		*/
		nameOfTag = xercesc::XMLString::transcode("SignatureMethod");
		errCode = createNode(tempNode, doc, sigInfo,nameOfTag,NULL,XMLNAMESPACE::DS);
		tempNode = getXMLTag(sigInfo, nameOfTag);
		xercesc::XMLString::release(&nameOfTag);

		if(errCode != ScErrno::SUCCESS)
		{
			return errCode;
		}

		try
		{
			parent = dynamic_cast<xercesc::DOMElement *>(tempNode);
		}
		catch(...)
		{
			return ScErrno::GENERAL_FAILURE;
		}

		tempNode = NULL;
		nameOfTag = xercesc::XMLString::transcode("Algorithm");
		valueOfTag = xercesc::XMLString::transcode(HMAC_ALGORITHM.c_str());
		errCode = addAttribute(parent,nameOfTag,valueOfTag);
		xercesc::XMLString::release(&nameOfTag);
		xercesc::XMLString::release(&valueOfTag);

		/* 
		*  Create HMACOutputLength text tag.
		*/
		nameOfTag = xercesc::XMLString::transcode("HMACOutputLength");
		valueOfTag = xercesc::XMLString::transcode(std::to_string(HMAC_OUT_LEN).c_str());
		errCode = createNode(tempNode,doc,parent,nameOfTag,valueOfTag, XMLNAMESPACE::DS);

		if(errCode != ScErrno::SUCCESS)
		{
			return errCode;
		}

		tempNode = NULL;
		parent = NULL;

		/*
		*  Create ReferenceTag with URI attribute.
		*/
		nameOfTag = xercesc::XMLString::transcode("Reference");
		errCode = createNode(tempNode, doc, sigInfo,nameOfTag,NULL,XMLNAMESPACE::DS);
		tempNode = getXMLTag(sigInfo, nameOfTag);
		xercesc::XMLString::release(&nameOfTag);

		if(errCode != ScErrno::SUCCESS)
		{
			return errCode;
		}

		try
		{
			refElement = dynamic_cast<xercesc::DOMElement *>(tempNode);
		}
		catch(...)
		{
			return ScErrno::GENERAL_FAILURE;
		}

		tempNode = NULL;
		nameOfTag = xercesc::XMLString::transcode("URI");
		valueOfTag = xercesc::XMLString::transcode(REFERENCE_URI.c_str());
		errCode = addAttribute(refElement,nameOfTag,valueOfTag);
		xercesc::XMLString::release(&nameOfTag);
		xercesc::XMLString::release(&valueOfTag);

		/*
		*  Creates DigestMethod tag with Algorithm attribute. 
		*/
		nameOfTag = xercesc::XMLString::transcode("DigestMethod");
		errCode = createNode(tempNode, doc, refElement,nameOfTag,NULL,XMLNAMESPACE::DS);
		tempNode = getXMLTag(refElement, nameOfTag);
		xercesc::XMLString::release(&nameOfTag);

		if(errCode != ScErrno::SUCCESS)
		{
			return errCode;
		}

		try
		{
			parent = dynamic_cast<xercesc::DOMElement *>(tempNode);
		}
		catch(...)
		{
			return ScErrno::GENERAL_FAILURE;
		}

		tempNode = NULL;
		nameOfTag = xercesc::XMLString::transcode("Algorithm");
		valueOfTag = xercesc::XMLString::transcode(DIGEST_ALGORITHM.c_str());
		errCode = addAttribute(parent,nameOfTag,valueOfTag);
		xercesc::XMLString::release(&nameOfTag);
		xercesc::XMLString::release(&valueOfTag);
		parent = NULL;
		refElement = NULL;
		return ScErrno::SUCCESS;
	}

	/*
	*  Obtains the latest Transport Key ID, which is assumed to be the 
	*  last Transport Key ID in the StringList obtained.
	*  Args: secCtx - The security context (input).
	*        trkOut - A reference to a std::string which is to hold the 
	*                 transport key in the calling function
	*                 (input/output).
	*  Rets: Standard error code.
	*/
	ScErrno getTrKId(SecCtx secCtx, std::string & trkOut)
	{
		StringList list;
		String TKID;
		ScErrno ret_val = ScErrno::SUCCESS;
		ret_val = ListTkIds(secCtx, &list);

		if(ret_val == ScErrno::SUCCESS)
		{
			TKID = list.pointer[list.length - 1];
			trkOut = std::string(TKID.pointer,TKID.pointer + TKID.length);
		}	
		return ret_val; 
	}

	/*
	*  Creates a hash of the main body of the XML document, and creates XML to
	*  encapsulate said data within the signature.
	*  Args: doc			   - The document which is to be signed (input).
	*        referenceElement  - The node which to which DigestValue node is to 
	*                            be appended (input).
	*        canonicalizedData - The canonicalized data, which will be hashed, 
	*                            that has been produced by a function earlier in 
	*                            the signing process.
	*  Rets: Standard error code.
	*/ 
	ScErrno addHashToXML(xercesc::DOMDocument * doc,
		xercesc::DOMElement * referenceElement,
		Buffer * canonicalizedData)
	{
		ScErrno ret_val = ScErrno::SUCCESS;
		std::string algorithm = DIGEST_ALGORITHM;
		XMLCh * nodeName;
		XMLCh * nodeValue;
		xercesc::DOMElement * tempElement = NULL;		
		std::string hashStr;
		Buffer hash;
		ret_val = getFragment(&algorithm);

		if(ret_val == ScErrno::SUCCESS)
		{
			/*
			*  Hashes the canonicalized data.
			*/
			ret_val = HashData(algorithm.c_str(), *canonicalizedData,
				               TRUNC_LEN, &hash);

			if(ret_val == ScErrno::SUCCESS)
			{
				/*
				*  Converts buffer to string to allow for temporary storage and 
				*  ability to transcode to UTF-16.
				*/
				ret_val = bufferToString(&hash, &hashStr);

				if(ret_val == ScErrno::SUCCESS)
				{
					nodeName  = xercesc::XMLString::transcode("DigestValue");
					nodeValue = xercesc::XMLString::transcode(hashStr.c_str());

					/*
					*  Create DigestValue node as child of Reference node and 
					*  add relevant data to it.
					*/
					ret_val = createNode(tempElement, doc, referenceElement,
						                 nodeName, nodeValue,XMLNAMESPACE::DS);
					xercesc::XMLString::release(&nodeValue);
					xercesc::XMLString::release(&nodeName);
				}
			}
		}

		return ret_val;
	}

	/*
	*  Adds a HMAC signature of the SignedInfo node, and its accompanying XML to 
	*  the Signature node.
	*  Args: secCtx			  - The security context (input).
	*	     doc			  - The document which is being signed (input).
	*		 signatureElement - The encapsulating signature node, to which the 
	*                           SignatureValue node will be appended (input).
	*		 sigInfoElement	  - The element which is to be canonicalized and 
	*                           HMAC'd to produce the signature (input).
	*		 trkId			  - The std::string containing the Transport Key ID,
	*                           which is obtained earlier in the signing 
	*                           process (input).
	*  Rets: Standard error code.
	*/
	ScErrno addHMACSigToXML(SecCtx secCtx,
		xercesc::DOMDocument * doc,
		xercesc::DOMElement * signatureElement,
		xercesc::DOMElement * sigInfoElement,
		const std::string & trkId)
	{
		std::string algorithm;
		ScErrno ret_val = ScErrno::SUCCESS;
		String TKID;
		Buffer HMACData;		
		XMLCh * nodeName;
		XMLCh * nodeValue;
		xercesc::DOMElement * tempElement = NULL;
		std::string signatureVal;
		Buffer sigInfo;
		std::vector<unsigned char> sigInfoVec;

		/*
		*  Pulls out the algorithm used for the HMAC, in this case
		*  'hmac-sha256'.
		*/  
		algorithm = HMAC_ALGORITHM;
		ret_val = getFragment(&algorithm);

		if(ret_val == ScErrno::SUCCESS)
		{
			/*
			*  Canonicalizes the SignedInfo tag and its child tags.
			*/
			ret_val = canonicalizeData(doc,sigInfoElement, &sigInfo,sigInfoVec);

			if(ret_val == ScErrno::SUCCESS)
			{
				/*
				*  Converts the std::string containing the previously acquired TrKId
				*  to a C style string to be used by the HMAC.
				*/  
				stdstringToString(trkId, &TKID);

				/*
				*  Performs a HMAC on the SignedInfo data, which has just been 
				*  canonicalized.
				*/
				ret_val = HmacTk(secCtx, algorithm.c_str(), TKID,
					sigInfo,HMAC_OUT_LEN/8, &HMACData);

				if(ret_val == ScErrno::SUCCESS)
				{
					signatureVal = std::string(reinterpret_cast<const char *>
						(HMACData.pointer),
						HMACData.length);
					nodeName = xercesc::XMLString::transcode("SignatureValue");
					nodeValue = xercesc::XMLString::transcode(signatureVal.c_str());

					/*
					*  Create SignatureValue node as child of Signature node and 
					*  add relevant data to it.
					*/
					ret_val = createNode(tempElement,doc,signatureElement,
						nodeName,nodeValue,XMLNAMESPACE::DS);
					xercesc::XMLString::release(&nodeValue);
					xercesc::XMLString::release(&nodeName);	
				}
			}
		}
		return ret_val;
	}
	/*
	*	Adds the necessary XML and data for the Transport Key to be 
	*  appended to the end of the signature.
	*  Args: doc              - The document which is being signed (input).
	*        signatureElement - The encapsulating signature node, to which the 
	*                           KeyInfo, and its child KeyName, are to be 
	*                           appended (input).
	*        trkId            - The transport key ID used to sign the document, 
	*                           which is obtained earlier in the signature 
	*                           process (input).
	*  Rets: Standard error code.
	*/
	ScErrno addTrKIdToXML(xercesc::DOMDocument * doc,
		xercesc::DOMElement * signatureElement,
		std::string & trkId)
	{
		ScErrno ret_val = ScErrno::SUCCESS;
		XMLCh * nodeName;
		XMLCh * nodeValue;
		xercesc::DOMElement * tempElement = NULL;
		xercesc::DOMElement * keyInfoEle  = NULL;
		xercesc::DOMNode * tempNode = NULL;		
		nodeName = xercesc::XMLString::transcode("KeyInfo");

		/*
		*  Create KeyInfo node.
		*/
		ret_val = createNode(tempElement,doc,signatureElement,
			nodeName,NULL,XMLNAMESPACE::DS);
		xercesc::XMLString::release(&nodeName);

		if(ret_val == ScErrno::SUCCESS)
		{
			nodeName = xercesc::XMLString::transcode("KeyInfo");

			/*
			*  Obtains the node just created.
			*/
			tempNode = getXMLTag(signatureElement,nodeName);
			xercesc::XMLString::release(&nodeName);

			if(tempNode != NULL)
			{
				keyInfoEle = reinterpret_cast<xercesc::DOMElement *>(tempNode);

				if(keyInfoEle != NULL)
				{
					nodeName = xercesc::XMLString::transcode("KeyName");
					nodeValue = xercesc::XMLString::transcode(trkId.c_str());

					/*
					*  Create KeyName node as child of KeyInfo node and 
					*  add relevant data to it.
					*/
					ret_val = createNode(tempElement,doc,keyInfoEle,
						nodeName,nodeValue,XMLNAMESPACE::DS);

					xercesc::XMLString::release(&nodeValue);
					xercesc::XMLString::release(&nodeName);
				}
				else
				{
					ret_val = ScErrno::GENERAL_FAILURE;
				}
			}	
			else
			{
				ret_val = ScErrno::RESOURCE_NOT_EXIST;
			}
		}
		return ret_val;
	}
	/*
	*  Parent function to the canonicalizeData function.
	*  Strips out the referenced node from the document and then passes it to
	*  the canonicalizeData function.
	*  Args: doc       - The document that contains the data to be 
	*                    canonicalized (input).
	*        root      - The topmost node of the DOMDocument being passed (input).
	*        canonData - Buffer which is to hold the canonicalized data (input).
	*        vec       - Vector which accompanies the Buffer and acts as main
	*                    storage (input).
	*  Rets: Standard error code.
	*/
	ScErrno getCanonData(xercesc::DOMDocument * doc,xercesc::DOMElement * root,
		Buffer * canonData, std::vector<unsigned char> & vec)
	{
		std::string tempString;
		XMLCh * nodeName;
		xercesc::DOMNode * canonNode;
		ScErrno ret_val = ScErrno::SUCCESS;

		/*
		*  Obtain the Id of the node which is to be canonicalized.
		*  getFragment is used as the name is set globally to '#xmldoc' 
		*  but is stored in the Id field as 'xmldoc', therefore it 
		*  is necessary to remove the preceding hash symbol.
		*/
		tempString = REFERENCE_URI;
		ret_val = getFragment(&tempString);

		if(ret_val == ScErrno::SUCCESS)
		{
			nodeName = xercesc::XMLString::transcode(tempString.c_str());

			/*
			*  Search through the children of the root node to find a node
			*  with an attribute 'Id' and a value of REFERENCE_URI.
			*/
			canonNode = getNodeWithAttributeId(root,nodeName);
			xercesc::XMLString::release(&nodeName);
			if(canonNode != NULL)
			{
				/*
				*  Canonicalize the node, if the node address is not NULL.
				*/
				ret_val = canonicalizeData(doc,canonNode,canonData,vec);
			}
			else
			{
				ret_val = ScErrno::RESOURCE_NOT_EXIST;
			}
		}

		return ret_val;
	}

	/*
	*  Function to call the appropriate functions to create a full XML signature
	*  and append it to the document passed in.
	*  Args: secCtx - Security Context (input).
	*        doc    - XML Document to be signed (input).
	*  Rets: Standard error code.   
	*/
	ScErrno signDocument(SecCtx secCtx, xercesc::DOMDocument * doc)
	{
		xercesc::DOMNode * tempNode;
		xercesc::DOMElement * sigElement;
		xercesc::DOMElement * sigInfoEle;
		xercesc::DOMElement * refElement;
		xercesc::DOMElement * root;
		ScErrno ret_val = ScErrno::SUCCESS;
		XMLCh * nodeName;
		std::string trkId;
		Buffer canonData;
		std::vector<unsigned char> canonVector;

		if(doc->hasChildNodes())
		{
			root = doc->getDocumentElement();

			/*
			*  Create signature node as a child of SignedKmsResponse.
			*/
			ret_val = createSignatureTag(root,doc);
			if(ret_val == ScErrno::SUCCESS)
			{
				nodeName = xercesc::XMLString::transcode("Signature");
				tempNode = getXMLTag(root,nodeName);
				xercesc::XMLString::release(&nodeName);

				if(tempNode != NULL)
				{
					sigElement = reinterpret_cast<xercesc::DOMElement *>(tempNode);

					if(sigElement != NULL)
					{
						nodeName = xercesc::XMLString::transcode("SignedInfo");
						tempNode = getXMLTag(tempNode,nodeName);
						xercesc::XMLString::release(&nodeName);

						if(tempNode != NULL)
						{

							sigInfoEle = reinterpret_cast<xercesc::DOMElement *>(tempNode);

							if(sigInfoEle != NULL)
							{
								/*
								*  Get Reference tag from Signature tag just created.	 
								*/
								nodeName = xercesc::XMLString::transcode("Reference");
								tempNode = getXMLTag(sigInfoEle, nodeName);
								xercesc::XMLString::release(&nodeName);

								if(tempNode != NULL)
								{
									refElement = reinterpret_cast<xercesc::DOMElement *>(tempNode);
									if(refElement != NULL)
									{
										ret_val = getCanonData(doc, root, &canonData, canonVector);

										if(ret_val == ScErrno::SUCCESS)
										{
											ret_val = getTrKId(secCtx,trkId);

											if(ret_val == ScErrno::SUCCESS)
											{
												ret_val = addHashToXML(doc,refElement, &canonData);
												if(ret_val == ScErrno::SUCCESS)
												{
													ret_val = addHMACSigToXML(secCtx, doc, sigElement,
														                      sigInfoEle, trkId);

													if(ret_val == ScErrno::SUCCESS)
													{
														ret_val = addTrKIdToXML(doc, sigElement, trkId);
													}
												}
											}
										}
									}	
									else
									{
										ret_val = ScErrno::GENERAL_FAILURE;
									}
								}
								else
								{
									ret_val = ScErrno::RESOURCE_NOT_EXIST;
								}
							}
							else
							{
								ret_val = ScErrno::GENERAL_FAILURE;
							}
						}
						else
						{
							ret_val = ScErrno::GENERAL_FAILURE;
						}
					}
				}
				else
				{
					ret_val = ScErrno::RESOURCE_NOT_EXIST;
				}
			}
			else
			{
				ret_val = ScErrno::RESOURCE_NOT_EXIST;
			}
		}	
		else
		{
			ret_val = ScErrno::RESOURCE_NOT_EXIST;
		}

		return ret_val;
	}

	/*
	*  Populates the DOMDocument with the necessary data to create a basic
	*  request such as UserUri, KmsUri, Time, ClientId, ClientReqUrl etc
	*  Args: doc           - The document to be populated (input).
	*        userUri       - The UserUri to be added to which the document 
	*                        relates (input).
	*        kmsUri        - The URI of the KMS receiving this XML document
	*                        (input).
	*        clientId      - The ID of the Client (input).
	*        deviceId      - The ID of the device sending request. 
	*                        Ignored if NULL (input).
	*        requestUrl    - The URL to which the request will be sent (input).
	*        clientMessage - Additional messages from the client?             
	*                        Ignored if null (input).
	*        clientError   - Error messages from client?
	*                        Ignored if null (input).
	*        addNamespace  - Additional tags to be appended to the request.
	*                        Ignored if null (input).
	*/	
	ScErrno populateDocument(xercesc::DOMDocument * doc, XMLCh * userUri,
		                     XMLCh * kmsUri, XMLCh * clientId, XMLCh * deviceId, 
		                     XMLCh * requestUrl, XMLCh * clientMessage,
		                     XMLCh * clientError, xercesc::DOMNode * addNamespace)
	{
		if(userUri == NULL || kmsUri == NULL || requestUrl == NULL)
		{
			return ScErrno::RESOURCE_NOT_EXIST;
		}

		// Holds current timestamp value.
		XMLCh * timestamp = NULL;
		XMLCh * requestElement = NULL;
		XMLCh * requestElement2 = NULL;

		ScErrno valueReturned = ScErrno::SUCCESS;
		/*
		*  Xercesc elements used for identifying points within the DOM tree
		*  First holds root element (<SignedKmsRequest> tag)
		*  Second holds first child of root (<KmsRequestTag>)
		*  Third is temporary node value used by createNode function.
		*/
		xercesc::DOMElement * c_Root = NULL;
		xercesc::DOMElement * c_KmsReq = NULL;
		xercesc::DOMElement * c_temp = NULL;
		std::string tempString = "";
		XMLCh * namespaceLocation = NULL;
		XMLCh * qualifiedName = NULL;
		XMLCh * namespaceUri = NULL;
		c_Root = doc->getDocumentElement();

		if(c_Root->hasAttributes() == NULL) 
		{
			/*
			*  Assignment of namespace charachteristics to SignedKmsRequest node.
			*/
			valueReturned = addNamespaceToElement(c_Root, "xmlns:" + DIGITAL_SIG_PREFIX,
				                                  DS_NAMESPACE, NAMESPACE_URI);
			if(valueReturned != ScErrno::SUCCESS)
			{
				return valueReturned;
			}

			valueReturned
			  = addNamespaceToElement(c_Root, "xmlns:" + SCHEMA_INSTANCE_PREFIX,
				                      XSI_NAMESPACE, NAMESPACE_URI);

			if(valueReturned != ScErrno::SUCCESS)
			{
				return valueReturned;
			}

			valueReturned
			  = addNamespaceToElement(c_Root,
			                          SCHEMA_INSTANCE_PREFIX + ":schemaLocation",
				                      XSI_LOCATION, NAMESPACE_URI);

			if(valueReturned != ScErrno::SUCCESS)
			{
				return valueReturned;
			}
		}

		if(!c_Root->hasChildNodes())
		{
			/*
			*  Create KMSRequest node and add ID and version attributes to it.
			*/
			requestElement = xercesc::XMLString::transcode("KmsRequest");
			c_KmsReq = doc->createElementNS(xercesc::XMLString::transcode(
				MAIN_NAMESPACE.c_str()),
				requestElement);
			xercesc::XMLString::release(&requestElement);
			addNamespaceToElement(c_KmsReq, "xmlns",MAIN_NAMESPACE,NAMESPACE_URI);
			requestElement = xercesc::XMLString::transcode("Id");

			/*
			*	Assigns the value to be 'xmldoc', needed assignment to temporary
			*  string to allow for trimming of the preceding hash (#) symbol.
			*/ 
			tempString = REFERENCE_URI;
			getFragment(&tempString);
			requestElement2 = xercesc::XMLString::transcode(tempString.c_str());
			c_KmsReq->setAttribute(requestElement,requestElement2);
			xercesc::XMLString::release(&requestElement);
			xercesc::XMLString::release(&requestElement2);
			requestElement = xercesc::XMLString::transcode("Version");
			requestElement2 = xercesc::XMLString::transcode(VERSION_VALUE.c_str());
			c_KmsReq->setAttribute(requestElement,requestElement2);
			xercesc::XMLString::release(&requestElement);
			xercesc::XMLString::release(&requestElement2);
			c_Root->appendChild(c_KmsReq);
		}

		/*
		*  Adds a UserUri tag and a child text node which has userUri data within.
		*/
		requestElement = xercesc::XMLString::transcode("UserUri");
		valueReturned = createNode(c_temp, doc, c_KmsReq, requestElement, userUri,
			                       XMLNAMESPACE::MAIN);
		xercesc::XMLString::release(&requestElement);

		if(valueReturned != ScErrno::SUCCESS)
		{
			return valueReturned;
		}

		/*
		*  Adds a KmsUri tag and a child text node which has kmsUri data within.
		*/
		requestElement = xercesc::XMLString::transcode("KmsUri");
		valueReturned = createNode(c_temp,doc,c_KmsReq,requestElement,kmsUri,XMLNAMESPACE::MAIN);
		xercesc::XMLString::release(&requestElement);

		if(valueReturned != ScErrno::SUCCESS)
		{
			return valueReturned;
		}

		/*
		*  Attempts to obtain a timestamp from the appropriate functions.
		*/
		try
		{
			std::string tempStr = "";
			valueReturned = getTimeStamp(&tempStr);

			if(valueReturned != ScErrno::SUCCESS) 
			{
				return ScErrno::GENERAL_FAILURE;
			}
			else
			{
				timestamp = xercesc::XMLString::transcode(tempStr.c_str());
			}
		}
		catch(...) // Catch any error, although will only be an XMLString exception.
		{
			if(timestamp != NULL)
			{
				xercesc::XMLString::release(&timestamp);
			}

			if(requestElement != NULL)
			{
				xercesc::XMLString::release(&requestElement);
			}

			return ScErrno::GENERAL_FAILURE; 
		}

		/*
		*  Adds a Time tag and a child text node which has timestamp data within.
		*/
		requestElement = xercesc::XMLString::transcode("Time");
		valueReturned = createNode(c_temp, doc, c_KmsReq, requestElement, timestamp,
			                       XMLNAMESPACE::MAIN);
		xercesc::XMLString::release(&timestamp);
		xercesc::XMLString::release(&requestElement);

		if(valueReturned != ScErrno::SUCCESS)
		{
			return valueReturned;
		}

		if(clientId != NULL)
		{
			/*
			*  Adds a ClientId tag and a child text node which has clientId data 
			*  within.
			*/
			requestElement = xercesc::XMLString::transcode("ClientId");
			valueReturned = createNode(c_temp,doc,c_KmsReq,requestElement,clientId,XMLNAMESPACE::MAIN);
			xercesc::XMLString::release(&requestElement);

			if(valueReturned != ScErrno::SUCCESS)
			{
				return valueReturned;
			}		
		}
		if(deviceId != NULL)
		{
			/*
			*  Adds a UserUri tag and a child text node which has userUri data within.
			*/
			requestElement = xercesc::XMLString::transcode("DeviceId");
			valueReturned = createNode(c_temp,doc,c_KmsReq,requestElement,deviceId,XMLNAMESPACE::MAIN);
			xercesc::XMLString::release(&requestElement);

			if(valueReturned != ScErrno::SUCCESS)
			{
				return valueReturned;
			}		
		}	
		/*
		*  Adds a ClientReqUrl tag and a child text node which has requestUrl 
		*  data within.
		*/	
		requestElement = xercesc::XMLString::transcode("ClientReqUrl");
		valueReturned = createNode(c_temp,doc,c_KmsReq,requestElement,requestUrl,XMLNAMESPACE::MAIN);
		xercesc::XMLString::release(&requestElement);

		if(valueReturned != ScErrno::SUCCESS)
		{
			return valueReturned;
		}

		if(clientMessage != NULL)
		{
			/*
			*  Adds a ClientMessage tag and a child text node which has clientMessage
			*  data within.
			*/
			requestElement = xercesc::XMLString::transcode("ClientMessage");
			valueReturned = createNode(c_temp,doc,c_KmsReq,requestElement,clientMessage,XMLNAMESPACE::MAIN);
			xercesc::XMLString::release(&requestElement);

			if(valueReturned != ScErrno::SUCCESS)
			{
				return valueReturned;
			}		
		}

		if(clientError != NULL)
		{
			requestElement = xercesc::XMLString::transcode("ClientError");
			valueReturned = createNode(c_temp,doc,c_KmsReq,requestElement,clientError,XMLNAMESPACE::MAIN);
			xercesc::XMLString::release(&requestElement);

			if(valueReturned != ScErrno::SUCCESS)
			{
				return valueReturned;
			}		
		}

		if(addNamespace != NULL)
		{
			try
			{
				c_KmsReq->appendChild(addNamespace);
			}
			catch(...)
			{
				return ScErrno::GENERAL_FAILURE;
			}
		}

		return ScErrno::SUCCESS;
	}

	/*
	*  Checks to see if the DOMDocument in question is signed, by searching 
	*  to see if the root node has any child nodes with name 'Signature'.
	*  Args: rootNode - The top most node of the DOMDOcument to be traversed
	*                   (input).
	*  Rets: A boolean value representing true if a Signature tag is found, 
	*        elsewise a false value is returned.
	*/
	bool isSigned(xercesc::DOMNode * rootNode)
	{
		XMLCh * sig = xercesc::XMLString::transcode("Signature");
		if(getXMLTag(rootNode, sig) == NULL)

		{
			xercesc::XMLString::release(&sig);
			return false;
		}
		else
		{
			xercesc::XMLString::release(&sig);
			return true;
		}
	}

	/*
	*  Serializes the entire DOMDocument and then pushes the serialized data into
	*	a std::vector.
	*  Args: doc - The DOMDocument to be serialized (input).
	*        vec - The std::vector which will hold the serialized data
	*              (input/output).
	*  Rets: Standard error code.
	*/
	ScErrno xmlDocToVector(xercesc::DOMDocument * doc,
		                   std::vector<unsigned char> &vec)
	{
		/*
		*  Creates an instance of LS type serializer.
		*/
		XMLCh * tempStr = xercesc::XMLString::transcode("LS");
		xercesc::DOMImplementation * impl
		  = xercesc::DOMImplementationRegistry::getDOMImplementation(tempStr);
		xercesc::XMLString::release(&tempStr);
		xercesc::DOMLSSerializer * writer = impl->createLSSerializer();

		/*
		*  Creates an LS type output for us to direct output stream to.
		*/
		xercesc::DOMLSOutput * out = impl->createLSOutput();

		/*
		*  Creates an output 'target' where data is stored by output stream. 
		*/
		xercesc::MemBufFormatTarget target;
		out->setByteStream(&target);

		/*
		*  Sets the encoding of the Document appropriately.
		*/
		out->setEncoding(xercesc::XMLString::transcode("UTF-8"));

		try
		{	
			/*
			*  Serializes the document to the specified output (and its target)
			*/
			writer->write(doc, out);

			/*
			*  Converts the outputted data to an array of XMLBytes for traversal. 
			*/
			const XMLByte * const rawBuff = target.getRawBuffer();
			vec.assign(rawBuff, rawBuff + target.getLen());
		}
		catch(...)
		{
			return ScErrno::GENERAL_FAILURE;
		}

		return ScErrno::SUCCESS;
	}

	/*
	*  Function to process and pull out all relevant data from a KeyProvision
	*  response.
	*  Args: secCtx      - Security Context (input).
	*        root        - The top most node within the DOMDocument (input).
	*        keyProvNode - The DOMNode which has name 'KmsKeyProv' (input).
	*        isSigned    - Boolean value detailing if the document has a digital
	*                      signature (input).
	*  Rets: Standard error code.
	*/
	ScErrno KmsKeyProvProcess(SecCtx secCtx,xercesc::DOMDocument * doc, 
		xercesc::DOMElement * root,
		xercesc::DOMNode * keyProvNode,bool isSigned)
	{ 
		xercesc::DOMNodeList * keyProvChildren = NULL;
		xercesc::DOMNode * packOrTrK = NULL;
		xercesc::DOMNode * userUriNode = NULL;
		xercesc::DOMNamedNodeMap * attributes = NULL;
		xercesc::DOMNode * kmsresponse = NULL;
		XMLCh * nodeName = NULL;
		XMLCh * trkName = NULL;
		XMLCh * userUriCh = NULL;
		ScErrno returnedErrorCode = ScErrno::SUCCESS;
		String userUri;
		std::string tempstring = "";
		std::vector<char> userUriVec;

		/*
		*  Verification of digital signature if present.
		*/
		if(isSigned)
		{
			returnedErrorCode = verifySigParent(secCtx, doc, root);

			if(returnedErrorCode != ScErrno::SUCCESS)
			{
				return returnedErrorCode;
			}

			nodeName = xercesc::XMLString::transcode("KmsResponse");
			kmsresponse = getXMLTag(root,nodeName);
			xercesc::XMLString::release(&nodeName);
		}
		else
		{
			kmsresponse = root;
		}

		/*
		*  Pull out UserUri and store it in a String type for use 
		*  with Transport Key storage.
		*/
		userUriCh = xercesc::XMLString::transcode("UserUri");
		userUriNode = getTextTag(kmsresponse,userUriCh);
		xercesc::XMLString::release(&userUriCh);

		if(userUriNode == NULL)
		{
			return ScErrno::RESOURCE_NOT_EXIST;
		}

		tempstring = xercesc::XMLString::transcode(userUriNode->getNodeValue());

		if(tempstring == "")
		{
			return ScErrno::RESOURCE_NOT_EXIST;
		}

		stringToVec(userUriVec,tempstring);
		returnedErrorCode = vecToString(&userUri,userUriVec);

		if(returnedErrorCode != ScErrno::SUCCESS)
		{
			return returnedErrorCode;
		}

		/*
		*  Ensures that the KeyProvision Node has child nodes.
		*/
		if(keyProvNode->hasChildNodes()) 
		{
			trkName = xercesc::XMLString::transcode("NewTransportKey");
			nodeName = xercesc::XMLString::transcode("KmsKeySet");
			keyProvChildren = keyProvNode->getChildNodes();

			/*
			*  Iterates over the child nodes of keyProvNode and searches for
			*  NewTransportKey and KmsKeySet tags. 
			*/
			for(XMLSize_t i =0; i < keyProvChildren->getLength(); ++i)
			{

				if(xercesc::XMLString::equals(nodeName, 
					keyProvChildren->item(i)->getNodeName()))
				{
					packOrTrK = keyProvChildren->item(i);

					/*
					*  Pulls out relevant data and stores using ScCrypto functions.
					*/
					returnedErrorCode = getKeyPack(secCtx,packOrTrK);

					if(returnedErrorCode != ScErrno::SUCCESS)
					{
						xercesc::XMLString::release(&nodeName);
						xercesc::XMLString::release(&trkName);
						return returnedErrorCode;
					}
				}
				else if(xercesc::XMLString::equals(trkName,
					keyProvChildren->item(i)->getNodeName()))
				{
					/*
					*  UserUri not set therefore trans key cannot be stored.
					*/
					if(userUri.length == 0) //
					{
						return ScErrno::RESOURCE_NOT_EXIST;
					}

					packOrTrK = keyProvChildren->item(i);

					/*
					*  Pulls out relevant data and stores using ScCrypto functions.
					*/
					returnedErrorCode = getTransportKey(secCtx, packOrTrK,userUri);

					if(returnedErrorCode != ScErrno::SUCCESS)
					{
						xercesc::XMLString::release(&nodeName);
						xercesc::XMLString::release(&trkName);
						return returnedErrorCode;
					}
				}
			}

			xercesc::XMLString::release(&nodeName);
			xercesc::XMLString::release(&trkName);
			keyProvChildren = NULL;
		}
		else
		{
			return ScErrno::RESOURCE_NOT_EXIST;
		}

		return ScErrno::SUCCESS;
	}
	/*
	*  Function to process and pull out all relevant data from a CertCache
	*  response.
	*  Args: secCtx        - Security Context (input).
	*        root          - The top most node within the DOMDocument (input).
	*        certCacheNode - The DOMNode which has name 'KmsCertCache' (input).
	*        isSigned      - Boolean value detailing of the document has a 
	*                        digital signature (input).
	*  Rets: Standard error code.
	*/
	ScErrno KmsCertCacheProcess(SecCtx secCtx, xercesc::DOMDocument * doc, 
		xercesc::DOMElement * root,
		xercesc::DOMNode * certCacheNode,bool isSigned)
	{
		XMLCh * signedKC = NULL;
		XMLCh * tempChar = NULL;
		xercesc::DOMNode * signedCertObj = NULL;
		xercesc::DOMNode * certSig = NULL;
		xercesc::DOMNode * certObj = NULL;
		xercesc::DOMNode * attribute = NULL;
		xercesc::DOMNodeList * certCacheChildren = NULL;
		xercesc::DOMNodeList * signedChildren = NULL;
		Buffer certificateBuffer;
		ScErrno returnedErrorCode = ScErrno::SUCCESS;
		std::vector<unsigned char> certificateVector;
		bool isRoot;

		/*
		*  Verification of digital signature if present.
		*/
		if(isSigned) 
		{
			returnedErrorCode = verifySigParent(secCtx, doc, root);

			if(returnedErrorCode != ScErrno::SUCCESS)
			{
				return returnedErrorCode;
			}
		}

		if(certCacheNode->hasChildNodes())
		{
			/*
			*  Attempts to strip out CacheNum for storage.
			*/ 
			tempChar = xercesc::XMLString::transcode("CacheNum");
			attribute = getAttribute(certCacheNode,tempChar);
			xercesc::XMLString::release(&tempChar);

			if(attribute != NULL)
			{
				//CacheNum provided therefore store
			}

			certCacheChildren = certCacheNode->getChildNodes();

			/*
			*  Each certificate is signed with its own digital signature, so
			*  it is necessary to search for the signature then verify said 
			*  signature against the creating node, after which you can push
			*  the certificate into a Buffer if it is successful.
			*/
			for(XMLSize_t i = 0; i < certCacheChildren->getLength();++i)
			{
				signedKC = xercesc::XMLString::transcode("SignedKmsCertificate");

				if(xercesc::XMLString::equals(signedKC,certCacheChildren->item(i)->getNodeName()))
				{
					xercesc::XMLString::release(&signedKC);
					signedCertObj = certCacheChildren->item(i);

					XMLCh * signature = xercesc::XMLString::transcode("Signature");
					certSig = getXMLTag(signedCertObj, signature);
					xercesc::XMLString::release(&signature);

					if(certSig == NULL)
					{
						return ScErrno::RESOURCE_NOT_EXIST;
					}

					/*
					*  Pull out reference URI and search for a node with a corresponding
					*  ID as an attribute.
					*/
					std::string tempStr = getReferenceURI(certSig);
					certObj = getNodeWithAttributeId(signedCertObj, 
						xercesc::XMLString::transcode(tempStr.c_str()));

					if(certObj == NULL)
					{
						return ScErrno::RESOURCE_NOT_EXIST;
					}

					/*
					*  Verify Signature against signed node.
					*/
					returnedErrorCode = verifySignature(secCtx, doc, signedCertObj, certSig);

					if(returnedErrorCode != ScErrno::SUCCESS)
					{
						xercesc::XMLString::release(&signedKC);
						return returnedErrorCode;
					}

					/*
					*  Push certificate into a Buffer.
					*/ 
					returnedErrorCode = certificateToBuffer(certObj, &certificateBuffer,
						                                    certificateVector, &isRoot);

					if(returnedErrorCode != ScErrno::SUCCESS)
					{
						xercesc::XMLString::release(&signedKC);
						return returnedErrorCode;
					}

					/*
					*  Store using appropriate ScCrypto function dependent on
					*  the type of certificate it is.
					*/ 
					if(isRoot)
					{
						returnedErrorCode = StoreRootCert(secCtx, certificateBuffer);
					}
					else
					{
						returnedErrorCode = StoreExternalCert(secCtx, certificateBuffer);
					}

					certificateBuffer.length = NULL;
					certificateBuffer.pointer = NULL;
					certificateVector.clear();
					certObj = NULL;
					certSig = NULL;

					if(returnedErrorCode != ScErrno::SUCCESS)
					{
						return returnedErrorCode;
					}
				}
				else
				{
					xercesc::XMLString::release(&signedKC);
				}
			}

			return ScErrno::SUCCESS;
		}

		return ScErrno::RESOURCE_NOT_EXIST;
	}

	/*
	*  Function to process and pull out all relevant data from a Init
	*  response.
	*  Args: secCtx        - Security Context (input).
	*        root          - The top most node within the DOMDocument (input).
	*        certCacheNode - The DOMNode which has name 'KmsInit' (input).
	*        isSigned      - Boolean value detailing of the document has a 
	*                        digital signature (input).
	*  Rets: Standard error code.
	*/	
	ScErrno KmsInitProcess(SecCtx secCtx, xercesc::DOMDocument * doc,  
		xercesc::DOMElement * root,
		xercesc::DOMNode * initNode,  bool isSigned)
	{
		// Obtain certificate and store in sccryptolib.
		// Convert xml to buffer.
		Buffer KmsCert;
		xercesc::DOMNode * kmsResponse = NULL;
		xercesc::DOMNode * certNode = NULL;
		xercesc::DOMNode * newTrKNode = NULL;
		xercesc::DOMNode * userUriNode = NULL;
		XMLCh * newtrk = NULL;
		XMLCh * kmsResp = NULL;
		XMLCh * uUri = NULL;
		xercesc::DOMNode * cert2 = NULL;
		String userUri;
		ScErrno ret = ScErrno::SUCCESS;
		std::string tempString = "";
		std::vector<unsigned char> kmsCertVec;
		std::vector<char> userUriVec;
		bool isRoot;

		/*
		*  Verification of digital signature if present.
		*/
		if(isSigned)
		{
			ret = verifySigParent(secCtx, doc, root);

			if(ret != ScErrno::SUCCESS)
			{
				return ret;
			}

			kmsResp = xercesc::XMLString::transcode("KmsResponse");
			kmsResponse = getXMLTag(root,kmsResp);
			xercesc::XMLString::release(&kmsResp);

			if(kmsResponse == NULL)
			{
				return ScErrno::RESOURCE_NOT_EXIST;
			}
		}

		/* 
		*	Pull out and store UserUri as a String, for use if NewTransportKey
		*  tag is present.
		*/
		uUri = xercesc::XMLString::transcode("UserUri");

		if(isSigned)
		{
			userUriNode = getTextTag(kmsResponse, uUri);
		}
		else
		{
			userUriNode = getTextTag(root, uUri);
		}	
		if(userUriNode == NULL)
		{
			return ScErrno::RESOURCE_NOT_EXIST;
		}

		tempString = xercesc::XMLString::transcode(userUriNode->getNodeValue());

		if(tempString != "")
		{
			stringToVec(userUriVec,tempString);
			vecToString(&userUri, userUriVec);
			tempString = "";
		}
		else
		{
			return ScErrno::RESOURCE_NOT_EXIST;
		}

		xercesc::XMLString::release(&uUri);

		/*
		*  Obtain and store root certificate.
		*/

		XMLCh * kmscer = xercesc::XMLString::transcode("KmsCertificate");

		certNode = getXMLTag(initNode,kmscer);
		xercesc::XMLString::release(&kmscer);

		if(certNode == NULL)
		{
			xercesc::XMLString::release(&kmscer);
			return ScErrno::RESOURCE_NOT_EXIST;
		}

		ret = certificateToBuffer(certNode, &KmsCert,kmsCertVec, &isRoot);

		if(ret != ScErrno::SUCCESS)
		{
			return ret;
		}

		if(isRoot)
		{
			ret = StoreRootCert(secCtx, KmsCert);
		}
		else
		{
			ret = StoreExternalCert(secCtx, KmsCert);
		}

		KmsCert.length = NULL;
		KmsCert.pointer = NULL;

		if(ret != ScErrno::SUCCESS)
		{
			return ret;
		}

		/*
		*  Search for NewTransportKey tag and store if found.
		*/
		newtrk = xercesc::XMLString::transcode("NewTransportKey");
		newTrKNode = getXMLTag(initNode,newtrk);
		xercesc::XMLString::release(&newtrk);

		if(newTrKNode != NULL)
		{
			ret = getTransportKey(secCtx, newTrKNode,userUri);
		}

		return ret;
	}
}

namespace ScLibMsKMS
{
	/*
	* Pass through function, to libmscrypto, to allow libmskms to manage
	* transport keys.
	* 
	* Args: secCtx         - Reference to the Security Context to be created
	*                        (output).
	*		 KMSUri        - The URI to which the Security Context is to be 
	*                        associated (input)
	*       TransportKeyId - The ID of the transport key associated with the
	*                        Security Context (input).
	*       TransportKey   - Buffer containg the transport key (input).
	* Rets: Standard error code. 
	*/	
	ScErrno CreateKmsSecurityContext(SecCtx & secCtx, std::string KMSUri,
		                             const std::string &TransportKeyId,
		                             const std::vector<unsigned char> &
		                             transportKey)
	{
		String kmsUriStr, trkIDStr;
		std::vector<char> kmsUriVec,trkIdVec;
		Buffer trkBuf;

		if (transportKey.empty() || TransportKeyId.empty() || KMSUri.empty())
		{
			return ScErrno::RESOURCE_NOT_EXIST;
		}

		stringToVec(kmsUriVec,KMSUri);
		vecToString(&kmsUriStr,kmsUriVec);
		stringToVec(trkIdVec,TransportKeyId);
		vecToString(&trkIDStr,trkIdVec);
		vecToBuff(&trkBuf,transportKey);
		return CreateKmsSecurityContext(kmsUriStr, trkIDStr, trkBuf, &secCtx);
	}

	/*
	* Returns XML, as a vector, containing an initialisation request.
	* Args: secCtx         - Reference to the Security Context (input).
	*	    KMSUrl         - The URL of the KMS this request is for
	*                        (input).
	*       UserUri        - The URI of the user for the specific KMS
	*                        (input).
	*       KMSInitReq     - Vector containing the XML request (output).
	* Rets: Standard error code. 
	*/
	ScErrno RequestKmsInitXML(SecCtx secCtx, std::string KMSUrl,
		                      const std::string &UserUri,
		                      std::vector<unsigned char> &KmsInitReq)
	{
		if(KMSUrl == "" || UserUri == "" || secCtx == 0)
		{
			return ScErrno::RESOURCE_NOT_EXIST;
		}

		if(!KmsInitReq.empty())
		{
			KmsInitReq.clear();
		}

		ScErrno scErrReturned = ScErrno::SUCCESS;
		xercesc::XMLPlatformUtils::Initialize();
		{
			XSECPlatformUtils::Initialise();
			xercesc::DOMDocument * document = NULL;
			xercesc::DOMImplementation * imp = NULL;
			XMLCh * namespaceCh = NULL;
			XMLCh * topNode = NULL;
			XMLCh * userUriU16 = NULL;
			XMLCh * kmsUriU16 = NULL;
			XMLCh * reqUriU16 = NULL;
			std::string tempStr;
			String kmsUri;

			/*		
			*  Create new DOMDocument and populate it with apropriate data.
			*/
			namespaceCh = xercesc::XMLString::transcode(MAIN_NAMESPACE.c_str());
			topNode = xercesc::XMLString::transcode("SignedKmsRequest");
			imp = xercesc::DOMImplementationRegistry::getDOMImplementation(
				    xercesc::XMLString::transcode("core")); 
			document = imp->createDocument(namespaceCh, topNode, 0);
			xercesc::XMLString::release(&topNode); 
			xercesc::XMLString::release(&namespaceCh);
			userUriU16 = xercesc::XMLString::transcode((char *)UserUri.c_str());
			scErrReturned = GetKmsUri(secCtx, &kmsUri);

			if(scErrReturned == ScErrno::SUCCESS)
			{
				kmsUriU16 = xercesc::XMLString::transcode(
					std::string(const_cast<const char *>(kmsUri.pointer),
					kmsUri.length).c_str());

				reqUriU16 = xercesc::XMLString::transcode(KMSUrl.c_str());
				scErrReturned = populateDocument(document, userUriU16, kmsUriU16,
					                             NULL, NULL, reqUriU16, NULL,
												 NULL, NULL);
				if(scErrReturned == ScErrno::SUCCESS)
				{
					/*
					*  Sign the document.
					*/
					scErrReturned = signDocument(secCtx, document);

					if(scErrReturned == ScErrno::SUCCESS)
					{
						/*
						*  Serialize and push into vector.
						*/
						scErrReturned = xmlDocToVector(document,KmsInitReq);
					} 
				}
			}

			XSECPlatformUtils::Terminate();
		}

		xercesc::XMLPlatformUtils::Terminate();
		return ScErrno::SUCCESS;
	}

	/*
	* Returns XML, as a vector, containing an key provision request.
	* Args: secCtx        - Reference to the Security Context (input).
	*		KMSUrl        - The URL of the KMS this request is for 
	*                       (input).
	*       UserUri       - The URI of the user for the specific KMS
	*                       (input).
	*       KMSKeyProvReq - Vector containing the XML request (output).
	* Rets: Standard error code. 
	*/
	ScErrno RequestKmsKeyProvXML(SecCtx secCtx, std::string KMSUrl,
		                         const std::string & UserUri,
		                         std::vector<unsigned char> &
								   KmsKeyProvReq)
	{
		if(KMSUrl == "" || UserUri == "" || secCtx == 0)
		{
			return ScErrno::RESOURCE_NOT_EXIST;
		}

		if(!KmsKeyProvReq.empty())
		{
			KmsKeyProvReq.clear();
		}

		ScErrno scErrReturned = ScErrno::SUCCESS;
		xercesc::XMLPlatformUtils::Initialize();
		{
			XSECPlatformUtils::Initialise();
			xercesc::DOMDocument * document = NULL;
			xercesc::DOMImplementation * imp = NULL;
			XMLCh * namespaceCh = NULL;
			XMLCh * topNode = NULL;
			XMLCh * userUriU16 = NULL;
			XMLCh * kmsUriU16 = NULL;
			XMLCh * reqUriU16 = NULL;
			char * tempChar;
			std::string tempStr;
			String kmsUri;

			/*		
			*  Create new DOMDocument and populate it with apropriate data.
			*/
			namespaceCh = xercesc::XMLString::transcode(MAIN_NAMESPACE.c_str());
			topNode = xercesc::XMLString::transcode("SignedKmsRequest");
			imp = xercesc::DOMImplementationRegistry::getDOMImplementation(
				xercesc::XMLString::transcode("core")); 
			document = imp->createDocument(namespaceCh,topNode, 0);
			xercesc::XMLString::release(&topNode); 
			xercesc::XMLString::release(&namespaceCh);
			tempChar = (char *)UserUri.c_str();
			userUriU16 = xercesc::XMLString::transcode(tempChar);
			scErrReturned = GetKmsUri(secCtx, &kmsUri);

			if(scErrReturned == ScErrno::SUCCESS)
			{
				kmsUriU16 = xercesc::XMLString::transcode(
					std::string(const_cast<const char *>(kmsUri.pointer),
					kmsUri.length).c_str());

				reqUriU16 = xercesc::XMLString::transcode(KMSUrl.c_str());
				scErrReturned = populateDocument(document, userUriU16, kmsUriU16,
					                             NULL, NULL, reqUriU16, NULL,
					                             NULL, NULL);

				if(scErrReturned == ScErrno::SUCCESS)
				{
					/*
					*  Sign the document.
					*/
					scErrReturned = signDocument(secCtx, document);

					if(scErrReturned == ScErrno::SUCCESS)
					{
						/*
						*  Serialize and push into vector.
						*/
						scErrReturned = xmlDocToVector(document,KmsKeyProvReq);
					}
				}
			}

			XSECPlatformUtils::Terminate();
		}

		xercesc::XMLPlatformUtils::Terminate();
		return scErrReturned;
	}

	/*
	* Returns XML, as a vector, containing an certificate cache request.
	* Args: secCtx          - Reference to the Security Context (input).
	*	    KMSUrl          - The URL of the KMS this request is for
	*                         (input).
	*       UserUri         - The URI of the user for the specific KMS
	*                         (input).
	*       KMSCertCacheReq - Vector containing the XML request (output).
	* Rets: Standard error code. 
	*/
	ScErrno RequestKmsCertCacheXML(SecCtx secCtx, std::string KMSUrl,
		                           const std::string & UserUri,
								   std::vector<unsigned char> &
								     KmsCertCacheReq)
	{
		if(KMSUrl == "" || UserUri == "" || secCtx == 0)
		{
			return ScErrno::RESOURCE_NOT_EXIST;
		}

		if(!KmsCertCacheReq.empty())
		{
			KmsCertCacheReq.clear();
		}

		ScErrno scErrReturned = ScErrno::SUCCESS;
		xercesc::XMLPlatformUtils::Initialize();
		{
			XSECPlatformUtils::Initialise();
			xercesc::DOMDocument * document = NULL;
			xercesc::DOMImplementation * imp = NULL;
			XMLCh * namespaceCh = NULL;
			XMLCh * topNode = NULL;
			XMLCh * userUriU16 = NULL;
			XMLCh * kmsUriU16 = NULL;
			XMLCh * reqUriU16 = NULL;
			char * tempChar;
			std::string tempStr;
			String kmsUri;

			/*		
			*  Create new DOMDocument and populate it with apropriate data.
			*/
			namespaceCh = xercesc::XMLString::transcode(MAIN_NAMESPACE.c_str());
			topNode = xercesc::XMLString::transcode("SignedKmsRequest");
			imp = xercesc::DOMImplementationRegistry::getDOMImplementation(
				    xercesc::XMLString::transcode("core")); 
			document = imp->createDocument(namespaceCh,topNode, 0);
			xercesc::XMLString::release(&topNode); 
			xercesc::XMLString::release(&namespaceCh);
			tempChar = (char *)UserUri.c_str();
			userUriU16 = xercesc::XMLString::transcode(tempChar);
			scErrReturned = GetKmsUri(secCtx, &kmsUri);

			if(ScErrno::SUCCESS == scErrReturned)
			{
				kmsUriU16 = xercesc::XMLString::transcode(
					std::string(const_cast<const char *>(kmsUri.pointer),
					            kmsUri.length).c_str());
				reqUriU16 = xercesc::XMLString::transcode(KMSUrl.c_str());
				scErrReturned = populateDocument(document, userUriU16, kmsUriU16,
					                             NULL, NULL, reqUriU16, NULL,
					                             NULL, NULL);

				if(scErrReturned == ScErrno::SUCCESS)
				{
					/*
					*  Sign the document.
					*/
					scErrReturned = signDocument(secCtx, document);

					if(scErrReturned == ScErrno::SUCCESS)
					{
						/*
						*  Serialize and push into vector.
						*/
						scErrReturned = xmlDocToVector(document,KmsCertCacheReq);
					}
				}
			}
			XSECPlatformUtils::Terminate();
		}

		xercesc::XMLPlatformUtils::Terminate();
		return scErrReturned;
	}

	/*
	* Main function to process information received from the KMS.
	* Args: secCtc      - Reference to the associated Security Context
	*                     (input).
	*	    KMSResponse - Buffer holding data from KMS (input).
	* Rets: Standard error code.
	*/
	ScErrno ProcessKmsRespXML(SecCtx secCtx,
		const std::vector<unsigned char> &
		KMSResponse)
	{
		return ProcessKmsRespXML(secCtx, KMSResponse, "");
	}

	/*
	* Main function to process information received from the KMS.
	* Args: secCtc      - Reference to the associated Security Context
	*                     (input).
	*	    KMSResponse - Buffer holding data from KMS (input).
	*		schemaDirectory - The directory containing the KMS response XML schema file
	*						if this parameter is empty the current directory will be used.
	*						If a directory is specified it must be suffixed with a directory seperator.
	* Rets: Standard error code.  
	*/
	ScErrno ProcessKmsRespXML(SecCtx secCtx,
		                      const std::vector<unsigned char> &
							    KMSResponse,
								std::string schemaDirectory)
	{
		if(KMSResponse.empty())	
		{
			return ScErrno::RESOURCE_NOT_ALLOCATED;
		}

		xercesc::XMLPlatformUtils::Initialize();
		ScErrno errorReturned = ScErrno::SUCCESS;
		{		
			XSECPlatformUtils::Initialise();

			/*
			*  Creates in memory buffer which holds the KMSReponse, 
			*  for use in parsing.
			*/
			xercesc::MemBufInputSource xmlBuffer(
				(const XMLByte *)&KMSResponse.front(),
				KMSResponse.size(), "myxml (in mem)");

			xercesc::DOMNode * kmsReponseTag = NULL; 
			xercesc::DOMNode * kmsMessageTag = NULL; 
			xercesc::DOMNode * kmsMessageTypeTag = NULL; 
			xercesc::DOMNodeList * nodeList = NULL;
			XMLCh * kmsMessage = NULL;
			XMLCh * unsignedResp = NULL;
			XMLCh * messageType = NULL;
			bool messageFound = false;
			std::string typeOfMessage = "";
			bool signedResponse = false;

			/* 
			*  Create new parser and set necessary flags.
			*/
			xercesc::XercesDOMParser parser;

			if(parser.loadGrammar((schemaDirectory + KMS_RESPONSE_SCHEMA).c_str(),
				xercesc::Grammar::SchemaGrammarType) == NULL)
			{
				errorReturned = ScErrno::XML_SCHEMA_NOT_FOUND;
			}
			else
			{
				parser.setValidationScheme(xercesc::XercesDOMParser::Val_Always);
				parser.setDoNamespaces(true);
				parser.setDoSchema(true);
				parser.setValidationSchemaFullChecking(true);
				xercesc::HandlerBase errHandler;
				parser.setErrorHandler(&errHandler);
				parser.setDoXInclude(true);
				parser.setExitOnFirstFatalError(true);

				try
				{
					/* 
					*  Attempt to parse data in memory.
					*/
					parser.parse(xmlBuffer);
				}
				catch(...)
				{
					errorReturned = ScErrno::GENERAL_FAILURE;
				}

				if(errorReturned == ScErrno::SUCCESS)
				{
					xercesc::DOMDocument * document = parser.getDocument();
					xercesc::DOMElement * root = document->getDocumentElement();

					/*
					*  Check to see if document is signed.
					*/
					unsignedResp = xercesc::XMLString::transcode("KmsResponse");

					if(!xercesc::XMLString::equals(unsignedResp, root->getNodeName())) 
					{ 
						xercesc::DOMNodeList * childrenOfRoot = NULL;

						if(root->hasChildNodes())
						{
							/*
							*  Ensures that the SignedKmsResponse tag has a corresponding 
							*  Signature tag.
							*/
							signedResponse = isSigned(root);

							if(signedResponse)
							{
								/*
								*  Obtains the KmsResponse tag.
								*/
								kmsReponseTag = getXMLTag(root, unsignedResp);
								if(kmsReponseTag == NULL)
								{
									errorReturned = ScErrno::KMS_RESPONSE_NOT_SIGNED;	
								}
							}
							else
							{
								//No accompanying signature to SignedKmsResponse therefore fail
								errorReturned = ScErrno::KMS_RESPONSE_NOT_SIGNED;
							}
						}
						else
						{
							errorReturned = ScErrno::KMS_RESPONSE_NOT_SIGNED;
						}

						xercesc::XMLString::release(&unsignedResp);
					}
					else
					{
						kmsReponseTag = root;
						xercesc::XMLString::release(&unsignedResp);
					}

					if(ScErrno::SUCCESS == errorReturned)
					{
						if(kmsReponseTag != NULL)
						{
							if(kmsReponseTag->hasChildNodes())
							{
								kmsMessage = xercesc::XMLString::transcode("KmsMessage");
								kmsMessageTag = getXMLTag(kmsReponseTag,kmsMessage);
								xercesc::XMLString::release(&kmsMessage);

								if(kmsMessageTag != NULL)
								{
									if(kmsMessageTag->hasChildNodes())
									{
										if(!messageFound)
										{
											messageType = xercesc::XMLString::transcode("KmsInit");
											kmsMessageTypeTag = getXMLTag(kmsMessageTag,messageType);
											xercesc::XMLString::release(&messageType);

											if(kmsMessageTypeTag != NULL)
											{
												messageFound = true;
											}
										}

										if(!messageFound)
										{
											messageType = xercesc::XMLString::transcode("KmsKeyProv");
											kmsMessageTypeTag = getXMLTag(kmsMessageTag,messageType);
											xercesc::XMLString::release(&messageType);

											if(kmsMessageTypeTag != NULL)
											{
												messageFound = true;
											}

										}

										if(!messageFound)
										{
											messageType = xercesc::XMLString::transcode("KmsCertCache");
											kmsMessageTypeTag = getXMLTag(kmsMessageTag,messageType);
											xercesc::XMLString::release(&messageType);
											if(kmsMessageTypeTag != NULL)
											{
												messageFound = true;
											}
										}

										if(kmsMessageTypeTag == NULL)
										{
											return ScErrno::RESOURCE_NOT_EXIST;
										}

										typeOfMessage = xercesc::XMLString::transcode(
											kmsMessageTypeTag->getNodeName());
									}
								}
								else
								{
									errorReturned = ScErrno::RESOURCE_NOT_EXIST;
								}
							}
							else
							{
								errorReturned = ScErrno::RESOURCE_NOT_EXIST;
							}
						}
						else
						{
							errorReturned = ScErrno::RESOURCE_NOT_EXIST;
						}

						/*
						*  Passes the parsed data of to the appropriate processing function 
						*  depending on what is held in typeOfMessage.
						*/
						if(typeOfMessage == "KmsKeyProv")
						{
							errorReturned = KmsKeyProvProcess(secCtx, document , root,  
								kmsMessageTypeTag, signedResponse);	
						}
						else if(typeOfMessage == "KmsInit")
						{
							errorReturned = KmsInitProcess(secCtx, document, root, 
								kmsMessageTypeTag, signedResponse);
						}
						else if(typeOfMessage == "KmsCertCache")
						{
							errorReturned = KmsCertCacheProcess(secCtx, document, root, 
								kmsMessageTypeTag, signedResponse);
						}
						else
						{
							errorReturned = ScErrno::GENERAL_FAILURE;
						}
					}
				}
			}

			XSECPlatformUtils::Terminate();
		}

		xercesc::XMLPlatformUtils::Terminate();
		return errorReturned;
	}
}
