/*******************************************************************************
 * Bridge Function
 *
 * A VERY SIMPLE program to receive UDP packets on port DEFAULT_LISTEN_PORT
 * (8300) and send these on to a number of other IP:Port addresses.
 *
 * The list of (IP:Port) addresses to send on to is obtained from files located
 * in the directory specified in the command line parameters.
 *
 * The received message is NOT passed back to the IP:Port address on which it
 * was received (i.e. not sent iback to the sender).
 ******************************************************************************/

#include "sc_socket.h"
#include <string.h>

#include <stdio.h>
#if defined (_WIN32) || defined (_WIN64)
#include <conio.h>
#endif

#include <sys/stat.h>
#include <stdlib.h>
#include <signal.h>
#include <iostream>
#include <fstream>

//Logging
#include "log4cpp/Category.hh"
#include "log4cpp/Appender.hh"
#include "log4cpp/FileAppender.hh"
#include "log4cpp/OstreamAppender.hh"
#include "log4cpp/Layout.hh"
#include "log4cpp/BasicLayout.hh"
#include "log4cpp/Priority.hh"

#define MAX_GROUP_MEMBERS    100 
#define POLL_TIMEOUT_SECS      5 /* On select timeout. After timeout the Goup UE list is refreshed. */
#define DEFAULT_LISTEN_PORT 8300
#define MAX_RCV_BUFF_SIZE   1500

SOCKET listen_socket;
SOCKET sending_socket;

typedef struct  
{
    std::string ip_addr;
    int port;
    std::string user_name;
} Ue;

/*******************************************************************************
 * Outputs usage
 *
 * Params:
 *     prog the program name
 ******************************************************************************/
void usage(char *prog) {
    printf("Usage: %s group-file-path\n", prog);
} /* usage */

/*******************************************************************************
 * Gets a UDP socket address for the supplied domain name and service.
 *
 * Params:
 *     domainname - the name or ip address
 *     service    - the UDP port
 *     sockaddr   - the socker address to populate.
 *
 * Return  Success/ Failure indication
 *
 * Lifted from original sc_app_main.cpp
 ******************************************************************************/
bool getInetUdpSocketAddress(const char *domainname, const char *service, sockaddr_in &sockaddr)
{
    int result;
    bool success = false;
    struct addrinfo hints;
    struct addrinfo *infolist = NULL;
    struct addrinfo *info_p = NULL;
    /* Set up the hints for an IPV4 UDP socket */
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_protocol = IPPROTO_UDP;

    result = getaddrinfo(domainname, service, &hints, &infolist);
    if (result == 0)
    {
        /* Search through the results for a suitable socket */
        for (info_p = infolist; info_p != NULL; info_p = info_p->ai_next)
        {
            if (info_p->ai_family == AF_INET)
            {
                /* Copy the sockaddr which we know is IPV4 sockaddr */
                memcpy(&sockaddr, info_p->ai_addr, sizeof(sockaddr_in));
                success = true;
                break;
            }
        }
    }

    /* Cleanup and exit */
    freeaddrinfo(infolist);
    return success;
} /* getInetUdpSocketAddress */

/*******************************************************************************
 * Outputs the UE contact details stored.
 *
 * Params:
 * 	group_ues The storage structure for group UEs.
 ******************************************************************************/
void outputUEContactDetails(std::vector<Ue> &group_ues) {
    char        *addr = NULL;
    short        port = 0;
    int          c = 0;

    printf("Current list: <%d>\n", group_ues.size());
    for (auto & ue: group_ues) {
        printf("%s  %s:%d\n", ue.user_name.c_str(), ue.ip_addr.c_str(), ue.port);
    }
} /* outputUEContactDetails */

/*******************************************************************************
 * Load the group member contact details from the group directory.
 *
 * Firstly, delete any group members (UE) that is no longer listed in the group
 * by going though the current-UE storage structure (and array of sockaddr_in).
 * If that entry cannot be found in the group directory then delete the entry
 * from the storage structure and shuffle (compact) remaining entries down.
 *
 * Next, add group members (UE) to the current-UE structure. This is achieved
 * by reading the contents of the group directory and checking if that contact
 * details are stored. If not the entry is added at the end of the storage
 * structure.
 *
 * Params:
 * 	group_file_name File name of group.cfg file.
 * 	group_ues The storage structure for group UEs.
 *
 * Return the number of stored UE's in group.
 ******************************************************************************/
int loadGroupMembers(
    std::string group_file_name,
    std::vector<Ue> &group_ues) 
{
    log4cpp::Category& log = log4cpp::Category::getRoot();
    
    std::vector<Ue> new_group_ues;
    std::ifstream group_file_stream;
    group_file_stream.open(group_file_name);
    if (!group_file_stream.is_open())
    {
        log.errorStream() << "Error opening group file: " << group_file_name;
        return -1;
    }

    std::string line;
    while (std::getline(group_file_stream, line))
    {
        Ue newUe;
        std::stringstream ss(line);
        ss >> newUe.user_name;
        ss >> newUe.ip_addr;
        std::string port_string;
        ss >> port_string;
        newUe.port = atoi(port_string.c_str());
        new_group_ues.push_back(newUe);
    }

    bool group_changed = false;
    if (group_ues.size() != new_group_ues.size())
    {
        group_changed = true;
    }
    else
    {
        for (unsigned int i = 0; i < group_ues.size(); i++)
        {
            if (group_ues[i].port != new_group_ues[i].port ||
                group_ues[i].ip_addr.compare(new_group_ues[i].ip_addr) != 0 ||
                group_ues[i].user_name.compare(new_group_ues[i].user_name) != 0)
            {
                group_changed = true;
                break;
            }
        }
    }

    group_file_stream.close();

    if (group_changed)
    {
        group_ues.swap(new_group_ues);
        log.debug("Group has changed.");
        outputUEContactDetails(group_ues);
    }
    
    return 0;
} /* loadGroupMembers */

/*******************************************************************************
 * Catch SIG_INT to close sockets.
 ******************************************************************************/
void signal_handler(int sig_num) {
    printf("\nCtrl-C detected, closing down!\n");
    /* Clean up */
    closesocket(listen_socket);
    closesocket(sending_socket);
    WSACleanup();
} /* signal_handler */

/*******************************************************************************
 * Main
 ******************************************************************************/
int main(int argc, char* argv[])
{
    // Setup logging.
    log4cpp::OstreamAppender console_appender("console", &std::cout);
    console_appender.setThreshold(log4cpp::Priority::DEBUG);
    log4cpp::FileAppender file_appender("default", "sc_bridge.log", false);
    file_appender.setThreshold(log4cpp::Priority::DEBUG);
    log4cpp::Category& log = log4cpp::Category::getRoot();
    log.setPriority(log4cpp::Priority::DEBUG);
    log.addAppender(console_appender);
    log.addAppender(file_appender);


    std::vector<Ue>  group_ues;
    sockaddr_in  listen_address;
    int          c = 0;
    int          result = 0;
    char        *group_dir = NULL; /* The dir containing the group members. */
    fd_set       fds;
    timeval      timeout;
    sockaddr_in  sender_address;
    char         message[MAX_RCV_BUFF_SIZE];
    socklen_t  sender_address_size = sizeof(sender_address);

    /* Initialise Winsock */
    WSADATA wsaData;
    result = WSAStartup(MAKEWORD(2, 2), &wsaData);
    if (result != NO_ERROR)
    {
        printf("Error: failed to initialise Winsock. Result = %d", result);
        return -1;
    }

    /*****************************************************************************/
    /* Handle command line parameters                                            */
    /*****************************************************************************/
    if (argc != 2) {
        usage(argv[0]);
        return -1;
    }
    group_dir = argv[1];

    /*****************************************************************************/
    /* Load Group UEs                                                            */
    /*****************************************************************************/
    if (loadGroupMembers(group_dir, group_ues) == -1) {
        printf("Error: Unable to load group UEs. Exiting!\n");
        return -1;
    }

    /*****************************************************************************/
    /* Listen socket for receiving messages from group UEs                       */
    /*****************************************************************************/
    if ((listen_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == INVALID_SOCKET) {
        printf("Error: unable to create listener socket: %d\n", WSAGetLastError());
        WSACleanup();
        return -1;
    }

    /*****************************************************************************/
    /* Sender socket for passing on messages to group UEs                        */
    /*****************************************************************************/
    if ((sending_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == INVALID_SOCKET) {
        printf("Error: unable to create sender socket: %d\n", WSAGetLastError());
        closesocket(listen_socket);
        WSACleanup();

        return -1;
    }

    /* Create listener socket - for messages */
    listen_address.sin_family = AF_INET;
    listen_address.sin_port = htons(DEFAULT_LISTEN_PORT);
    listen_address.sin_addr.s_addr = htonl(INADDR_ANY);
    result = bind(listen_socket, (SOCKADDR *)&listen_address, sizeof(listen_address));
    if (result != 0) {
        printf("Error: failed to bind listener socket. result = %d\n", result);
        closesocket(listen_socket);
        closesocket(sending_socket);
        WSACleanup();
        return -1;
    }

    /* Set up signal handler */
    if (signal(SIGINT, signal_handler) == SIG_ERR) {
        printf("Error: failed to activate signal handler for SIG_INT!\n");
        return -1;
    }

    /* Set up file descriptors for select */
    while (1) {
        /* Set up the file descriptor for the socket */
        FD_ZERO(&fds);
        FD_SET(listen_socket, &fds);

        /* Set wait time for receive - needs to be reset each iter. */
        timeout.tv_sec = POLL_TIMEOUT_SECS;
        timeout.tv_usec = 0;
        result = select(MAX_GROUP_MEMBERS, &fds, NULL, NULL, &timeout);
        if (result == SOCKET_ERROR) {
            break;
        }
        /* Check if any messages were received on the listening socket */
        if (result > 0) {
            if (FD_ISSET(listen_socket, &fds)) {
                result = recvfrom(listen_socket, message, sizeof(message), 0,
                    (SOCKADDR *)&sender_address, &sender_address_size);

                if (result == SOCKET_ERROR)
                {
                    printf("Error: while receiving message from socket.\n");
                    closesocket(listen_socket);
                    closesocket(sending_socket);
                    WSACleanup();
                    return -1;
                }

                else { 	/* Received something */

                    log.debug("Received something %d bytes long from %s:%d",
                        result,
                        inet_ntoa(sender_address.sin_addr),
                        ntohs(sender_address.sin_port));
                    std::stringstream ss;
                    for (c = 0; c < result; c++) {
                        ss << std::hex << std::uppercase << ((message[c] & 0xf0) >> 4) 
                           << std::hex << std::uppercase << (message[c]  & 0x0f);
                    }
                    log.debug(ss.str());

                    /* Distribute to all other parties */
                    for (auto & ue : group_ues) {
                        /* Do not send the same message back to the original sender */

                        log.debugStream() << "Comparing " <<
                            inet_ntoa(sender_address.sin_addr) << ":" <<
                            ntohs(sender_address.sin_port) << " to " <<
                            ue.ip_addr << ":" << ue.port;

                        bool same_as_sender = 
                            ((strcmp(inet_ntoa(sender_address.sin_addr), ue.ip_addr.c_str()) == 0) &&
                            (ntohs(sender_address.sin_port) == ue.port));

                        if (!same_as_sender) 
                        {
                            sockaddr_in ue_sockaddr_in;
                            getInetUdpSocketAddress(ue.ip_addr.c_str(), std::to_string(ue.port).c_str(), ue_sockaddr_in);
                            log.debugStream() << "Sending to " << ue.user_name << "(" << ue.ip_addr << ":" << ue.port << ")";
                            result = sendto(sending_socket, message, result, 0, (SOCKADDR *)&ue_sockaddr_in, sizeof(sockaddr_in));
                            if (result == SOCKET_ERROR)
                            {
                                log.errorStream() << "Failed to send to UE. Error = " << WSAGetLastError();
                            }
                        }
                    }
                }
            }
        }
        else 
        {
            if (loadGroupMembers(group_dir, group_ues) == -1)
            {
                log.error("loading group failed");
                return -1;
            };
        }
    }

    return 0;
}

/******************************************************************************/
/*                                End Of File                                 */
/******************************************************************************/
